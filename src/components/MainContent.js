
import React, { Component } from "react";
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const options = {
  chart: {
    type: 'spline'
  },
  xAxis: { 
    categories : ['Jan','Feb','Mar','Apr','May','Jun']
  },
  yAxis: { 
    categories : [0 ,100 ,200 ,300 ,400 ,500 ,600]
  },
  title: {
    text: 'Customer who have paid',
    align: 'left',
    fontSize: '14px',
  },
  series: [
    {
      data: [1, 2, 1, 4, 3, 6],
      color: 'rgb(80, 214, 182)'
    }
  ]
};

class MainContent extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Dashboard</div>
            <div className="page-info">
              <div className="breadcrumb-item">Home</div>
              <div className="breadcrumb-item">Default</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-2-4">
              <div className="card index">
                <div className="card-body">
                  <div className="row">
                    <div className="col-12">
                      <div className="card-title small">
                        <span className="fs-11 font-montserrat all-caps">
                          total store
                        </span>
                      </div>
                      <h1 className="text-success no-margin sm-bold">1.230</h1>
                      <div className="card-icon">
                        <img src={require("../asset/images/icons/analytics.png")} alt="Logo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2-4">
              <div className="card index">
                <div className="card-body">
                  <div className="row">
                    <div className="col-12">
                      <div className="card-title small">
                        <span className="fs-11 font-montserrat all-caps">
                          Store online
                        </span>
                      </div>
                      <h1 className="text-success no-margin sm-bold">983</h1>
                      <div className="card-icon">
                        <img src={require("../asset/images/icons/coin.png")} alt="Logo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2-4">
              <div className="card index">
                <div className="card-body">
                  <div className="row">
                    <div className="col-12">
                      <div className="card-title small">
                        <span className="fs-11 font-montserrat all-caps">
                          spl printed today
                        </span>
                      </div>
                      <h1 className="text-success no-margin sm-bold">16.902</h1>
                      <div className="card-icon">
                        <img src={require("../asset/images/icons/browser.png")} alt="Logo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2-4">
              <div className="card index">
                <div className="card-body">
                  <div className="row">
                    <div className="col-12">
                      <div className="card-title small">
                        <span className="fs-11 font-montserrat all-caps">
                          Average sales
                        </span>
                      </div>
                      <h1 className="text-success no-margin sm-bold"><sup>Rp</sup>124.302</h1>
                      <div className="card-icon">
                        <img src={require("../asset/images/icons/browsers.png")} alt="Logo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2-4">
              <div className="card index">
                <div className="card-body">
                  <div className="row">
                    <div className="col-12">
                      <div className="card-title small">
                        <span className="fs-11 font-montserrat all-caps">
                          Total sales today
                        </span>
                      </div>
                      <h1 className="text-success no-margin sm-bold"><sup>Rp</sup>892.907.382</h1>
                      <div className="card-icon">
                        <img src={require("../asset/images/icons/online-shop.png")} alt="Logo" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              <div className="card">
                <div className="card-body">
                  <div className="card-title m-10-vc">
                   This Week Sales
                  </div>
                  <div className="card-content">
                    <HighchartsReact highcharts={Highcharts} options={options} />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="card">
                <div className="card-body">
                  <div className="card-title m-10-vc">
                    Most Buyed Items
                  </div>
                  <div className="card-content">
                    <table className="table table-hover no-border">
                      <thead className="thead-green">
                        <tr>
                          <th className="all-caps">item name</th>
                          <th className="all-caps">sales</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Chitato 165gr</td>
                          <td>1802</td>
                        </tr>
                        <tr>
                          <td>Coca Cola, 3 liter</td>
                          <td>1674</td>
                        </tr>
                        <tr>
                          <td>Monde Serena 55gr</td>
                          <td>952</td>
                        </tr>
                        <tr>
                          <td>Indomie Ayam Bawang</td>
                          <td>812</td>
                        </tr>
                        <tr>
                          <td>Stella MT FLW ref225</td>
                          <td>86</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MainContent;
