import React from "react";
import { NavLink } from "react-router-dom";
import $ from "jquery";
import { userActions } from "../actions";
import { connect } from 'react-redux';
// import $ from 'jquery';

class SideBar extends React.Component {
  constructor(props){
    super(props);
  }
  logout = event =>{
    const { dispatch } = this.props;
    dispatch(userActions.logout());
  }
  componentDidMount(){

    $(document).ready(function() {
      $(".menu-list .tablinks:not(:first)").addClass("inactive");
      $(".tabcontent").hide();
      $(".tabcontent:first").show();

      $(".menu-list .tablinks").click(function() {
        var t = $(this).attr("id");
        if ($(this).hasClass("inactive")) {
          //this is the start of our condition
          $(".menu-list .tablinks").addClass("inactive");
          $(this).removeClass("inactive");

          $(".tabcontent").hide();
          if(typeof t!='undefined'){
            $("#" + t + "C").show();
          }
          
        }
      });

      if ($(".sub-menu").hasClass(".submenu-active")) {
        $(".left-panel").addClass("submenu-active");
      }
      $(".left-panel").hover(function() {
        $(".left-panel").addClass("submenu-active");
      });
      $(".main-menu .menu-list li").click(function() {
        var t = $(this).attr("id");
        // console.log(t)
        if(typeof t!='undefined'){
          $(".left-panel .sub-menu").addClass("submenu-active");
          $(".left-panel .main-menu").addClass("submenu-active");
          $(".left-panel .main-menu .logo-icon").addClass("submenu-active");
          $(".left-panel").addClass("submenu-active");
          $(".navbar-menu").addClass("submenu-active");
          $(".right-panel").addClass("submenu-active");
        }else{
          $(".left-panel .sub-menu").removeClass("submenu-active");
          $(".left-panel .main-menu").removeClass("submenu-active");
          $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
          $(".left-panel").removeClass("submenu-active");
          $(".navbar-menu").removeClass("submenu-active");
          $(".right-panel").removeClass("submenu-active");
        }
      });

      $(".submenu-tab ul li").click(function() {
        $(".right-panel").addClass("submenu-active");
      })

      $(".right-panel").click(function() {
        $(".left-panel .sub-menu").removeClass("submenu-active");
        $(".left-panel .main-menu").removeClass("submenu-active");
        $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
        $(".left-panel").removeClass("submenu-active");
        $(".navbar-menu").removeClass("submenu-active");
        $(this).removeClass("submenu-active");
      });
    });
  }
  render(){
    let {url} = this.props
    return (
      <div className="left-panel" id="menu-tabs">
        <div className="main-menu">
          {/* <div className="logo-icon" /> */}
          <ul className="menu-list">
            {/* <NavLink exact to={`${url}`} activeClassName="active">
              <li className="tablinks">
                <i className="fas fa-home" />
                <div className="caption">Dashboard</div>
              </li>
            </NavLink> */}
            
            <NavLink exact to={`${url}/employee`}>
              <li className="tablinks">
                  <i className="fas fa-user-tie" />
                  <div className="caption">Employee</div>
              </li>
            </NavLink>
            <li className="tablinks" onClick={(event)=>{this.logout()}}>
              <i className="fas fa-sign-out-alt" />
              <div className="caption">Logout</div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}



const mapStateToProps = (state) =>{
  const { loggingIn } = state.authentication;
  return {
      loggingIn
  };
}

export default connect(mapStateToProps)(SideBar);
