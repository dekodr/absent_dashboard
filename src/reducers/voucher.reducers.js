const initialState = { 
    voucher: [],
    open: false,
    product_id: '', 
    total:0,
    total_page: 0, 
    voucher_name: '',
    voucher_code: '',
    total_voucher:0,
    type_voucher: 1,
    image: '',
    voucher_code_list :[]
 };


export function voucher(state = initialState, action) {
    switch (action.type) {
        case 'FETCHED_ALL_VOUCHER':
            return {
            ...state,
            voucher: action.voucher,
            total: action.total,
            total_page: action.total_page
            };
        case 'SHOW_LOADING_VOUCHER_ENTRY':
            return {
            ...state,
            isLoading: true
            };
        case 'HIDE_LOADING_VOUCHER_ENTRY':
            return {
                ...state,
                isLoading: false
                };
        case 'VOUCHER_DETAIL':
            return {
                ...state,
                voucher_id: action.id,
                total_voucher: action.total_voucher,
                image: action.image,
                type_voucher: action.type_voucher,
                voucher_name: action.voucher_name,
                image: action.image,
                product_id: action.product_id,
                is_linked: action.is_linked,
                
            };
        case "VOUCHER_CODE_LIST_BY_ID":
            return {
                ...state,
                voucher_code_list :action.voucher_code_list
                
            };
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        case "RESET_STATE_VOUCHER":
            return initialState
        case "MESSAGE":
            // console.log(action)
            return {
                ...state,
                type: action.type,
                message: action.message
            }
        default:
            return state
    }
  }