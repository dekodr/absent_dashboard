import React from 'react';
import { connect } from 'react-redux';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
    withRouter
  } from "react-router-dom";
import {customerGroupAction,categoryAction,customerAction,voucherAction, customer_groupAction} from '../../actions';

class InsertCustomerGroup extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
			
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(customerAction.onChangeProps(prop, event));
    };
	componentDidMount(){
        const { match : {params }, dispatch } = this.props;
        dispatch(customer_groupAction.resetState())
        dispatch(categoryAction.getCategory());
        dispatch(customerAction.getCustomer());
        dispatch(voucherAction.getVoucher());
        
        if(params.id){
            this.setState({status : 'Update'});
            dispatch(customer_groupAction.getCustomerGroupById(params.id));
        } else {
            this.setState({status : 'Insert'});
        }
    }
    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
        
        let cust_list_id = "["+this.props.customer_group.cust_list_id.join(", ")+"]"
        let payload={
            name: this.props.customer_group.name,
            // cust_list_id:cust_list_id,
        }

        if(params.id){
            dispatch(customer_groupAction.editCustomerGroupInfo(params.id, payload, this.props));
            dispatch(customer_groupAction.resetState())
        }else{
            dispatch(customer_groupAction.createCustomer_group(payload, this.props));
        }
    }
    handleChangeCheckbox = prop => event => {
        const { dispatch } = this.props;
        // console.log(prop)
        dispatch(customer_groupAction.onChangeCheckbox(prop, event));
        // console.log(this.props)
    };
	render(){
        
        const { match : {params }} = this.props;
        const { category } = this.props.category;
        const { customer } = this.props.customer;
        const { voucher } = this.props.voucher;
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Customer Group</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Customer</div>
                            <div className="breadcrumb-item">{this.state.status} Customer Group</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.customer_group.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">{this.state.status} Customer Group</h2>
                            <div className="card-content">
                                
                                <form className="form">
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Customer Group Name</label>
                                        <input type="text" value={this.props.customer_group.name} className="form-control" 
                                         onChange={this.handleChange('name')}/>
                                    </div>
                                    {/* <h3>Daftar Customer</h3>
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Telephone</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            customer && customer.map(data => {
                                                let checked = "";
                                                let curr_value = data.customer_id[0];
                                                let custom = this.props.campaign.custom;

                                                // if((custom.indexOf(curr_value) > -1) == true){
                                                //     checked="checked"
                                                // }

                                                return (
                                                    <tr key={data.customer_id}>
                                                        <td>
                                                            <input type="checkbox" value={data.customer_id} className="form-control" onChange={this.handleChangeCheckbox('cust_list_id')}/>
                                                        </td>
                                                        <td>{data.first_name}</td>
                                                        <td>{data.last_name}</td>
                                                        <td>{data.phone_no}</td>
                                                    </tr>
                                                )
                                                
                                            })
                                        }
                                        </tbody>
                                    </table> */}
                                    
                                    {/* <div className="button-row">
                                        <div className="btn-group" >
                                            {
                                                this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                            onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> :
                                            <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                            }
                                            <button className="btn" onClick={() => this.props.history.push('/dashboard/setting/customer_group')}>Cancel</button>
                                       
                                        </div>
                                    </div> */}
                                </form>
                            </div>
                            </div>
                          </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
                                {
                                    this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> :
                                <button type="button" className="btn btn-primary" 
                                    onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                }
                                <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/setting/customer_group')}>Cancel</button>

									{/* <button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> */}
							
								</div>
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const InsertCustomerGroupPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(InsertCustomerGroup));
export default InsertCustomerGroupPage;
