import React from 'react';
import { connect } from 'react-redux';
// import Promo from '.'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import {dashboardService} from '../services/dashboard.service'

import DataTable, { createTheme } from 'react-data-table-component';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
  } from "react-router-dom";
import { dashboardAction } from '../actions/dashboard.action';
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Home extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			active_promo: null,
			broadcaster: null,
			total_hit: null,
			top_program: [],
			top_product: [],
			top_sales: [],
			id_merchant: null
		};
	}
	
	componentDidMount(){
		const {dispatch} = this.props
		const id_merchant = localStorage.getItem('merchant_id')
		// dispatch(dashboardAction.getActivePromo());
		
		
		if(id_merchant==5){
			this.props.getVoucherRedeemByLocation()
			this.props.getVoucherRedeemByCampaign()
		}else{
			this.props.getActivePromo();
			this.props.getBroadcaster();
			this.props.getTotalHit();
			this.props.getTopVoucher();
			this.props.getTopProduct()
			this.props.getTopSales()
		}
		
		this.setState({id_merchant: id_merchant})
		
		
	}
	render(){
		let { active_promo, broadcaster, total_hit, top_voucher, top_program_hit,  top_product, top_program_redeem, data, sales, top_sales_hit, top_sales_redeem, total } = this.props.dashboard
		let {voucher_redeem_by_location, voucher_redeem_by_campaign} = this.props.dashboard
		console.log(voucher_redeem_by_campaign)
		if(typeof data!='undefined'){
			broadcaster = data.length
			total_hit = data.total_hit
		}
		
		top_program_hit = top_program_hit.filter(function (el) {
			return el != null;
		});
		let options_chart_1 = {}
		if(typeof top_voucher!=='undefined'){
			// console.log(typeof top_voucher)
			options_chart_1 = {
				chart: {
				  type: 'column'
				},
				title: {
				  text: ''
				},
				subtitle: {
				  text: ''
				},
				xAxis: {
				  categories: top_voucher,
				  crosshair: true
				},
				yAxis: {
				  min: 0,
				  title: {
					text: 'Total Hit'
				  }
				},
				plotOptions: {
				  column: {
					  grouping: false,
					  shadow: false,
				  }
				},
				series: [{
				  name: 'Hit Total',
				  data: top_program_hit,
				  color: 'rgba(165,170,217,1)',
				  pointPadding: 0.3, 
				}, {
					name: 'Redeem Total',
					color: 'rgba(126,86,134,.9)',
					data: top_program_redeem,
					pointPadding: 0.4,
					
				  }]
			};
		}

		let sales_code_chart = {}
		if(typeof sales!=='undefined'){
			// console.log(typeof top_voucher)
			sales_code_chart = {
				chart: {
				  type: 'column'
				},
				title: {
				  text: ''
				},
				subtitle: {
				  text: ''
				},
				xAxis: {
				  categories: sales,
				  crosshair: true
				},
				yAxis: {
				  min: 0,
				  title: {
					text: 'Total Hit'
				  }
				},
				plotOptions: {
				  column: {
					  grouping: false,
					  shadow: false,
				  }
				},
				series: [{
				  name: 'Hit Total',
				  data: top_sales_hit,
				  color: 'rgba(165,170,217,1)',
				  pointPadding: 0.3, 
				}, {
					name: 'Redeem Total',
					color: 'rgba(126,86,134,.9)',
					data: top_sales_redeem,
					pointPadding: 0.4,
					
				  }]
			};
		}
		
		const options_bar_1 = {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			title: {
				text: ''
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: false
					},
					showInLegend: false
				}
			},
			series: [{
				name: 'Category',
				innerSize: '20%',
				colorByPoint: true,
				data: this.state.category_promo
			}]
		}
		const achiever_bar = {
			chart: {
			  type: 'column'
			},
			title: {
			  text: ''
			},
			subtitle: {
			  text: ''
			},
			xAxis: {
			  categories: this.state.achiever_label,
			  crosshair: true
			},
			yAxis: {
			  min: 0,
			  title: {
				text: 'Total Hit'
			  }
			},
			plotOptions: {
				column: {
					grouping: false,
					shadow: false,
				}
			},
			series: [{
			  name: 'Total Hit',
			  data: this.state.achiever_data,
			  pointPadding: 0.3,
			},{
				name: 'Total Redeem',
				data: this.state.achiever_redeem,
				pointPadding: 0.4,
			  }]
		}
		let voucher_redeem_by_location_chart = {}
		if(voucher_redeem_by_location!='undefined'){
			voucher_redeem_by_location_chart = {
				chart: {
				type: 'column'
				},
				title: {
				text: ''
				},
				subtitle: {
				text: ''
				},
				xAxis: {
					type:'category'
				},
				yAxis: {
				min: 0,
				title: {
					text: 'Total Hit'
				}
				},
				plotOptions: {
				column: {
					grouping: false,
					shadow: false,
				}
				},
				series: [{
				name: 'Redeem',
				data: voucher_redeem_by_location,
				//   function(){
				// 	let data = []
				// 	voucher_redeem_by_location.map(function(key, value){
				// 		data[key] = value['count']
				// 	})
				// 	  return data
				//   },
				color: 'rgba(165,170,217,1)',
				pointPadding: 0.3, 
				}]
			};
		}
		let voucher_redeem_by_campaign_chart = {}
		if(voucher_redeem_by_campaign!='undefined'){
			voucher_redeem_by_campaign_chart = {
				chart: {
					type: 'column'
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					type:'category'
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Total Hit'
					}
				},
				plotOptions: {
					column: {
						grouping: false,
						shadow: false,
					}
				},
				series: [{
					name: 'Redeem',
					data: voucher_redeem_by_campaign,
					// function(){
					// 	let data = []
					// 	voucher_redeem_by_location.map(function(key, value){
					// 		data[key] = value['count']
					// 	})
					// 	return data
					// },
					color: 'rgba(165,170,217,1)',
					pointPadding: 0.3, 
				}]
			}
		}
		
		return (
			<div>
				<div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Dashboard</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Dashboard</div>
						</div>
					</div>
				</div>
				{
					this.state.id_merchant==5 ? 
						<div className="p-20-c">
							
							<div className="row" id="index-first-row">
								<div className="col-12">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Voucher Redeem by Store</div>
											{ 	
												(typeof voucher_redeem_by_location!=='undefined') ? 
													<HighchartsReact
													highcharts={Highcharts}
													options={voucher_redeem_by_location_chart}
												/>: null
											}
											
										</div>
									</div>			
								</div>
								
							</div>
							<div className="row">
								<div className="col-12">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Voucher Redeem by Code </div>
											{ 	
												(typeof voucher_redeem_by_campaign!=='undefined') ? 
													<HighchartsReact
													highcharts={Highcharts}
													options={voucher_redeem_by_campaign_chart}
												/>: null
											}
											
										</div>
									</div>			
								</div>
							</div>
							<div className="row">
								
								<div className="col-6">
									<div className="card">
										<div className="card-body">
											{
						
												<DataTable
													pagination={true}
													columns={[
														{
															name: 'Sales Code',
															selector: 'name',
															sortable: true,
														},
														{
															name: 'Total Hit',
															selector: 'y',
															sortable: true,
														}
														]}
													data={voucher_redeem_by_location}
													striped={true}
													customStyles={styleTable}
													defaultSortField={'count'}
													defaultSortAsc={false}
												/>
											}  
										</div>
									</div>
								</div>
								<div className="col-6">
									<div className="card">
										<div className="card-body">
											{
						
												<DataTable
													pagination={true}
													columns={[
														{
															name: 'Sales Code',
															selector: 'name',
															sortable: true,
														},
														{
															name: 'Total Hit',
															selector: 'y',
															sortable: true,
														}
														]}
													data={voucher_redeem_by_campaign}
													striped={true}
													customStyles={styleTable}
													defaultSortField={'count'}
													defaultSortAsc={false}
												/>
											}  
										</div>
									</div>
								</div>
							</div>
						</div> :
						<div className="p-20-c">
							<div className="row" id="index-first-row">
								<div className="col-4">
									<div className="card index">
										<div className="card-body">
										<div className="row">
											<div className="col-12">
											<div className="card-title small">
												<span className="fs-11 font-montserrat all-caps">
												Active Promo
												</span>
											</div>
											<h1 className="text-success no-margin sm-bold">{active_promo}</h1>
											<div className="card-icon">
												<img src={require("../asset/images/icons/browser.png")} alt="Logo" />
											</div>
											</div>
										</div>
										</div>
									</div>
								</div>
								<div className="col-4">
									<div className="card index">
										<div className="card-body">
										<div className="row">
											<div className="col-12">
											<div className="card-title small">
												<span className="fs-11 font-montserrat all-caps">
												Broadcaster
												</span>
											</div>
											<h1 className="text-success no-margin sm-bold">{broadcaster}</h1>
											<div className="card-icon">
												<img src={require("../asset/images/icons/analytics.png")} alt="Logo" />
											</div>
											</div>
										</div>
										</div>
									</div>
								</div>
								<div className="col-4">
									<div className="card index">
										<div className="card-body">
										<div className="row">
											<div className="col-12">
											<div className="card-title small">
												<span className="fs-11 font-montserrat all-caps">
												Total Hit
												</span>
											</div>
											<h1 className="text-success no-margin sm-bold">{total}</h1>
											<div className="card-icon">
												<img src={require("../asset/images/icons/coin.png")} alt="Logo" />
											</div>
											</div>
										</div>
										</div>
									</div>
								</div>
							</div>
		
							<div className="row">
								<div className="col-12">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Most Hit Sales Code</div>
											{
												(typeof top_voucher!=='undefined') ? 
													<HighchartsReact
													highcharts={Highcharts}
													options={sales_code_chart}
												/>: null
											}
											
										</div>
									</div>			
								</div>
							</div>
							<div className="row">
								{/* <div className="col-6">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Top 5 Promo Program</div>
											{
												(typeof top_voucher!=='undefined') ? 
													<HighchartsReact
													highcharts={Highcharts}
													options={options_chart_1}
												/>: null
											}
											
										</div>
									</div>		
								</div> */}
								<div className="col-12">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Most Hit Sales Code</div>
											{
						
												<DataTable
													pagination={true}
													columns={[
														{
															name: 'Sales Code',
															selector: 'name',
															sortable: true,
														},
														{
															name: 'Total Hit',
															selector: 'count',
															sortable: true,
														},
														{
															name: 'Total Redeem',
															selector: 'redeem',
															sortable: true,
														}
														]}
													data={data}
													striped={true}
													customStyles={styleTable}
													defaultSortField={'count'}
													defaultSortAsc={false}
												/>
											}  
										</div>
									</div>		
								</div>
							</div>
							<div className="row">
								{/* <div className="col-4">
									<div className="card">
										<div className="card-body">
											<div className="box-title" style={{ zIndex : 10, position : 'relative'}}>Most Hit By Category</div>
											<HighchartsReact
												highcharts={Highcharts}
												options={options_bar_1}
											/>
										</div>
									</div>		
								</div> */}
									
							</div>
							
							{/* <div className="row">
								<div className="col-12">
									<div className="card">
										<div className="card-body">
											<div className="box-title">Top 5 Achiever</div>
											<HighchartsReact
												highcharts={Highcharts}
												options={achiever_bar}
											/>
										</div>
									</div>		
								</div>
							</div> */}
						</div>
				}
			</div>
			
	  	);
	}
}
const mapStateToProps = (state) =>{
    return state
  }
const mapDispatchToProps = (dispatch)=>{
    return {
        getActivePromo:()=>{
            dispatch(dashboardAction.getActivePromo());
		},
		getBroadcaster:()=>{
            dispatch(dashboardAction.getBroadcaster());
		},
		getTotalHit:()=>{
			dispatch(dashboardAction.getTotalHit());
		},
		getTopProduct:()=>{
			dispatch(dashboardAction.getTopProduct())
		},
		getTopVoucher:()=>{
			dispatch(dashboardAction.getTopVoucher());
		},
		getTopSales:()=>{
			dispatch(dashboardAction.getTopSales())
		},
		getVoucherRedeemByLocation:()=>{
			dispatch(dashboardAction.getVoucherRedeemByLocation())
		},
		getVoucherRedeemByCampaign:()=>{
			dispatch(dashboardAction.getVoucherRedeemByCampaign())
		}
    }
}

const Home_page = connect(mapStateToProps,mapDispatchToProps, null, {
    pure: false
})(Home);
export default Home_page;
