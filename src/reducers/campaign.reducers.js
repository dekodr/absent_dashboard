const initialState = { anchor: 'left',
    campaign: [],
    open: false,
    campaign_id: '',
    name: '',
    from_date: '',
    to_date:'',
    fee:'',
    total_voucher:'',
    max_per_day:[],
    max_per_month:[],
    voucher_list_id:[],
    list_employee_group:[],
    list_customer_group:[],
    banner: '',
    total:[],
    customer_group_id:[],
    limit:[],
    _voucher:[],
    maximum_request:[],
    option_request:[],
    message: {
        type: '',
        message:''
    },
    messages:'',
    employee_greeting_img:''
 };


export function campaign(state = initialState, action) {
   
    switch (action.type) {
        case 'FETCHED_ALL_CAMPAIGN':
            return {
            ...state,
            _voucher:[],
            maximum_request:[],
            option_request:[],
            campaign: action.campaign
            };
        case 'CAMPAIGN_DETAIL':
            return {
                ...state,
                campaign_id: action.campaign_id,
                name: action.name,
                fee: action.fee,
                from_date: action.from_date,
                to_date: action.to_date,
                total_voucher: action.total_voucher,
                max_per_day: action.max_per_day,
                max_per_month: action.max_per_month,
                voucher_list_id: action.voucher_list_id,
                emp_list_id: action.emp_list_id,
                banner: action.banner,
                default_banner : action.banner,
                list_customer_group :action.list_customer_group,
                list_employee_group: action.list_employee_group,
                _voucher:action._voucher,
                max_per_day:action.max_per_day,
                max_per_month:action.max_per_month,
                limit:action.limit,
                status:action.status,
                customer_group_id:action.customer_group_id,
                messages: action.messages,
                wrong_code: action.wrong_code,
                employee_no_promo: action.employee_no_promo,
                empty_slot: action.empty_slot,
                wrong_index: action.wrong_index,
                max_reached: action.max_reached,
                no_campaign: action.no_campaign,
                empty_voucher: action.empty_voucher,
                last_voucher: action.last_voucher,
                employee_greeting: action.employee_greeting,
                employee_mesages: action.employee_mesages,
                employee_greeting_img: action.employee_greeting_img,
                default_employee_greeting_img : action.employee_greeting_img
            };
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };  
        case "HANDLE_ON_CHANGE_CHECKBOX":
            let _state = state[action.props]
            if(typeof action.value =='string'){
                action.value = parseInt(action.value)
            }
            if(_state.includes(action.value)==false){
                _state.push(action.value)
            }else{
                let index = _state.indexOf(action.value);
                if (index !== -1) _state.splice(index, 1);
            }
            return {
                ...state,
                [action.props]: _state
            }; 
        case "HANDLE_ON_CHANGE_CHECKBOX_VOUCHER":
            let _state_voucher = state[action.props]
            if(typeof action.value =='string'){
                action.value = parseInt(action.value)
            }
            if(_state_voucher[action.key]==action.value){
                let index = _state_voucher.indexOf(action.value);
                if (index !== -1) _state_voucher.splice(index, 1);
            }else{
                _state_voucher[action.key] = parseInt(action.value)
            }
            return {
                ...state,
                [action.props]: _state_voucher
            };   
        case "HANDLE_ON_CHANGE_CHECKBOX_CUSTOMER":
            let _customer_group_id = state[action.props]
            let list_customer_group = state.list_customer_group
            if(typeof action.id =='string'){
                action.id = parseInt(action.id)
            }
            if(typeof action.value =='string'){
                action.value = parseInt(action.value)
            }
            
            if(_customer_group_id.includes(action.id)==false){

                _customer_group_id.push(action.id)
            }else{
                let index = _customer_group_id.indexOf(action.id);
                if (index !== -1) _customer_group_id.splice(index, 1);
            }
           
            // console.log(list_customer_group.includes(action.value))
            // if(list_customer_group.includes(action.value)==false){
            //     list_customer_group.push(action.value)
            // }else{
                
            //     let index = list_customer_group.indexOf(action.value);
            //     if (index !== -1) list_customer_group.splice(index, 1);
            // }
            // console.log(state)
            // // console.log(action.value)
            // // console.log(list_customer_group)
            // console.log(list_customer_group.includes(action.value))
            return {
                ...state,
                customer_group_id: _customer_group_id
                // list_customer_group:list_customer_group
            };    
        case "HANDLE_ON_CHANGE_OPTION_VOUCHER":
            let _state_voucher_option = state[action.props]
            if(_state_voucher_option[action.key]==action.value){
                let index = _state_voucher_option.indexOf(action.value);
                if (index !== -1) _state_voucher_option.splice(index, 1);
            }else{
                _state_voucher_option[action.key] = action.value
            }
            return {
                ...state,
                [action.props]: _state_voucher_option
            };   
        case "HANDLE_ON_CHANGE_NUMBER":
            let _state_number = state[action.props]
            _state_number[action.key] = action.value
            
            return {
                ...state,
                [action.props]: _state_number
            };   
        case "RESET_STATE_CAMPAIGN":
            return initialState
        case "SET_MESSAGE_CAMPAIGN":
			return {
				...state,
				message: {
					status_message: action.status_message,
					message: action.message
				}
            }
        case "REMOVE_ALERT_CAMPAIGN":
            return {
				...state,
				message: {
					status_message: "",
					message: ""
				}
            }
        case "UPDATE_DATE_CAMPAIGN":
            return {
                ...state,
                [action.field]: action.date
            }
        default:
            return state
    }
  }