import axios from 'axios';
import config from '../config/config';
import uuid from 'react-uuid'

export const customerServices = {
    get,
    post,
    put,
    deleteDetail,
    post_csv
};

function get(apiEndpoint){
    return axios.get(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}
function post_csv(apiEndpoint, payload){
	
	const newFileName = uuid();
	
    const formData = new FormData();
    formData.append('uploadcsv', payload.uploadcsv);
    

    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
	
}
function post(apiEndpoint, payload){
    // console.log(payload,"<<<<<<<<<<<<")
    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function put(apiEndpoint, payload){
    return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function deleteDetail(apiEndpoint){
    return axios.delete(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function getOptions(){
    let options = {}; 
    if(localStorage.getItem('token')){
        options.headers = { 'x-access-token': localStorage.getItem('token') };
    }
    return options;
}