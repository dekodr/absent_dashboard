import React, { Component } from "react";
import "../asset/css/component.css";

import PropTypes from "prop-types";

class ToggleSwitch extends Component {
    
    state = {
        checked: this.props.defaultChecked
    };

    componentDidMount(){
        console.log("is default checked:", this.props)
    }
    onChange = e => {
      this.setState({
        checked: e.target.checked
      });
      console.log("keklick:",e.target.checked)
      if (typeof this.props.onChange === "function") this.props.onChange();
    };

    render() {
        setTimeout(
            function() {
                this.setState({position: 1});
            }
            .bind(this),
            3000
        );

        this.setTimeout()
        return (
            <div className="switch-control">
                <label className="switch">
                    <input 
                        type        = "checkbox" 
                        name        = {this.props.name}
                        id          = {this.props.id}
                        onChange    = {this.onChange}
                        value       = {this.props.value}/>
                    <span className="slider round"></span>
                </label>
                <label className="switch-label">Is Active?</label>
            </div>
            // <div className="toggle-switch">
            //     <input
            //         type="checkbox"
            //         className="toggle-switch-checkbox"
            //         name={this.props.Name}
            //         id={this.props.Name}
            //         />
            //     <label className="toggle-switch-label" htmlFor={this.props.Name}>
            //         <span className="toggle-switch-inner" />
            //         <span className="toggle-switch-switch" />
            //     </label>
            // </div>
        );
    }
}
ToggleSwitch.propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    name: PropTypes.string,
    onChange: PropTypes.func,
    defaultChecked: PropTypes.bool,
    Small: PropTypes.bool,
    currentValue: PropTypes.bool,
    disabled: PropTypes.bool
  };
  
export default ToggleSwitch;