import React from 'react';
import { connect } from 'react-redux';

import DataTable, { createTheme } from 'react-data-table-component';
import {
    BrowserRouter as Router,
    withRouter
  } from "react-router-dom";
//import $ from "jquery";
import { employeeAction, employeeGroupAction } from '../../actions';
import "../../asset/css/pagination.css";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class DetailEmployeeGroup extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            page: 1
        }
    }
    
    componentDidMount(){
        const { match : {params }, dispatch } = this.props;
        if(params.id){
            dispatch(employeeGroupAction.getEmployeeGroupById(params.id));
            dispatch(employeeGroupAction.getEmployeeListByGroupId(params.id));
        }
    }
    handleChangeFilter = prop => event => {
        this.props.handleChangeFilter(prop, event)
    }
    handlePageClick = (event)=>{
        const { dispatch, employee } = this.props;
        this.props.handlePageClick(event, employee)
    }
    render(){
        const { list_employee } = this.props.employeegroup;
       
        return (
            <div>
                <div className="p-20-c">
                    <div className="breadcrumb">
                        <div className="page-title">Employee Group</div>
                        <div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Employee Group</div>
                            <div className="breadcrumb-item">Detail Employee Group</div>
                        </div>
                    </div>
                </div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-8">
                            
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                                    <form className="form">
                                     <div className="form-group">
                                        <label htmlFor="#" className="form-label">Employee Group Name</label>
                                        <input type="text" value={this.props.employeegroup.name} className="form-control" 
                                         disabled  />
                                    </div>
                                    {
                                        <DataTable
                                            title="Employee" 
                                            pagination={true}
                                            columns={[
                                                {
                                                    name: 'First Name',
                                                    selector: 'first_name',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Last Name',
                                                    selector: 'last_name',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Telephone',
                                                    selector: 'phone_no',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Employee Code',
                                                    selector: 'code_name',
                                                    sortable: true,
                                                }
                                                // {
                                                //     name: 'Action',
                                                //     selector: 'category_id',
                                                //     cell: row => <span>
                                                //         <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/category/edit/"+row.category_id)}><i className="far fa-edit"></i></button>
                                                //         <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.category_id)}><i className="fas fa-trash"></i></button>
                                                //         </span>
                                                // }
                                                ]}
                                            data={list_employee}
                                            striped={true}
                                            customStyles={styleTable}
                                        />
                                    }
                                    </form>
                                    </div>      
                                </div>
                            </div>      
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-auto right">
                            <div className="card">
                                <div className="card-body">

                            	    <div className="button-row">
                                        <div className="btn-group" style={{justifyContent: 'space-between', textAlign: 'right'}} >

                                        <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/setting/employee_group')}>Cancel</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        );
    }
}
const mapStateToProps = (state) =>{

    return state
}
const DetailEmployeeGroupPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(DetailEmployeeGroup));
export default DetailEmployeeGroupPage;