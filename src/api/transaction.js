import axios from "axios";

export const transactionService = {
  get_broadcaster,
  get_active_promo,
  get_hit_total,
  get_total_promo_top,
  get_total_by_category,
  get_total_by_product,
  get_achiever,
  get_employee,
  get_transaction_employee

}
const API_URL = 'http://103.93.56.182:14011/'
// const API_URL = 'http://35.197.143.136:8000/'
// function login(username, password, fcmToken){
//   // const { email, password } = props;
  
//   // NOTE HTTP is insecure, only post to HTTPS in production apps
  
//   return axios.get(AUTH_URL,{
//     params:{
//       username: username,
//       password: password,
//       fcmToken: fcmToken
//       // username: '081398434402',
//       // password: '2016071'
//     }
//   })
// }

function get_broadcaster(){
    return axios.get(API_URL+'/broadcaster',{
        params:null
    })
}
function get_active_promo(){
  return axios.get(API_URL+'/promo/active',{
      params:null
  })
}
function get_hit_total(){
  return axios.get(API_URL+'/hit/total',{
      params:null
  })
}
function get_total_promo_top(){
  return axios.get(API_URL+'/promo/top',{
    params:null
})
}
function get_total_by_category(){
  return axios.get(API_URL+'/hit/category',{
    params:null
})
}
function get_total_by_product(){
  return axios.get(API_URL+'/hit/product',{
    params:null
})
}
function get_achiever(){
  return axios.get(API_URL+'/hit/achiever',{
    params:null
})
}
function get_employee(){
  return axios.get(API_URL+'/employee',{
    params:null
})
}
function get_transaction_employee(id){
  return axios.get(API_URL+'/transaction/employee',{
    params:{
      id: id
    }
})
}