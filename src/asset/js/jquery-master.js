import $ from "jquery";

$(document).ready(function() {

  // - - - - - SIDEBAR - - - - -
  $(".menu-list .tablinks:not(:first)").addClass("inactive");
  $(".tabcontent").hide();
  $(".tabcontent:first").show();

  $(".menu-list .tablinks").click(function() {
    var t = $(this).attr("id");
    if ($(this).hasClass("inactive")) {
      //this is the start of our condition
      $(".menu-list .tablinks").addClass("inactive");
      $(this).removeClass("inactive");

      $(".tabcontent").hide();
      $("#" + t + "C").show();
    }
  });

  if ($(".sub-menu").hasClass(".submenu-active")) {
    $(".left-panel").addClass("submenu-active");
  } else {
    $(".left-panel").hover(function() {
      $(".left-panel").addClass("submenu-active");
    });
  }

  // $('.sub-menu .submenu-tab li').click(function() {
  //   $(".left-panel .sub-menu").removeClass("submenu-active");
  //   $(".left-panel .main-menu").removeClass("submenu-active");
  //   $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
  //   $(".left-panel").removeClass("submenu-active");
  //   $(".navbar-menu").removeClass("submenu-active");
  //   $(".right-panel").removeClass("submenu-active");
  // });

  $(".main-menu .menu-list li").click(function() {
    $(".left-panel .sub-menu").addClass("submenu-active");
    $(".left-panel .main-menu").addClass("submenu-active");
    $(".left-panel .main-menu .logo-icon").addClass("submenu-active");
    $(".left-panel").addClass("submenu-active");
    $(".navbar-menu").addClass("submenu-active");
    $(".right-panel").addClass("submenu-active");
  });

  $(".submenu-tab ul li").click(function() {
    $(".right-panel").addClass("submenu-active");
  })

  $(".right-panel").click(function() {
    $(".left-panel .sub-menu").removeClass("submenu-active");
    $(".left-panel .main-menu").removeClass("submenu-active");
    $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
    $(".left-panel").removeClass("submenu-active");
    $(".navbar-menu").removeClass("submenu-active");
    $(this).removeClass("submenu-active");
  });
  // SIDEBAR - - - END

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 30) {
      $(".navbar-menu").addClass("scroll-active");
    } else {
      $(".navbar-menu").removeClass("scroll-active");
    }
  });

  // $('.dropify').dropify();

  $('.form-group.disabled').children('input').attr('disabled','true');

  $('.form-group').click(function() {
    $(this).toggleClass('focused');
    $(this).children('input').focus();
  });

  // - - - - - - - - - R A D I O  J S - - - - - - - - -
  $('.radio-label').click(function(e) {
    e.stopPropagation();

    if (!$(this).hasClass('checked')) {
      if ($(this).is('#radio-label1')) {
        console.log()
        $( "#radio1" ).prop( "checked", true ).trigger('click');
        $( "#radio2" ).prop( "checked", false );
        $('#radio-label1').addClass('checked');
        $('#radio-label2').removeClass('checked');
      } else {
        $( "#radio1" ).prop( "checked", false );
        $( "#radio2" ).prop( "checked", true ).trigger('click');
        $('#radio-label1').removeClass('checked');
        $('#radio-label2').addClass('checked');
      }
    } else {
      $( "#radio1" ).prop( "checked", false );
      $( "#radio2" ).prop( "checked", false );
      $(this).removeClass('checked');
    }

  });
  // - - - - - - - R A D I O  J S  E N D - - - - - - -


  // - - - - - - - - - S W I T C H  J S - - - - - - - - -
  $('.switch-label').click(function(e) {
    // e.stopPropagation();

    // $(this).siblings('.switch').click();

  });
  // - - - - - - - S W I T C H  J S  E N D - - - - - - - 


  // - - - - - - - - - F I L E  I N P U T  J S - - - - - - - - -
  $('#chooseFile').bind('change', function () {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
      $(".file-upload").removeClass('active');
      $("#noFile").text("No file chosen..."); 
    }
    else {
      $(".file-upload").addClass('active');
      $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
    }
  });
  // - - - - - - - F I L E  I N P U T  J S  E N D - - - - - - -

  


  $(document).mouseup(function (e)
                    {
    var container = $(".form-group"); // YOUR CONTAINER SELECTOR

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
      container.removeClass('focused');
    }
  });

});





