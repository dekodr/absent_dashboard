const initialState = { anchor: 'left',
    customer: [],
    open: false,
    id: '',  
    name: '',
    mobile: '',
    phone_number: '',
    address: '',
    isLoading:false
 };


export function customer(state = initialState, action) {
    
    switch (action.type) {
        case 'FETCHED_ALL_CUSTOMER':
            return {
            ...state,
            customer: action.customer
            };
        case 'CUSTOMER_DETAIL':
            return {
                ...state,
               
                id: action.id,
                first_name: action.first_name,
                last_name: action.last_name,
                phone_no: action.phone_no,
                customer_group_id : action.customer_group_id,
                is_elastic: action.is_elastic
            };
        case "CUSTOMER_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };    
        case 'SHOW_LOADING_CUSTOMER_ENTRY':
            return {
            ...state,
            isLoading: true
            };
        case 'HIDE_LOADING_CUSTOMER_ENTRY':
            return {
                ...state,
                isLoading: false
                };
        
		case "RESET_STATE_CUSTOMER":
			return initialState

        case "MESSAGE":
            return {
                ...state,
                type: action.type,
                message: action.message
            }
        default:
            return state
    }
  }