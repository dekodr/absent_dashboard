import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';
import ReactPaginate from 'react-paginate';

import $ from "jquery";
import { employeeAction } from '../../actions';
import "../../asset/css/pagination.css";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Employee extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            page: 1
        }
    }
    
    componentDidMount(){
        const { dispatch, employee } = this.props;

        this.props.resetState()
        this.props.getEmployee(employee)


        $('.modal-toggle').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal-wrapper').addClass('modal-active');
            $('.modal').addClass('modal-active');
        })

        $('.modal-wrapper span.modal-outsider').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal').removeClass('modal-active');
        })
        $('.modal-wrapper .btn-link').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal').removeClass('modal-active');
        })
        
        $('.dropify').dropify();
        $('#employeeWrapper').detach().appendTo('#root')

    }
    handleChangeFilter = prop => event => {
        this.props.handleChangeFilter(prop, event)
    };
    handleClick = (event, id) => {
        const { employee } = this.props;
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            this.props.deleteEmployeeById(id, employee);
            window.scrollTo(0, 0)
        }
    }
    
    handleSubmitFilter = ()=>{
        const { dispatch, employee } = this.props;

        let payload={
            first_name  : employee.form_filter.first_name,
            last_name   : employee.form_filter.last_name,
            phone_no    : employee.form_filter.phone_no
        }
        this.props.handleSubmitFilter(employee,payload)
        
    }
    handleUploadCsv = (prop, name) => event => {
        this.props.onChangeProps(prop, name)
        
	}
    handleSubmitUploadCsv(event){
        const { dispatch, employee } = this.props;
        this.props.uploadCsvEmployee(employee.uploadcsv, this.props)
       
	}
    handlePageClick = (event)=>{
        const { dispatch, employee } = this.props;
        this.props.handlePageClick(event, employee)
    }
    render(){
        const { employee, total_page, total } = this.props.employee;
        console.log(employee.isLoading)
        return (
            <div>
                <div className="p-20-c">
                    <div className="breadcrumb">
                        <div className="page-title">Employee</div>
                        <div className="page-info">
                            <div className="breadcrumb-item">Home</div>
                            <div className="breadcrumb-item">Employee</div>
                        </div>
                    </div>
                </div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                <div className="button-row">
                    <div className="btn-group" >
                        <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/employee/insert")}><i className="far fa-plus-square"></i>Create</button>
                        {/* <button className="btn btn-primary modal-toggle" ><i className="fas fa-upload"></i>Upload CSV</button> */}
                    </div>
                </div>
                {
                
                    <DataTable
                        title="Employee"
                        pagination={true}
                        columns={[
                            {
                                name: 'Full Name',
                                selector: 'name',
                                sortable: true,
                            },
                            {
                                name: 'Email',
                                selector: 'email',
                                sortable: true,
                            },
                            
                            {
                                name: 'NIK',
                                selector: 'nik',
                                sortable: true,
                            },
                            {
                                name: 'Action',
                                selector: 'employee_id',
                                cell: row => <span>
                                    <button className="btn btn-primary icon-only" onClick={() => this.props.history.push("/dashboard/presence/"+row.user_id)}><i class="far fa-calendar-check"></i> </button>
                                    <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/employee/edit/"+row.user_id)}><i className="far fa-edit"></i></button>
                                    <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.user_id)}><i className="fas fa-trash"></i></button>
                                    </span>
                            }
                            ]}
                        data={employee}
                        striped={true}
                        customStyles={styleTable}
                    />
                }   
                                    </div>      
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
                <div className="modal-wrapper" id="employeeWrapper">
                    <span className="modal-outsider"></span>
                    <div className="modal-wrapper-inner">
                        <div className="modal">
                        <div className="modal-header">
                            Upload CSV Employee
                        </div>
                        <div className="modal-content">
                            <div className="form-group">
                                <label htmlFor="#" className="form-label">Upload CSV</label>
                                <input type="file" className="dropify"  id="id_service_image" name="template" ref={(ref)=>this.fileupload = ref} onChange={this.handleUploadCsv(this.fileupload,'uploadcsv')}  />
                            </div> 
                        </div>
                        <div className="modal-footer">
                            {
                                this.props.employee.isLoading ?
                                <div className="button-group d-flex" >
                                    <span style={{color:"#626262"}}>Importing Data ...</span>
                                </div> :
                                <div className="button-group d-flex">
                                <button className="btn btn-link">cancel</button>
                                <button className="btn btn-info" onClick={() => this.handleSubmitUploadCsv()}>Process</button>
                                </div>
                            }
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        employee : state.employee
    };
  }
const mapDispatchToProps = (dispatch)=>{
    
    return {
        onChangeProps:(prop, name)=>{
            dispatch(employeeAction.onChangeCsv(name, prop.files[0]));
        },
        getEmployee: (employee) => {
            dispatch(employeeAction.getEmployee(employee))
        },
        handlePageClick:(event, employee)=>{
            dispatch(employeeAction.changePageEmployee(event, employee));
        },
        handleChangeFilter:(prop,event)=>{
            dispatch(employeeAction.onChangeFilter(prop, event));
            
        },
        handleSubmitFilter:(employee,payload)=>{
            dispatch(employeeAction.handleSubmitFilter(employee,payload))
        },
        deleteEmployeeById:(id, employee)=>{
            dispatch(employeeAction.deleteEmployeeById(id, employee))
        },
        uploadCsvEmployee:(form, props)=>{

            dispatch(employeeAction.uploadCsvEmployee(form, props));
            // alert(' Importing Data')
            // $('.modal-wrapper').removeClass('modal-active');
            // $('.modal').removeClass('modal-active');
        },
        resetState:()=>{
            dispatch(employeeAction.resetState())
        },
        
    }
}
  const employeePage = connect(mapStateToProps,mapDispatchToProps, null, {
    pure: false
  })(Employee);
export default employeePage;
