import axios from 'axios';
import config from '../config/config';

export const presenceServices = {
    get
};

function get(apiEndpoint){
    return axios.get(config.baseUrl+apiEndpoint+'?company_id=1', {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        return err
    })
}