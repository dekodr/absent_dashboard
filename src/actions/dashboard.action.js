import { dashboardService } from '../services';
import { history } from '../helpers';

export const dashboardAction = {
    getVoucherRedeemByLocation,
    getActivePromo,
    getBroadcaster,
    getTotalHit,
    getTopVoucher,
    getTopVoucherLabel,
    getTopProduct,
    getTopSales,
    getVoucherRedeemByLocation,
    getVoucherRedeemByCampaign
};
function getVoucherRedeemByLocation(){
    return dispatch => {
        dashboardService.getVoucherRedeemByLocation()
        .then((response)=>{
            dispatch(getVoucherRedeemByLocationHandler(response));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
export function getVoucherRedeemByLocationHandler(voucher){
    console.log(voucher)
    return{
        type: "GET_VOUCHER_REDEEM_BY_LOCATION",
        voucher_redeem_by_location: voucher.voucher
    }
}
function getVoucherRedeemByCampaign(){
    return dispatch => {
        dashboardService.getVoucherRedeemByCampaign()
        .then((response)=>{
            dispatch(getVoucherRedeemByCampaignHandler(response));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
export function getVoucherRedeemByCampaignHandler(voucher){
    return{
        type: "GET_VOUCHER_REDEEM_BY_CAMPAIGN",
        voucher_redeem_by_campaign: voucher.voucher
    }
}
function getActivePromo(){
    return dispatch => {
        dashboardService.getActivePromo()
        .then((response)=>{
            dispatch(getActivePromoHandler(response));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}

function getBroadcaster(){
    return dispatch => {
        dashboardService.getBroadcaster()
        .then((response)=>{
            dispatch(getBroadcasterHandler(response));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getTotalHit(){
    return dispatch => {
        dashboardService.getTotalHit()
        .then((response)=>{
            dispatch(getTotalHitHandler(response));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getTopVoucher(){
    return dispatch => {
        dashboardService.getTopVoucher()
        .then((response)=>{
            // console.log(response, "<><><>")
            dispatch(getTopVoucherHandler(response));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getTopVoucherLabel(top_program_hit, top_program_redeem, id){
    return dispatch => {
        dashboardService.getTopVoucherLabel(id)
        .then((response)=>{
            
            dispatch(getTopVoucherLabelHandler(response.data.data, top_program_hit, top_program_redeem));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getTopProduct(){
    return dispatch => {
        dashboardService.getTopProduct()
        .then((response)=>{
            dispatch(getTopProductHandler(response.data.product));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getTopSales(){
    return  (async dispatch => {
        await dashboardService.getTopSales()
        .then((response)=>{
            console.log(response.data)
             dispatch(getTopSalesHandler(response.data.sales_code, response.data.total_hit))
        }).catch((err)=>{
            console.log("Error sales");
            console.log(err);
        })
    });
}
export function getTopSalesHandler(data, total){
    let sales = []
    let top_sales_redeem = []
    let top_sales_hit = []
    if(typeof data!='undefined'){
        data.slice(0, 20).map((value, key)=>{
            
            sales[key] = value['name']
            top_sales_redeem[key] = value['redeem']
            top_sales_hit[key] = value['count']
        })
    }
    
    return{
        type: "GET_TOP_SALES",
        data: data,
        total: total,
        sales: sales,
        top_sales_redeem: top_sales_redeem,
        top_sales_hit:top_sales_hit
    }
}
export function getTopProductHandler(data){
    return{
        type: "GET_TOP_PRODUCT",
        data: data
    }
}
export function getActivePromoHandler(active_promo){
    return{
        type: "GET_ACTIVE_PROMO",
        active_promo: active_promo
    }
}

export function getBroadcasterHandler(broadcaster){
    return{
        type: "GET_BROADCASTER",
        broadcaster: broadcaster
    }
}

export function getTopVoucherLabelHandler(program,top_program_hit, top_program_redeem){
    return{
        type: "TOP_PROGRAM_LABEL",
        program: program,
        top_program_redeem: top_program_redeem,
        top_program_hit:top_program_hit
    }
}
export function getTopVoucherHandler(top_voucher){
    let program = []
    let top_program_redeem = []
    let top_program_hit = []
    if(typeof top_voucher!='undefined'){
        top_voucher.data.voucher.slice(0, 5).map((value, key)=>{
            console.log(value)
            program[key] = value['name']
            top_program_redeem[key] = value['redeem']
            top_program_hit[key] = value['count']
        })
    }
    return{
        type: "TOP_VOUCHER",
        program: program,
        top_program_redeem: top_program_redeem,
        top_program_hit:top_program_hit
    }
    
}
export function getTotalHitHandler(total_hit){
    // let total = 0;
    // total_hit.map(value=>{
    //     if(typeof value._source.hit != 'undefined'){
    //         total+=value._source.hit.length
    //     }
        
    // })
    // console.log(total)
    return{
        type: "GET_TOTAL_HIT",
        total_hit: total_hit
    }
}
