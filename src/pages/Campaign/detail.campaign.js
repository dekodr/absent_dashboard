import React from 'react';
import { connect } from 'react-redux';
import Message from "../../components/Message"
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";

import {campaignAction,categoryAction,employeeGroupAction,voucherAction,customer_groupAction, message} from '../../actions';
import $ from 'jquery';
import DatePicker from 'react-date-picker';
import "../../../node_modules/dropify/src/sass/dropify.scss";
// import Moment from 'react-moment';
import moment, { Moment } from "moment";
class InsertCampaign extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
            is_edit: false,
            seconds: 5
        };
        this.timer = 0
		this.startTimer = this.startTimer.bind(this);
		this.countDown = this.countDown.bind(this);
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(campaignAction.onChangeProps(prop, event));
    };
    startTimer() {
        if (this.timer == 0 && this.state.seconds > 0) {
          this.timer = setInterval(this.countDown, 1000);
        }
    }
    countDown() {
        const { dispatch} = this.props;
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
        //   time: this.secondsToTime(seconds),
          seconds: seconds,
        });
        console.log(seconds)
        // Check if we're at zero.
        if (seconds == 0) { 
            dispatch(campaignAction.removeAlert())
          clearInterval(this.timer);
        }
    }
    handleUpload = (prop, name) => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeImage(prop.files[0], name));
    };
	componentDidMount(){
        const { match : {params, path }, dispatch } = this.props;
        
        dispatch(categoryAction.getCategory());
        dispatch(campaignAction.getCampaign());
        if(params.id){
            dispatch(campaignAction.getCampaignById(params.id));
            
        }
        if(path=="/dashboard/campaign/edit/:id"){
            this.setState({is_edit:true})
        }
        
        
        $('.tabs-group').mouseover(function() {

            var tab_group = $(this).attr('id');
            $(document).on('click', '#'+tab_group+' ul.nav-tabs li', function(){
        
            // $().click(function(){
              var tab_id = $(this).attr('data-tab');
        
              $('#'+tab_group+' ul.nav-tabs li').removeClass('current');
              $('#'+tab_group+ ' .tab-pane').removeClass('current');
        
              $(this).addClass('current');
              $('#'+tab_group+" #"+tab_id).addClass('current');
            })
          })
    }
    componentDidUpdate(prevProps){
        const { match : {params, path }, dispatch } = this.props;
        
        if(this.props.campaign.status!=undefined && prevProps.campaign.status!=this.props.campaign.status){
            if(path=="/dashboard/campaign/edit/:id"){
                this.setState({is_edit:true})
                dispatch(employeeGroupAction.getEmployeeGroup(params.id));
                dispatch(customer_groupAction.getCustomer_group(params.id));
                console.log(this.props)
                if(this.props.campaign.status==2){
    
                    dispatch(voucherAction.getVoucherByCampaign(params.id));
                    
                }else{
                    dispatch(voucherAction.getVoucher());
                }
                
            }else{
                dispatch(employeeGroupAction.getEmployeeGroupSelected(params.id));
                dispatch(customer_groupAction.getCustomer_group_selected(params.id));
                dispatch(voucherAction.getVoucherByCampaign(params.id));
            }
        }
    }
    handleUpdate(event){
        const { match : {params }, campaign, voucher} = this.props;
        const { dispatch } = this.props;
            
        let payload={
            category_id:        campaign.category_id,
            name:               campaign.name,
            from_date:          campaign.from_date,
            to_date:            campaign.to_date,
            fee:                campaign.fee,
            total:              campaign.total,
            total_voucher:      campaign.total_voucher,
            max_per_day:        campaign.max_per_day,
            max_per_month:      campaign.max_per_month,
            voucher_list_id:    campaign.voucher_list_id,
            list_employee_group:campaign.list_employee_group,
            list_customer_group:campaign.list_customer_group,
            customer_group_id:  campaign.customer_group_id,
            banner:             campaign.banner,
            _voucher:           campaign._voucher,
            option_request:     campaign.option_request,
            maximum_request:    campaign.maximum_request,
            voucher:            voucher.voucher,
            messages:           campaign.messages,
			wrong_code:         campaign.wrong_code,
			employee_no_promo:  campaign.employee_no_promo,
			empty_slot:         campaign.empty_slot,
			wrong_index:        campaign.wrong_index,
			max_reached:        campaign.max_reached,
			no_campaign:        campaign.no_campaign,
			empty_voucher:      campaign.empty_voucher,
			last_voucher:       campaign.last_voucher,
			employee_greeting:  campaign.employee_greeting,
            employee_mesages:   campaign.employee_mesages,
            employee_greeting_img: campaign.employee_greeting_img,
        }
        dispatch(campaignAction.editCampaignInfo(params.id, payload, this.props));
        
    }
    handlePublish = ()=>{
        const { dispatch,match : {params }} = this.props;
        dispatch(campaignAction.handlePublish(params.id, this.props));
    }
    handeUnpublish=()=>{
        const { dispatch,match : {params }} = this.props;
        dispatch(campaignAction.handleUnpublish(params.id, this.props));
    }
    handleChangeCheckbox = prop => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeCheckbox(prop, event));
    }
    handleChangeCheckboxCustomer = prop => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeCheckboxCustomer(prop, event));
    };
    handleChangeNumber = (prop,key) => event =>{
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeNumber(prop, event, key));
    }
    handleChangeOptionRequest = (key) => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeOptionRequest('option_request', event, key));
    };
    setRequestValueDefault(campaign, _key){
        if(typeof campaign.max_per_day[_key] != 'undefined'){
            return this.props.campaign.max_per_day[_key]
        } else if(typeof campaign.max_per_month[_key] !=  'undefined'){
            return this.props.campaign.max_per_month[_key]
        }else{
            return ''
        }                                                                
    }
    setRequestValue(campaign, _key, value){
        if(typeof value!='undefined'){
            return value
        }else{
            if(typeof campaign.max_per_day[_key] != 'undefined'){
                return this.props.campaign.max_per_day[_key]
            } else if(typeof campaign.max_per_month[_key] !=  'undefined'){
                return this.props.campaign.max_per_month[_key]
            }else{
                return null
            }   
        }
                                                                     
    }
    
    setRequestValueOption(campaign, _key, value){
        if(typeof value!='undefined'){
            return value
        }else{
            if(typeof campaign.max_per_day[_key] != 'undefined'){
                return 1
            } else if(typeof campaign.max_per_month[_key] !=  'undefined'){
                return 2
            }else{
                return null
            }   
        }
                                                                     
    }
    onChangeFromDate = (field, date) => {
        console.log(field, date)
        this.props.dispatch(campaignAction.onChangeFromDate(field,date));
    }
    setRequestOption(campaign, _key){
        if(typeof campaign.max_per_day[_key] != 'undefined'){
            return 1
        } else if(typeof campaign.max_per_month[_key] !=  'undefined'){
            return 2
        }                                                               
    }
    
	render(){
        $('.dropify').dropify();
        const { match : {params }} = this.props;
        const { message : {status_message, message} } = this.props.campaign
        const { category } = this.props.category;
        const { employee } = this.props.employee;
        const { voucher } = this.props.voucher;
        const { employee_group } = this.props.employeegroup;
        const {customer_group} = this.props.customer_group;
        
        let _from_date = null
        let _to_date = null
        let title = ''
        let disable = ''
        
        if(this.state.is_edit){
            title = 'Edit'
            disable ='form-control'
            if(typeof this.props.campaign.from_date!= "undefined" &&this.props.campaign.from_date!='' &&this.props.campaign.from_date!=null){
                _from_date = moment(this.props.campaign.from_date, moment.defaultFormat).toDate();

            }
            if(typeof this.props.campaign.to_date!= "undefined" &&this.props.campaign.to_date!=''&&this.props.campaign.to_date!=null){
                _to_date = moment(this.props.campaign.to_date, moment.defaultFormat).toDate();
            }
        }else{
            title = 'Review'
            disable ='form-control disable'
            if(typeof this.props.campaign.from_date!= "undefined"){
                _from_date = moment(this.props.campaign.from_date).format('YYYY-MM-DD');
            }
            if(typeof this.props.campaign.to_date!= "undefined"){
                _to_date = moment(this.props.campaign.to_date).format('YYYY-MM-DD');
            }
        }
        console.log(this.props.campaign.default_employee_greeting_img)
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">{title} Campaign</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Campaign</div>
                            <div className="breadcrumb-item">{title} Campaign</div>
						</div>
					</div>
				</div>
                
                <div className="p-20-c">
               
                
                    <div className="row">
                        <div className="col-12">
                        
                            <div className="card" style={{minHeight: "450px"}}>
                                <div className="card-body">
                                    <h2 className="m-20-vc">{title} Campaign</h2>
                                    <div className="card-content">

                                        
                                            <div className="row">
                                                <div className="col-4">
                                                    <div className="form-group" style={{textAlign: "center"}}>
                                                        <img src={this.props.campaign.default_banner} style={{width: '100%'}}></img>
                                                    </div>
                                                    {
                                                        this.state.is_edit ? <div className="form-group">
                                                            <label htmlFor="#" className="form-label">Banner</label>
                                                            <input type="file" className="dropify" name="template" ref={(ref)=>this.fileupload = ref} onChange={this.handleUpload(this.fileupload,'banner')} />
                                                        </div> : null
                                                    }
                                                </div>     
                                                <div className="col-8">
                                                    {
                                                        this.state.is_edit ? <div className="form date-picker">	
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="#" className="form-label">From Date</label>
                                                                        <div className="date-picker">
                                                                            <DatePicker
                                                                            onChange={(date)=>this.onChangeFromDate('from_date', date)}
                                                                            value={_from_date}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>	
                                                                <div className="col-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="#" className="form-label">To Date</label>
                                                                        <div className="date-picker">
                                                                            <DatePicker
                                                                            onChange={(date)=>this.onChangeFromDate('to_date', date)}
                                                                            value={_to_date}
                                                                            />
                                                                        </div>
                                                                    </div>	
                                                                </div>
                                                            </div>	
                                                        </div> : null
                                                    }
                                                    
                                                    <form className="form">
                                                    {
                                                        this.state.is_edit==false ? 
                                                        <div><div className="form-group">
                                                            <label htmlFor="#" className="form-label">From Date</label>
                                                            <input type="date" value={_from_date} className={disable} 
                                                            onChange={this.handleChange('from_date')} disabled={!this.state.is_edit}/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label htmlFor="#" className="form-label">To Date</label>
                                                            <input type="date" value={_to_date} className={disable} 
                                                            onChange={this.handleChange('to_date')} disabled={!this.state.is_edit}/>
                                                        </div></div> : null 
                                                    }
                                                    <div className="form-group">
                                                        <label htmlFor="#" className="form-label">Campaign Name</label>
                                                        <input type="text" value={this.props.campaign.name} className={disable} 
                                                        onChange={this.handleChange('name')} disabled={!this.state.is_edit}/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="#" className="form-label">INCENTIVE</label>
                                                        <input type="number" inputMode="numeric" pattern="[0-9]*" type="tel" value={this.props.campaign.fee} className={disable} 
                                                        onChange={this.handleChange('fee')} disabled={!this.state.is_edit}/>
                                                    </div>
                                                    {/* <div className="form-group">
                                                        <label htmlFor="#" className="form-label">Campaign Messages</label>
                                                        <input type="text" value={this.props.campaign.messages} className={disable} 
                                                        onChange={this.handleChange('messages')} disabled={!this.state.is_edit}/>
                                                    </div> */}
                                                    {/* <div className="form-group">
                                                        <label htmlFor="#" className="form-label">Total Voucher</label>
                                                        <input type="text" value={this.props.campaign.total_voucher} className={disable} 
                                                        onChange={this.handleChange('total_voucher')} disabled={!this.state.is_edit}/>
                                                    </div> */}
                                                    </form>
                                                </div> 
                                            </div>       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <form className="form">
                                    <div className="card-body">
                                        <div className="tabs-group" id="tg-1">
                                            <ul className="nav-tabs">
                                                <li className="tab-item current" data-tab="tab-1" >White List Employee Group</li>
                                                <li className="tab-item" data-tab="tab-2">White List Customer Group</li>
                                                <li className="tab-item" data-tab="tab-3">Voucher</li>
                                                <li className="tab-item" data-tab="tab-4">Message</li>
                                            </ul>
                                            <div className="tab-content">
                                                <div id="tab-1" className="tab-pane current">
                                                    <div className="col-12">
                                                    <table className="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Group Name</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            employee_group.length >0 ? (
                                                                employee_group && employee_group.map((data,key) => {
                                                                    let checked = false;
                                                                    if(typeof this.props.campaign.list_employee_group !='undefined'){
                                                                        if(this.props.campaign.list_employee_group.includes(data.employee_group_id)){
                                                                            checked = true
                                                                        }
                                                                    }
                                                                    console.log()
                                                                    return (
                                                                        <tr key={key}>
                                                                            <td>{
                                                                                (this.state.is_edit&&this.props.campaign.status!=2) ? <input type="checkbox" checked={checked} value={data.employee_group_id} 
                                                                                className="form-control" onChange={this.handleChangeCheckbox('list_employee_group')}/>:null }</td>
                                                                            <td>{data.name}</td>
                                                                        </tr>
                                                                    )
                                                                    
                                                                })
                                                            )  : (
                                                                <tr>
                                                                    <td colSpan="4" align="center">Tidak Ada Data</td>
                                                                </tr>
                                                            )
                                                        }
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div id="tab-2" className="tab-pane">
                                                    <div className="col-12">
                                                        <table className="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Customer Group Name</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                (typeof customer_group!=='undefined' && (this.props.campaign.customer_group_id.length > 0 || this.state.is_edit)) ? (
                                                                    customer_group.length >0 ? (
                                                                        customer_group && customer_group.map(data => {
                                                                            let checked = false;
                                                                            if(typeof this.props.campaign.customer_group_id !='undefined'){
                                                                                if(this.props.campaign.customer_group_id.includes(data.customer_group_id)){
                                                                                    checked = true
                                                                                }
                                                                            }
                                                                            return (
                                                                                <tr key={data.customer_group_id}>
                                                                                    <td>
                                                                                        {
                                                                                            this.state.is_edit ? <input type="checkbox" checked={checked} value={data.customer_group_id} 
                                                                                            id={data.customer_group_id}  className="form-control" onChange={this.handleChangeCheckboxCustomer('customer_group_id')}/> : null
                                                                                        }
                                                                                        
                                                                                    </td>
                                                                                    <td>{data.name}</td>
                                                                                </tr>
                                                                            )
                                                                            
                                                                        })
                                                                    )  : (
                                                                        <tr>
                                                                            <td colSpan="4" align="center">Tidak Ada Data</td>
                                                                        </tr>
                                                                    )
                                                                ):<tr>
                                                                <td colSpan="4" align="center">Tidak Ada Data</td>
                                                            </tr>
                                                                
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div id="tab-3" className="tab-pane">
                                                    <table className="table table-striped">

                                                        <thead>
                                                            <tr>
                                                            
                                                                <th>#</th>
                                                                <th>Voucher Name</th>
                                                                <th >Image</th>
                                                                <th style={{width: '40px'}}>Total Voucher</th>
                                                                <th>Voucher Request Limit</th>
                                                                
                                                            
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            
                                                            voucher && voucher.map((data, key) => {
                                                            
                                                                let checked = false;
                                                                let _key = null;
                                                                
                                                                if(typeof this.props.campaign._voucher !='undefined'){
                                                                    if(this.props.campaign._voucher.includes(data.voucher_id)){
                                                                        checked = true
                                                                    }
                                                                    _key = Object.keys(this.props.campaign._voucher).find(key => this.props.campaign._voucher[key] === data.voucher_id)
                                                                    
                                                                }
                                                                
                                                            
                                                                return (
                                                                    <tr key={key}>
                                                                        <th>
                                                                            {
                                                                                (this.state.is_edit&&this.props.campaign.status!=2) ?
                                                                                <input type="checkbox" checked={checked} value={data.voucher_id} 
                                                                                className="form-control" onChange={this.handleChangeCheckbox('_voucher')}/> : null
                                                                            }
                                                                            
                                                                        </th>
                                                                        <td>{data.voucher_name}</td>
                                                                        <td>{(this.state.is_edit&&this.props.campaign.status!=2)? <img src={data.voucher_image}  height="80" /> : <img src={data.image}  height="80" /> }</td>
                                                                        <td>{data.total_voucher}</td>
                                                                        {
                                                                            (this.state.is_edit&&this.props.campaign.status!=2) ? 
                                                                                <td >
                                                                                    <div className="form-group col-6" style={{ margin: '5px', display: 'table-cell',  width: '100px'}}>
                                                                                            <input type="text" defaultValue={this.setRequestValueDefault(this.props.campaign, _key)}
                                                                                        value={this.setRequestValue(this.props.campaign, _key, this.props.campaign.maximum_request[key])} 
                                                                                        className="form-control" 
                                                                                        onChange={this.handleChangeNumber('maximum_request', key)}/>
                                                                                    </div>
                                                                                    <select 
                                                                                        className="form-control col-4" 
                                                                                        defaultValue={this.setRequestOption(this.props.campaign, _key)} 
                                                                                        style={{  margin: '5px', display: 'table-cell',height:'30px',background:'white'}}
                                                                                        onChange={this.handleChangeOptionRequest(key)}
                                                                                        value={this.setRequestValueOption(this.props.campaign, _key, this.props.campaign.option_request[key])}
                                                                                    >
                                                                                        <option value="1">request per day</option>
                                                                                        <option value="2">request per month</option>
                                                                                    </select>
                                                                                        
                                                                                </td> : <td >
                                                                                    {
                                                                                        
                                                                                        (typeof data.max_per_day == 'undefined') ? <p>{data.max_per_month} request per month</p> :
                                                                                        <p>{data.max_per_day} request per day</p>
                                                                                    }
                                                                                        
                                                                                </td>
                                                                        }
                                                                        
                                                                        {/* <td>{
                                                                                this.state.is_edit ? <div className="form-group">
                                                                                    <input type="text" defaultValue={this.props.campaign.limit[_key]} className="form-control" 
                                                                                    onChange={this.handleChangeNumber('total', key)}/>
                                                                                </div> : <p>{data.limit} vouchers</p>
                                                                            }
                                                                            
                                                                        </td> */}
                                                                    </tr>
                                                                )
                                                                
                                                            })
                                                        }
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div id="tab-4" className="tab-pane">
                                                <form className="form">
													<div className="form-group col-12">
														<label htmlFor="#" className="form-label">Employee Greeting Image</label>
                                                        <div style={{width: '100%'}}>
                                                            <img src={this.props.campaign.default_employee_greeting_img} style={{width: '300px', marginTop:'20px', textAlign:'center'}}></img>
                                                        </div>
                                                        {
                                                            this.state.is_edit ? <div className="form-group">
                                                                <label htmlFor="#" className="form-label">Employee Greeting Image</label>
                                                                <input type="file" className="dropify" name="template" ref={(ref)=>this.fileuploadgreeting = ref} onChange={this.handleUpload(this.fileuploadgreeting,'employee_greeting_img')} />
                                                            </div> : null
                                                        }
													</div>
												</form>
                                                <div style={{color: 'grey', margin: '10px', padding: '10px 10px 10px 30px', borderLeft: '3px solid #3FB5A7', position: 'sticky', top: '0',}}>
                                                    <ul style={{listStyleType: "disc"}}>
                                                        <li>
                                                            use *text* to get <b style={{fontWeight: "bold"}}>text</b>
                                                        </li>
                                                        <li>
                                                            use _text_ to get <i>text</i>
                                                        </li>
                                                        <li>
                                                            use text to get <strike>text</strike>
                                                        </li>
                                                        <li>
                                                            Use &#123;salescode&#125; to get individual sales promo code
                                                        </li>
                                                    </ul>
                                                </div>
                                                
												<table className="table table-striped">
                                                    <tbody>
														<tr><td colSpan="2" style={{textAlign: "center", fontWeight: "bold"}}>*MESSAGE TO EMPLOYEE</td></tr>
														<tr>
															<td>Employee welcome</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea disabled={!this.state.is_edit} style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_greeting} className="form-control" 
																onChange={this.handleChange('employee_greeting')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Forwardable Message for employee</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																	<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_mesages} className="form-control" 
																	onChange={this.handleChange('employee_mesages')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Employee cannot request promo warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_no_promo} className="form-control" 
																onChange={this.handleChange('employee_no_promo')}/>	
																</div>
															</td>
														</tr>
														
														<tr><td colSpan="2"  style={{textAlign: "center", fontWeight: "bold"}}>*MESSAGE TO CUSTOMER</td></tr>
														<tr>
															<td>Campaign greeting messages</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																	<textarea  disabled={!this.state.is_edit} style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  className="form-control" onChange={this.handleChange('messages')} value={this.props.campaign.messages}></textarea>
																</div>
															</td>
														</tr>
														<tr>
															<td>User request the last voucher on the campaign</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.last_voucher} className="form-control" 
																onChange={this.handleChange('last_voucher')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Wrong referal code warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.wrong_code} className="form-control" 
																onChange={this.handleChange('wrong_code')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>User have request voucher warning </td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.empty_slot} className="form-control" 
																onChange={this.handleChange('empty_slot')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Wrong voucher index warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.wrong_index} className="form-control" 
																onChange={this.handleChange('wrong_index')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Voucher is on maximum daily / monthly hit warning </td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.max_reached} className="form-control" 
																onChange={this.handleChange('max_reached')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>No promo or promo have been expired warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.no_campaign} className="form-control" 
																onChange={this.handleChange('no_campaign')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Empty voucher warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  disabled={!this.state.is_edit}  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.empty_voucher} className="form-control" 
																onChange={this.handleChange('empty_voucher')}/>	
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-auto right">
                            <div className="card">
                                <div className="card-body">

                            	    <div className="button-row">
                                        <div className="btn-group" style={{justifyContent: 'space-between', textAlign: 'right'}} >

                                            {
                                                this.state.is_edit ? <div><button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleUpdate(event)}><i className="fas fa-pen"></i>Edit</button><button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> </div>: 
                                                (this.props.campaign.status!=1 ? <div>
                                                    <button 
                                                        type="button" 
                                                        className="btn btn-primary" 
                                                        onClick={(event) => this.handlePublish(event)}>
                                                            <i className="fas fa-paper-plane"></i>Publish
                                                    </button>
                                                    <button 
                                                        className="btn btn-secondary" 
                                                        onClick={() => this.props.history.push('/dashboard/campaign')}>
                                                            Cancel
                                                    </button>
                                                </div>:
                                                <div>
                                                    <button 
                                                        type="button" 
                                                        className="btn btn-danger" 
                                                        onClick={(event) => this.handeUnpublish(event)}>
                                                            <i className="fas fa-times"></i>Unpublish
                                                    </button>
                                                    <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Back</button>  
                                                </div>
                                                
                                                )
                                            }

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{
    return state
}
const InsertCampaignPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(InsertCampaign));
export default InsertCampaignPage;
