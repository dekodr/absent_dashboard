import React, { useState } from 'react';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import DataTable, { createTheme } from 'react-data-table-component';
import { employeeGroupAction } from '../../actions';
import "../../asset/css/pagination.css";
import "../../asset/css/card.css";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Employee_group extends React.Component{
    constructor(props){
        super(props);
    }
    
    componentDidMount(){
        this.props.resetState()
        this.props.getEmployeeGroup()
    }
    handleChangeFilter = prop => event => {
        this.props.handleChangeFilter(prop, event)
    };
    handleClick = (event, id) => {
        // console.log(id);
        const { dispatch, employee_group } = this.props;

        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            this.props.deleteEmployeeGroup(id);
        }
    }
    handleSubmitFilter = ()=>{
        const { dispatch, employee_group } = this.props;

        let payload={
            first_name  : employee_group.form_filter.first_name,
            last_name   : employee_group.form_filter.last_name,
            phone_no    : employee_group.form_filter.phone_no
        }
        this.props.handleSubmitFilter(employee_group,payload)
        
    }
    handlePageClick = (event)=>{
        const { dispatch, employee_group } = this.props;
        this.props.handlePageClick(event, employee_group)
        
    }
    render(){
        const { employee_group, total_page, total } = this.props.employeegroup;
        console.log(this.props)
        return (
            <div>
                
                    <div className="button-row">
                        <div className="btn-group" >
                            <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/employee_group/insert")}><i className="far fa-plus-square"></i>Create</button>
                        </div>
                    </div>
                    <DataTable
                        title="Employee Group"
                        pagination={true}
                        columns={[
                            {
                                name: 'Group Name',
                                selector: 'name',
                                sortable: true,
                            },
                            {
                                name: 'Action',
                                selector: 'employee_group_id',
                                cell: row => <span>
                                    <button className="btn btn-warning  icon-only" onClick={() => this.props.history.push("/dashboard/employee_group/edit/"+row.employee_group_id)}><i className="far fa-edit"></i></button>
                                    <button className="btn btn-primary  icon-only" onClick={() => this.props.history.push("/dashboard/employee_group/detail/"+row.employee_group_id)}><i className="fas fa-info"></i></button>
                                    <button className="btn btn-danger  icon-only" onClick={(event) => this.handleClick(event, row.employee_group_id)}><i className="fas fa-trash"></i></button>
                                    
                                    </span>
                            }
                            ]}
                        data={employee_group}
                        striped={true}
                        customStyles={styleTable}
                    />
                    
                
            </div>      
           
        );
    }
}
const mapStateToProps = (state) =>{
    // console.log(state)
    return {
        employeegroup : state.employeegroup
    };
  }
const mapDispatchToProps = (dispatch)=>{
    return {
        getEmployeeGroup:()=>{
            dispatch(employeeGroupAction.getEmployeeGroup());
        },
        deleteEmployeeGroup:(id)=>{
            dispatch(employeeGroupAction.deleteEmployeeGroup(id));
        },
        resetState:()=>{
            dispatch(employeeGroupAction.resetState())
        }
    }
}
    
//     return {
//         getEmployeeGroup: (employee_group) => {
//             dispatch(employeeGroupAction.getEmployeeGroup(employee_group))
//         },
//         handlePageClick:(event, employee_group)=>{
//             dispatch(employeeGroupAction.changePageEmployee(event, employee_group));
//         },
//         handleChangeFilter:(prop,event)=>{
//             dispatch(employeeGroupAction.onChangeFilter(prop, event));
            
//         },
//         handleSubmitFilter:(employee,payload)=>{
//             dispatch(employeeGroupAction.handleSubmitFilter(employee,payload))
//         }
//     }
// }
  const Employee_group_page = connect(mapStateToProps,mapDispatchToProps, null, {
    pure: false
  })(Employee_group);
export default Employee_group_page;
