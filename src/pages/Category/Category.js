import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
// import {transactionService} from '../api/transaction.js';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import { categoryAction } from '../../actions';
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Category extends React.Component{
	constructor(props){
		super(props);
	}
	
	componentDidMount(){
        const { dispatch } = this.props;
        dispatch(categoryAction.resetState());
        dispatch(categoryAction.getCategory());
	}

    handleClick = (event, id) => {
        const { dispatch } = this.props;
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            dispatch(categoryAction.deleteCategoryById(id));
        }
    }

    handleChangeFilter = prop => event => {
        const { dispatch } = this.props;

        dispatch(categoryAction.onChangeFilter(prop, event));
    };
    
	render(){
       
        const { category } = this.props.category;
       
		return (
			<div>             
                <div className="button-row">
                    <div className="btn-group" >
                        <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/category/insert")}><i className="far fa-plus-square"></i>Create</button>
                    </div>
                </div>
                {
                    <DataTable
                        title="Product Category" 
                        pagination={true}
                        columns={[
                            {
                                name: 'Product Category',
                                selector: 'category_name',
                                sortable: true,
                            },
                            {
                                name: 'Action',
                                selector: 'category_id',
                                cell: row => <span>
                                    <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/category/edit/"+row.category_id)}><i className="far fa-edit"></i></button>
                                    <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.category_id)}><i className="fas fa-trash"></i></button>
                                    </span>
                            }
                            ]}
                        data={category}
                        striped={true}
                        customStyles={styleTable}
                    />
                }
            </div>      
	  	);
	}
}
const mapStateToProps = (state) =>{
    return {
        category : state.category
    };
  }

  const CategoryPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
  })(Category));
export default CategoryPage;
