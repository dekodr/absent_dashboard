const initialState = { anchor: 'left',
    customer_group: [],
    open: false,
    employee_page: 0,
    id: '',  
    name: '',
    phone_number: '',
    cust_list_id:'',
    cust_list_id:[]
    
 };


export function customer_group(state = initialState, action) {
    
    switch (action.type) {
        case 'FETCHED_ALL_CUSTOMER_GROUP':
            return {
            ...state,
            customer_group: action.customer_group
            };
        case 'CustomerGroup_DETAIL':
            // console.log("action", action)
            return {
                ...state,
                id: action.id,  
                name: action.name,
                cust_list_id: action.cust_list_id,
            };
        case "HANDLE_ON_CHANGE_CHECKBOX_CUSTOMER_GROUP":
            // console.log(action)
            let _state = state[action.props]
            if(typeof action.value =='string'){
                action.value = parseInt(action.value)
            }
            if(_state.includes(action.value)==false){
                _state.push(action.value)
            }else{
                let index = _state.indexOf(action.value);
                if (index !== -1) _state.splice(index, 1);
            }
            
            return {
                ...state,
                [action.props]: _state
            };  
        case "CUSTOMER_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };    
        case "MESSAGE":
            // console.log(action)
            return {
                ...state,
                type: action.type,
                message: action.message
            }
        case "HANDLER_GET_CUSTOMER_LIST_BY_GROUP_ID":
            return {
                ...state,
                list_customer: action.customer
            }
        case "RESET_STATE_CUSTOMER_GROUP":
            return initialState
        default:
            return state
    }
  }