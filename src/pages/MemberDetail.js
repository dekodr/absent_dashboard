import React from 'react';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import {transactionService} from '../api/transaction.js';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";

class MemberDetail extends React.Component{
    
	constructor(props){
        
        super(props);
        
		this.state = {
			detail: [],
            transaction_customer:0,
            transaction_emp:0
        };
	}
	
	componentDidMount(){
        // console.log(this.props.match.params.id)
        
        transactionService.get_transaction_employee(this.props.match.params.id).then((data)=>{
            // console.log(data);
            const transaction_month_label = [];
            const transaction_month_data = [];
            const redeem_month_data = [];
            const transaction_customer = data.data.transaction_customer.count;
            const transaction_emp = data.data.transaction_emp.count;
            const transaction_month = data.data.transaction_month;
            const total_redeem = (data.data.total_incentive.count * 1000).toLocaleString().replace(/,/g,'.');
            
            const redeem_month = data.data.redeem_month;
			      this.setState({
                transaction_customer,
                transaction_emp,
                total_redeem
            })
            for (var key in transaction_month) {
                transaction_month_label.push(transaction_month[key][0])
                transaction_month_data.push(transaction_month[key][1]['ct'])
                redeem_month_data.push(redeem_month[key][1]['ct'])

            }
			
            this.setState({
                transaction_month_label,
                transaction_month_data,
                redeem_month_data
			})
		})
   
		
    }
    fetch = () =>{
        let { id } = useParams();
        
    }
	render(){
		const month_bar = {
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            subtitle: {
              text: ''
            },
            xAxis: {
              categories: this.state.transaction_month_label,
              crosshair: true
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Total Hit'
              }
            },
            plotOptions: {
              column: {
                grouping: false,
                shadow: false,
              }
            },
            series: [{
              name: 'Hit Total',
              data: this.state.transaction_month_data,
              pointPadding: 0.3,
            },{
              name: 'Redeem Total',
              data: this.state.redeem_month_data,
              pointPadding: 0.4,
            }]
          }
		return (
			<div className="right-panel">
                <div className="row" id="index-first-row">
						<div className="col-3">
							<div className="card">
								<div className="card-body">
									<div id="title">Active Promo</div>
                                    <div id="number">{this.state.transaction_emp}</div>
									<div id="icon"></div>
								</div>
							</div>
						</div>	
						<div className="col-3">
							<div className="card">
								<div className="card-body">
									<div id="title">Hit Number</div>
									<div id="number">{this.state.transaction_customer}</div>
									<div id="icon"></div>
								</div>
							</div>
						</div>	
            <div className="col-3">
							<div className="card">
								<div className="card-body">
									<div id="title">Incentive</div>
									<div id="number">Rp{this.state.total_redeem}</div>
									<div id="icon"></div>
								</div>
							</div>
						</div>	
					</div>

					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="box-title">Monthly Hit</div>
									<HighchartsReact
                                        highcharts={Highcharts}
                                        options={month_bar}
                                    />
								</div>
							</div>		
						</div>
                    </div>

            </div>
	  	);
	}
}

export default withRouter(MemberDetail);
