const initialState = { anchor: 'left',
    category: [],
    open: false,
    category_id: '',  
    category_name: ''
 };


export function category(state = initialState, action) {
    switch (action.type) {
        case 'FETCHED_ALL_CATEGORY':
            return {
            ...state,
            category: action.category
            };
        case 'CATEGORY_DETAIL':

            return {
                ...state,
                category_id: action.id,
                category_name: action.category_name,
                is_linked: action.is_linked,
            };
        // case "EMPLOYEE_UPDATED":
        //     return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };    
        
        case "RESET_STATE_EMPLOYEE":
            return initialState

        case "MESSAGE":
            // console.log(action)
            return {
                ...state,
                type: action.type,
                message: action.message
            }
            
        default:
            return state
    }
  }