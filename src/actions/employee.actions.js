import React from 'react';
import { employeeServices } from '../services';
import { history } from '../helpers';

import Message from "../components/Message"
export const employeeAction = {
    getEmployee,
    getEmployeeById,
    onChangeProps,
    editEmployeeInfo,
    createEmployee,
    deleteEmployeeById,
    changePageEmployee,
    onChangeFilter,
    submitFilter,
    handleSubmitFilter,
    onChangeCsv,
    uploadCsvEmployee,
    resetState,
    message,
    onChangeImage,
    onChangeUpload
};

function getEmployee(employee, payload={}){
    if(Object.entries(payload).length !== 0 && payload.constructor === Object){
        employee.submit_filter = payload;
    }
    

    return dispatch => {
        let apiEndpoint = 'user';
        employeeServices.get(apiEndpoint,employee)
        .then((response)=>{
            dispatch(changeEmployeeList(response.data));
        }).catch((err)=>{
            console.log("Error>>");
            console.log(err);
        })
    };
}

function createEmployee(payload, props){
    return dispatch => {
        let apiEndpoint = 'registration';
        console.log("payload>>>",payload)

        employeeServices.post(apiEndpoint, payload)
        .then((response)=>{
            console.log(response);
            dispatch(message("success", response.data.message))
            dispatch(createEmployeeInfo());
            props.history.push('/dashboard/employee');

        }) 
    }
}

function getEmployeeById(id){

    return dispatch => {
        let apiEndpoint = 'user/'+ id;
        employeeServices.get(apiEndpoint,1)
        .then((response)=>{
            dispatch(editEmployeeDetails(response.data.data));
        })
    };
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function onChangeFilter(props, event){
    return dispatch =>{
        dispatch(handleOnChangeFilter(props, event.target.value));
    }
}
function onChangeCsv(props, name){
	return dispatch =>{
		dispatch(handleOnChangeProps(props, name));
	}
}
function editEmployeeInfo(payload,props){
    return dispatch => {
        let apiEndpoint = 'registration';
        employeeServices.put(apiEndpoint, payload)
        .then((response)=>{
            // check response and send feedback
            
            if(response.status == 200 || response.status == 201){
                alert('Success!');
                props.history.push('/dashboard/employee');
            }
            //     dispatch(message("success", response.data.message))
            //     dispatch(updatedUserInfo());
            //     props.history.push('/dashboard/employee');

            //     // redirecting
            //     // props.history.push('/dashboard/employee');
            // }else if(response.message.indexOf(422)){
            //     dispatch(message("error", "Please check again. Required field is empty."))
            // }else{
            //     // dispatch(message("error", response.response.data.message))
            // }

        })
    }
}

function deleteEmployeeById(id, employee){
    return dispatch => {
        let apiEndpoint = 'registration/'+id;
        employeeServices.deleteDetail(apiEndpoint,1)
        .then((response)=>{
            console.log(response);
            // check response and send feedback
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteEmployeeDetails());
                dispatch(employeeAction.getEmployee(employee));
            }
            // else{
                // dispatch(message("error", response.response.data.message))
            // }
        })
    };
}
function changePageEmployee(page, employee){
    let _page = page.selected;
    
    return dispatch => {
       
        dispatch(changePage(_page))
        employee.page = _page;
        dispatch(getEmployee(employee))
    }
    
}
function onChangeImage(props, name){
	return dispatch =>{
		dispatch(handleOnChangeImage(props, name));
	}
}
function onChangeUpload(props, name){
	return dispatch =>{
		dispatch(handleOnChangeUpload(props, name));
	}
}
function uploadCsvEmployee(form,props){
    return dispatch => {
        alert("Importing Data...")
		dispatch(showLoading());
		let apiEndpoint = 'employee/bulk';
	// 	let _payload = {
	// 		'voucher_name': payload.voucher_name,
	// 		'total_voucher':payload.total_voucher,
	// 		'type_voucher':payload.type_voucher,
	// 		'template': payload.template,
	// 		'product_id':payload.product_id,
	// 		'code_csv':payload.code_csv
    // 	}
		employeeServices.post_csv(apiEndpoint, {'uploadcsv':form})
		.then((response)=>{
            console.log(response)
            // dispatch(createEmployeeInfo());
            // dispatch(employeeAction.getEmployee());
            alert('Import Complete!')
			dispatch(hideLoading())
			// dispatch(resetStateHandler())
            // props.history.push('/dashboard/employee');
            window.location.reload()
		}).catch(response=>{
            console.log(response)
        })
	}
}
function handleSubmitFilter(employee,payload){
    employee.page = 0
    return dispatch => {
        dispatch(submitFilter())
        dispatch(changePage(0))
        dispatch(getEmployee(employee,payload))
    }
}

function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}
export function submitFilter(){
    return {
        type: "HANDLE_SUBMIT_FILTER"
    }
}
export function changePage(page){
    return {
        type: "CHANGE_PAGE_EMPLOYEE",
        page: page
    }
}
export function changeEmployeeList(employee){
    return{
        type: "FETCHED_ALL_EMPLOYEE",
        employee: employee.data,
        total:employee.total,
        total_page:employee.total_page

    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function handleOnChangeFilter(props, value){
    return{
        type: "HANDLE_ON_CHANGE_FILTER",
        props: props,
        value: value
    }
}
export function handleOnChangeImage(props, name){
	return{
		type: "HANDLE_ON_CHANGE",
		props: name,
		value: props
	}
}
export function handleOnChangeUpload(props, name){
	return{
		type: "HANDLE_ON_CHANGE",
		props: name,
		value: props
	}
}


export function editEmployeeDetails(employee){
    return{
        type: "EMPLOYEE_DETAIL",
        emp_id: employee.id,
        email: employee.email,
        name: employee.name,
        nik: employee.nik,
        card_id: employee.card_id,
        phone_no: employee.phone_no,
        photo: employee.photo
    }
}
export function resetPage(){
    return{
        type: "RESET_PAGE"
    }
}
export function showLoading(){
    return{
        type: "SHOW_LOADING_EMPLOYEE_ENTRY"
    }
}
export function hideLoading(){
    return{
        type: "HIDE_LOADING_EMPLOYEE_ENTRY"
    }
}
export function updatedUserInfo(){
    return{
        type: "USER_UPDATED"
    }
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}

export function createEmployeeInfo(){
    return{
        type: "EMPLOYEE_CREATED_SUCCESSFULLY"
    }
}

export function deleteEmployeeDetails(){
    return{
        type: "DELETED_VENDOR_DETAILS"
    }
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_EMPLOYEE"
	}
}