const initialState = { anchor: 'left',
	employee: [],
	open: false,
	total:0,
	total_page: 0, 
	id: '',  
	name: '',
	mobile: '',
	phone_number: '',
	address: '',
	form_filter: {},
	submit_filter: {}, 
	page:0
 };


export function employee(state = initialState, action) {
	switch (action.type) {
		case 'FETCHED_ALL_EMPLOYEE':
			return {
			...state,
			employee: action.employee,
			total: action.total,
			total_page: action.total_page
			};
		case 'EMPLOYEE_DETAIL':
			return {
				...state,
				id: action.id,  
				emp_id: action.id,
				name: action.name,
				phone_no: action.phone_no,
				email: action.email,
				nik: action.nik,
				card_id: action.card_id,
				photo: action.photo
			};
		case "EMPLOYEE_UPDATED":
			return state;

		case "CHANGE_PAGE_EMPLOYEE":
			return {
				...state,
				page: action.page
			}
		
		case "HANDLE_ON_CHANGE":
			console.log(action)
			return {
				...state,
				[action.props]: action.value
			};

		case "HANDLE_ON_CHANGE_FILTER":
			console.log(state)
			let _filter = {
				...state['form_filter'],
				[action.props] : action.value}
				// console.log(state)
			return {
				...state,
				form_filter: _filter
			};   
		case "RESET_PAGE":
			return {
				...state,
				page: 1
			};   
		case "HANDLE_SUBMIT_FILTER":
			
			return {
				...state,
				page: 0,
				submit_filter: state['form_filter']
			};
		case "RESET_STATE_EMPLOYEE":
			return initialState

		case "MESSAGE":
			// console.log(action)
			return {
				...state,
				type: action.type,
				message: action.message
			}
		case 'HIDE_LOADING_EMPLOYEE_ENTRY':
			return {
				...state,
				isLoading: false
				};
		case 'SHOW_LOADING_EMPLOYEE_ENTRY':
			return {
			...state,
			isLoading: true
			};
		default:
			return state
	}
  }