import axios from 'axios';
import config from '../config/config';
import S3 from 'react-aws-s3';
import uuid from 'react-uuid'
const config_aws = {
    bucketName: 'telegram-bot-s3',
    dirName: 'promo', /* optional */
    region: 'ap-southeast-1',
    accessKeyId: "AKIAXZBVME4GEGDNABFF",
    secretAccessKey: "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",
    s3Url: 'https://telegram-bot-s3.s3-ap-southeast-1.amazonaws.com', /* optional */
}
const ReactS3Client = new S3(config_aws);
export const campaignServices = {
    get,
    post,
    put,
    deleteDetail,
    publish,
    unpublish
};

function get(apiEndpoint){
    return axios.get(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function publish(apiEndpoint){
    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {}, {
        headers: {
            "Content-Type":"application/json",
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log(err);
    })
}
function unpublish(apiEndpoint){
    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {}, {
        headers: {
            "Content-Type":"application/json",
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log(err);
    })
}
function join_group(list){
    let str = "[";
    str += list.join(',')
    str += "]";
    return str
}
/**
 * Upload field banner
 * @function
 * @param {Object} payload 
 * @returns {string|object}
 */
async function upload_banner(payload){
    const file = {
        uri: payload.banner.uri,
        name: payload.banner.fileName,
        type: payload.banner.type
    }
    
    if(typeof payload.banner=='string'){
        return payload.banner
    }else{
        return ReactS3Client
        .uploadFile(payload.banner, uuid()).then(
            response=>{
                return response
            }
        )
    }
}
/**
 * Upload field banner
 * @function
 * @param {Object} payload 
 * @returns {string|object}
 */
async function upload_employee_greeting_img(payload){
    const file = {
        uri: payload.employee_greeting_img.uri,
        name: payload.employee_greeting_img.fileName,
        type: payload.employee_greeting_img.type
    }
    
    if(typeof payload.employee_greeting_img=='string'){
        
        return payload.employee_greeting_img
    }else{
        return ReactS3Client
        .uploadFile(payload.employee_greeting_img, uuid()).then(
            response=>{
                return response
            }
        )
    }
}
function post(apiEndpoint, payload){
    
    let voucher_checkbox = []
    
    payload._voucher.map((value, index)=>{
        let _key_option = '';
        let data_push = null;
        if(typeof payload.option_request[index]=='undefined'){
            _key_option = 1
        }else{
            _key_option = parseInt(payload.option_request[index])
        }
        if(typeof payload.total[index]!='' && payload.maximum_request[index]!=''){
            if(_key_option==1){
                data_push = {
                    "voucher_id":payload._voucher[index],
                    "limit":payload.total[index],
                    "max_per_day":payload.maximum_request[index]
                }
            }else{
                data_push = {
                    "voucher_id":payload._voucher[index],
                    "limit":payload.total[index],
                    "max_per_month":payload.maximum_request[index]
                }
            }
            if(value!=''){
                voucher_checkbox.push(data_push)
            }
        }
        
    })
    return Promise.all([
        upload_banner(payload),
        upload_employee_greeting_img(payload)
    ]).then(response => {
        let _payload={
            category_id: payload.category_id,
            name: payload.name,
            from_date: payload.from_date,
            to_date: payload.to_date,
            fee:payload.fee,
            total_voucher:payload.total_voucher,
            max_per_day:parseInt(payload.to_date),
            max_per_month:parseInt(payload.max_per_month),
            voucher_list_id:voucher_checkbox,
            emp_list_id:join_group(payload.list_employee_group),
            cust_list_id:join_group(payload.list_customer_group),
            cust_group_list_id:join_group(payload.customer_group_id),
            banner: response[0].location,
            messages: payload.messages,
            wrong_code: payload.wrong_code,
            employee_no_promo: payload.employee_no_promo,
            empty_slot: payload.empty_slot,
            wrong_index: payload.wrong_index,
            max_reached: payload.max_reached,
            no_campaign: payload.no_campaign,
            empty_voucher: payload.empty_voucher,
            last_voucher: payload.last_voucher,
            employee_greeting: payload.employee_greeting,
            employee_mesages: payload.employee_mesages,
            employee_greeting_img:response[1].location
        }
       
        return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), _payload, {
            headers: {
                "Content-Type":"application/json",
                "Authorization" : "Bearer "+localStorage.getItem('token')
            }
        }).then((response)=>{
            return response;
        }).catch((err)=>{
            console.log(err);
        })
    })
}

async function put(apiEndpoint, payload){

    
    const ReactS3Client = new S3(config_aws);
    
    let voucher_checkbox = []
    
    payload._voucher.map((value, index)=>{
        let _key_option = '';
        let data_push = null;
       
        // if(typeof payload.option_request[index]=='undefined'){
        //     _key_option = 1
        // }else{
        //     _key_option = payload.option_request[_key]
        // }
        // console.log(_key_option)
        // GET KEY FOR CHECKING NEW OPTION AND NEW TOTAL
        let _key = Object.keys(payload._voucher_mapping).find(
            key => payload._voucher_mapping[key] === value
        )
        //          ||
        //          \/
        // GET NEW KEY VOR OVERWRITE OPTION AND TOTAL
        console.log('Key Voucher List :'+_key)
        if(typeof payload.option_request[_key]!='undefined'){
            _key_option = payload.option_request[_key]
        }else if(typeof payload.max_per_day[index]!='undefined'){
            _key_option = 1
        }else if(typeof payload.max_per_month[index]!='undefined'){
            _key_option = 2
        }else{
            _key_option = 1
        }

        
        //DAPAT KEY OPTIONNYA 
        console.log('Key OPTION:'+_key_option)

        //AMBIL LIMIT VOUCHER
        let limit = ''
        if(typeof payload.voucher_list_id[index]!='undefined'){
            limit = payload.voucher_list_id[index].limit
        }
        if(payload.total[_key]){
            limit = payload.total[_key]
        }
        console.log('LIMIT:'+limit)
        //DAPAT LIMIT VOUCHER


        //AMBIL MAX PER DAY ATAU MAX PER MONTH
        let maximum_request = 0
        if(typeof payload.voucher_list_id[index]!='undefined'){
            maximum_request = ( typeof payload.voucher_list_id[index].max_per_day!='undefined') ?
                                payload.voucher_list_id[index].max_per_day :
                                payload.voucher_list_id[index].max_per_month
        }
        if(typeof payload.maximum_request[_key]!='undefined'){
            maximum_request = payload.maximum_request[_key]
        }
        //DAPAT MAX PER DAY ATAU MAX PER MONTH
        console.log('dapat maximum_request:'+maximum_request)
        // if(typeof payload.total[index]!='' && payload.maximum_request[index]!=''){
            
        if(_key_option==1){
            data_push = {
                "voucher_id":value,
                "limit":limit,
                "max_per_day":maximum_request
            }
        }else{
            data_push = {
                "voucher_id":value,
                "limit":limit,
                "max_per_month":maximum_request
            }
        }
        if(value!=''){
            voucher_checkbox.push(data_push)
        }
    })
    // if(typeof payload.banner=='string'){
        
            
    //     let _payload={
    //         category_id: payload.category_id,
    //         name: payload.name,
    //         from_date: payload.from_date,
    //         to_date: payload.to_date,
    //         fee:payload.fee,
    //         total_voucher:payload.total_voucher,
    //         max_per_day:parseInt(payload.to_date),
    //         max_per_month:parseInt(payload.max_per_month),
    //         voucher_list_id:voucher_checkbox,
    //         emp_list_id:join_group(payload.list_employee_group),
    //         cust_list_id:join_group(payload.list_customer_group),
    //         cust_group_list_id:join_group(payload.customer_group_id),
    //         banner: payload.banner,
    //         is_active: false,
    //         messages: payload.messages,
    //         wrong_code: payload.wrong_code,
    //         employee_no_promo: payload.employee_no_promo,
    //         empty_slot: payload.empty_slot,
    //         wrong_index: payload.wrong_index,
    //         max_reached: payload.max_reached,
    //         no_campaign: payload.no_campaign,
    //         empty_voucher: payload.empty_voucher,
    //         last_voucher: payload.last_voucher,
    //         employee_greeting: payload.employee_greeting,
    //         employee_mesages: payload.employee_mesages,
    //         employee_greeting_img: payload.employee_greeting_img,
    //     }
    //     console.log(">>>>>>")
    //     console.log(payload)
    //     return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), _payload, {
    //         headers: {
    //             "Content-Type":"application/json",
    //             "Authorization" : "Bearer "+localStorage.getItem('token')
    //         }
    //     }).then((response)=>{
    //         return response;
    //     }).catch((err)=>{
    //         console.log(err);
    //     })
        
    // }else{
        async function upload_banner(){
            if(typeof payload.banner=='string'){
                console.log(payload)
                return payload.banner
            }else{
                return ReactS3Client
                .uploadFile(payload.banner, uuid()).then(
                    data=>{
                        console.log(data)
                        return data
                    }
                )
            }
        }
        async function upload_employee_greeting_img(){
            if(typeof payload.employee_greeting_img=='string'){
                return payload.employee_greeting_img
            }else{
                return ReactS3Client
                .uploadFile(payload.employee_greeting_img, uuid()).then(
                    data=>{
                        console.log(data)
                        return data
                    }
                )
            }
        }

        return Promise.all([
            upload_banner(),
            upload_employee_greeting_img()
        ]).then(data=>{
            let _payload={
                category_id: payload.category_id,
                name: payload.name,
                from_date: payload.from_date,
                to_date: payload.to_date,
                fee:payload.fee,
                total_voucher:payload.total_voucher,
                max_per_day:parseInt(payload.to_date),
                max_per_month:parseInt(payload.max_per_month),
                voucher_list_id:voucher_checkbox,
                emp_list_id:join_group(payload.list_employee_group),
                cust_list_id:join_group(payload.list_customer_group),
                cust_group_list_id:join_group(payload.customer_group_id),
                cust_group_list_id:join_group(payload.customer_group_id),
                banner: data[0].location,
                messages: payload.messages,
                wrong_code: payload.wrong_code,
                employee_no_promo: payload.employee_no_promo,
                empty_slot: payload.empty_slot,
                wrong_index: payload.wrong_index,
                max_reached: payload.max_reached,
                no_campaign: payload.no_campaign,
                empty_voucher: payload.empty_voucher,
                last_voucher: payload.last_voucher,
                employee_greeting: payload.employee_greeting,
                employee_mesages: payload.employee_mesages,
                employee_greeting_img: data[1].location,
                is_active: false
            }
            console.log(_payload)
            return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), _payload, {
                headers: {
                    "Content-Type":"application/json",
                    "Authorization" : "Bearer "+localStorage.getItem('token')
                }
            }).then((response)=>{
                return response;
            }).catch((err)=>{
                console.log(err);
            })
        }).catch(err => console.error(err))
}

function deleteDetail(apiEndpoint){
    return axios.delete(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Content-Type":"application/json",
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log(err);
    })
}

function getOptions(){
    let options = {}; 
    if(localStorage.getItem('token')){
        options.headers = { 'x-access-token': localStorage.getItem('token') };
    }
    return options;
}