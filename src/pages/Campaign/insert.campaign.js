import React from 'react';
import { connect } from 'react-redux';
import Message from "../../components/Message"
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	useParams,
	useRouteMatch,
	withRouter
  } from "react-router-dom";
 
import {campaignAction,categoryAction,employeeGroupAction,voucherAction,customer_groupAction} from '../../actions';
import $ from 'jquery';
import dropify from "dropify";
import "../../../node_modules/dropify/src/sass/dropify.scss";
import moment, { Moment } from "moment";
import "../../asset/css/campaign.css";
import DatePicker from 'react-date-picker';
import { isNullOrUndefined } from 'util';


class InsertCampaign extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
			seconds: 5,
			from_date: '',
			to_date: '',
        };
		this.timer = 0
		this.startTimer = this.startTimer.bind(this);
		this.countDown = this.countDown.bind(this);
	}
	onChangeFromDate 	= from_date => this.setState({ from_date })
	onChangeToDate 		= to_date => this.setState({ to_date })
	startTimer() {
        if (this.timer == 0 && this.state.seconds > 0) {
          this.timer = setInterval(this.countDown, 1000);
        }
    }
    countDown() {
        const { dispatch} = this.props;
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
        // time: this.secondsToTime(seconds),
          seconds: seconds,
        });
        // console.log(seconds)
        // Check if we're at zero.
        if (seconds == 0) { 
            dispatch(campaignAction.removeAlert())
          clearInterval(this.timer);
        }
    }
	handleChange = prop => event => {
		const { dispatch } = this.props;

		dispatch(campaignAction.onChangeProps(prop, event));
	};
	handleChangeVoucher_ = prop => event => {
		const { dispatch } = this.props;

		dispatch(campaignAction.onChangeVoucher_(prop, event));
	};
	handleUpload = (prop, name) => event => {
		const { dispatch } = this.props;
		dispatch(campaignAction.onChangeImage(prop.files[0], name));
	};
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
		dispatch(campaignAction.resetStateCampaign())
		dispatch(categoryAction.getCategory());
		dispatch(campaignAction.getCampaign());
		dispatch(voucherAction.getVoucher());
		dispatch(employeeGroupAction.getEmployeeGroup());
		dispatch(customer_groupAction.getCustomer_group());
		if(params.id){
			dispatch(campaignAction.getCampaignById(params.id));
		}
		$('.tabs-group').mouseover(function() {
            var tab_group = $(this).attr('id');
            $(document).on('click', '#'+tab_group+' ul.nav-tabs li', function(){
        
            // $().click(function(){
              var tab_id = $(this).attr('data-tab');
        
              $('#'+tab_group+' ul.nav-tabs li').removeClass('current');
              $('#'+tab_group+ ' .tab-pane').removeClass('current');
        
              $(this).addClass('current');
              $('#'+tab_group+" #"+tab_id).addClass('current');
            })
		  })
		  
		  
	}

	handleSubmit(event){
		const { match : {params } } = this.props;
		const { dispatch } = this.props;
		

		this.props.campaign.voucher_list_id.map(data =>{
			// voucher_list_id
		})
		let payload={
			category_id: this.props.campaign.category_id,
			name: this.props.campaign.name,
			from_date: this.state.from_date,
			to_date: this.state.to_date,
			fee:this.props.campaign.fee,
			total:this.props.campaign.total,
			voucher_list_id:this.props.campaign.voucher_list_id,
			list_employee_group:this.props.campaign.list_employee_group,
			list_customer_group:this.props.campaign.list_customer_group,
			customer_group_id:this.props.campaign.customer_group_id,
			banner: this.props.campaign.banner,
			max_per_day:this.props.campaign.max_per_day,
			max_per_month:this.props.campaign.max_per_month,
			_voucher: this.props.campaign._voucher,
			option_request:this.props.campaign.option_request,
			maximum_request:this.props.campaign.maximum_request,
			messages: this.props.campaign.messages,
			wrong_code: this.props.campaign.wrong_code,
			employee_no_promo: this.props.campaign.employee_no_promo,
			empty_slot: this.props.campaign.empty_slot,
			wrong_index: this.props.campaign.wrong_index,
			max_reached: this.props.campaign.max_reached,
			no_campaign: this.props.campaign.no_campaign,
			empty_voucher: this.props.campaign.empty_voucher,
			last_voucher: this.props.campaign.last_voucher,
			employee_greeting: this.props.campaign.employee_greeting,
			employee_mesages: this.props.campaign.employee_mesages,
			employee_greeting_img: this.props.campaign.employee_greeting_img
		}

        
		dispatch(campaignAction.createCampaign(payload, this.props));
		// this.startTimer()
        
    }
    handleChangeCheckbox = prop => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeCheckbox(prop, event));
    };
    handleChangeCheckboxVoucher = (prop,key) => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeCheckboxVoucher(prop, event, key));
    };
    handleChangeOptionRequest = (key) => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeOptionRequest('option_request', event, key));
    };
    handleChangeCheckboxCustomer = prop => event => {
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeCheckboxCustomer(prop, event));
    };
    handleChangeNumber = (prop,key) => event =>{
        const { dispatch } = this.props;
        dispatch(campaignAction.onChangeNumber(prop, event, key));
    }
	render(){
		$('.dropify').dropify();
		const { match : {params }} = this.props;
        const { message : {status_message, message} } = this.props.campaign
		const { category } = this.props.category;
		const { employee } = this.props.employee;
		const { voucher } = this.props.voucher;
		const { employee_group } = this.props.employeegroup;
		const {customer_group} = this.props.customer_group;

		let _from_date = ''
		let _to_date = ''
		let title = ''
		let disable = ''
		// console.log(this.props.campaign.from_date)
		if(typeof this.props.campaign.from_date!= "undefined"){
			if(this.props.campaign.from_date!=''){
				_from_date = moment(this.props.campaign.from_date).format('YYYY-MM-DD');
			}
				
		}
		if(typeof this.props.campaign.to_date!= "undefined"){
			if(this.props.campaign.to_date!=''){
				_to_date = moment(this.props.campaign.to_date).format('YYYY-MM-DD');
			}
		}
		if(this.state.is_edit){
			title = 'Edit'
			disable ='form-control'
		}else{
			title = 'Insert'
			disable ='form-control disable'
		}
		return (
			<div>
				<div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Campaign</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Campaign</div>
							<div className="breadcrumb-item">Insert Campaign</div>
						</div>
					</div>
				</div>
				<div className="p-20-c">
				{   
					message ? <Message type={status_message} text={message}/> : null
				}
					<div className="row">
						<div className="col-12">
							<div className="card" style={{minHeight: "450px"}}>
								<div className="card-body">
									<div className="row">
										<div className="col-4">
											<h2 className="m-20-vc">{title} Campaign</h2>
											<div className="card-content">
												<form className="form">
													<div className="form-group">
														<input type="file" className="dropify" 
														name="template" ref={(ref)=>this.fileuploadbanner = ref} onChange={this.handleUpload(this.fileuploadbanner,'banner')} />
													</div>
												</form>
											</div>
										</div>
										<div className="col-8">
											<div className="form date-picker">	
												<div className="row">
													<div className="col-6">
														<div className="form-group">
															<label htmlFor="#" className="form-label">From Date</label>
															{/* <input type="date" value={this.props.campaign.from_date} className="form-control" 
															onChange={this.handleChange('from_date')}/> */}
															<div className="date-picker">
																<DatePicker
																onChange={this.onChangeFromDate}
																value={this.state.from_date}
																/>
															</div>
														</div>
													</div>	
													<div className="col-6">
														<div className="form-group">
															<label htmlFor="#" className="form-label">To Date</label>
															<div className="date-picker">
																<DatePicker
																onChange={this.onChangeToDate}
																value={this.state.to_date}
																/>
															</div>
														</div>	
													</div>
												</div>	
											</div>
											<form className="form">
												
												<div className="form-group">
													<label htmlFor="#" className="form-label">Campaign Name</label>
													<input type="text" value={this.props.campaign.name} className="form-control" 
													onChange={this.handleChange('name')}/>
												</div>
												
												<div className="form-group">
													<label htmlFor="#" className="form-label">INCENTIVE</label>
													<input type="number" inputmode="numeric" pattern="[0-9]*" type="tel" value={this.props.campaign.fee} className="form-control" 
													onChange={this.handleChange('fee')}/>
												</div>
												{/* <div className="form-group">
													<label htmlFor="#" className="form-label">Campaign Messages</label>
													<textarea className="form-control" onChange={this.handleChange('messages')} value={this.props.campaign.messages}></textarea> 
													
												</div> */}
											</form>
										</div>	
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="tabs-group" id="tg-1">
										<ul className="nav-tabs">
											<li className="tab-item current" data-tab="tab-1" >White List Employee Group</li>
											<li className="tab-item" data-tab="tab-2">White List Customer Group</li>
											<li className="tab-item" data-tab="tab-3">Voucher</li>
											<li className="tab-item" data-tab="tab-4">Message</li>
										</ul>
										<div className="tab-content">
											<div id="tab-1" className="tab-pane current">
												<div className="col-12">
												<table className="table table-striped">
													<thead>
														<tr>
															<th>#</th>
															<th>Group Name</th>
														</tr>
													</thead>
													<tbody>
													{
														employee_group.length >0 ? (
															employee_group && employee_group.map(data => {
																return (
																	<tr key={data.employee_group_id}>
																		<td>
																			<input type="checkbox" value={data.employee_group_id} className="form-control" onChange={this.handleChangeCheckbox('list_employee_group')}/>
																		</td>
																		<td>{data.name}</td>
																	</tr>
																)
															})
														)  : (
															<tr>
																<td colSpan="4" align="center">Tidak Ada Data</td>
															</tr>
														)
													}
													</tbody>
												</table>
												</div>
											</div>
											<div id="tab-2" className="tab-pane">
												<div className="col-12">
													<table className="table table-striped">
														<thead>
															<tr>
																<th>#</th>
																<th>Customer Group Name</th>
															</tr>
														</thead>
														<tbody>
														{
														(typeof customer_group!=='undefined') ? (
															customer_group.length >0 ? (
																customer_group && customer_group.map(data => {
																	return (
																		<tr key={data.customer_group_id}>
																			<td>
																				<input type="checkbox" value={data.cust_list_id} id={data.customer_group_id} className="form-control" onChange={this.handleChangeCheckboxCustomer('customer_group_id')}/>
																			</td>
																			<td>{data.name}</td>
																		</tr>
																	)
																	
																})
															)  : (
																<tr>
																	<td colSpan="4" align="center">Tidak Ada Data</td>
																</tr>
															)
														):<tr>
														<td colSpan="4" align="center">Tidak Ada Data</td>
													</tr>
														
													}
														</tbody>
													</table>
												</div>
											</div>
											<div id="tab-3" className="tab-pane">
												<table className="table table-striped">

													<thead>
														<tr>
														
															<th>#</th>
															<th>Voucher Name</th>
															<th>Image</th>
                                                            <th style={{width: '40px'}}>Total Voucher</th>
															<th>Voucher Request Limit</th>
														
														</tr>
													</thead>
													<tbody>
													{
														voucher && voucher.map((data, key) => {
															let checked = false;
															let _key = null;
															if(typeof this.props.campaign._voucher !='undefined'){
																if(this.props.campaign._voucher.includes(data.voucher_id)){
																	checked = true
																}
																_key = Object.keys(this.props.campaign._voucher).find(key => this.props.campaign._voucher[key] === data.voucher_id)
																
															}
															
															return (
																<tr key={data.voucher_id}>
																	<th>
																		<input type="checkbox" checked={checked} value={data.voucher_id} 
																		className="form-control" onChange={this.handleChangeCheckboxVoucher('_voucher', key)}/>
																	</th>
																	<td>{data.voucher_name}</td>
																	<td><img src={data.voucher_image}  height="80" /></td>
																	<td>{data.total_voucher}</td>
																	<td>
																		<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell',  width: '100px'}}>
	                                                                        <input type="number" inputmode="numeric" pattern="[0-9]*" type="tel" value={this.props.campaign.max_per_day[data.voucher_id]} className="form-control" 
	                                                                        onChange={this.handleChangeNumber('maximum_request', key)}/>
	                                                                    </div>
																		<select className="form-control col-4" style={{  margin: '5px', display: 'table-cell',height:'30px',background:'white'}} onChange={this.handleChangeOptionRequest(key)}>
	                                                                        <option value="1">request per day</option>
	                                                                        <option value="2">request per month</option>
	                                                                    </select> 
																	</td>
																	
																</tr>
															)
															
														})
													}
													</tbody>
												</table>
											</div>
											<div id="tab-4" className="tab-pane">
											<form className="form">
													<div className="form-group">
														<label htmlFor="#" className="form-label">Employee Greeting Image</label>
														<input type="file" className="dropify" 
														name="template" ref={(ref)=>this.fileuploadgreeting = ref} onChange={this.handleUpload(this.fileuploadgreeting,'employee_greeting_img')} />
													</div>
												</form>
											<div style={{color: 'grey', margin: '10px', padding: '10px 10px 10px 30px', borderLeft: '3px solid #3FB5A7', position: 'sticky', top: '0',}}>
												<ul style={{listStyleType: "disc"}}>
													<li>
														use *text* to get <b style={{fontWeight: "bold"}}>text</b>
													</li>
													<li>
														use _text_ to get <i>text</i>
													</li>
													<li>
														use text to get <strike>text</strike>
													</li>
													<li>
														Use &#123;salescode&#125; to get individual sales promo code
													</li>
												</ul>
											</div>
												<table className="table table-striped">
													<tbody>
														<tr><td colSpan="2" style={{textAlign: "center", fontWeight: "bold"}}>*MESSAGE TO EMPLOYEE</td></tr>
														<tr>
															<td>Employee welcome</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea  style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_greeting} placeholder="Selamat ! kamu sudah bergabung menjadi sales REFIRA. Dapatkan insentif menarik dengan cara membagikan promo dibawah ini ya :" className="form-control" 
																onChange={this.handleChange('employee_greeting')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Forwardable Message for employee</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																	<textarea placeholder="PROMO TERBARU !
																	Minimart lagi bagi-bagi promo loh.. caranya cukup mudah, cukup ketikan kode promo yang tertera pada gambar diatas ke nomer WA : +6282124399959. dapatkan segera ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_mesages} className="form-control" 
																	onChange={this.handleChange('employee_mesages')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Employee cannot request promo warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="mohoh maaf, semua sales REFIRA tidak bisa mengikuti promo yang sedang berlangsung" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.employee_no_promo} className="form-control" 
																onChange={this.handleChange('employee_no_promo')}/>	
																</div>
															</td>
														</tr>
														
														<tr><td colSpan="2"  style={{textAlign: "center", fontWeight: "bold"}}>*MESSAGE TO CUSTOMER</td></tr>
														<tr>
															<td>Campaign greeting messages</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																	<textarea placeholder="PROMO SERU !!!hello.. kita lagi bagi bagi promo loh.. dapatkan promo menarik cukup dengan menunjukan gambar promo ini di kasir. ayo segera ! persediaan terbatas... Jangan sampai kehabisan ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px", fontSize: "14px"}}  className="form-control" onChange={this.handleChange('messages')} value={this.props.campaign.messages}></textarea>
																</div>
															</td>
														</tr>
														<tr>
															<td>User request the last voucher on the campaign</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Terimakasih kamu telah berpartisipasi dalam promo kali ini. nantikan promo seru lainnya ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.last_voucher} className="form-control" 
																onChange={this.handleChange('last_voucher')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Wrong referal code warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="maaf, kode promo kamu salah atau tidak di kenal. coba cek lagi kode promo kamu ya.." style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.wrong_code} className="form-control" 
																onChange={this.handleChange('wrong_code')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>User have request voucher warning </td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Maaf, promo untuk kamu hari ini sudah habis. coba lagi besok dan tunggu promo seru lainnya ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.empty_slot} className="form-control" 
																onChange={this.handleChange('empty_slot')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Wrong voucher index warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Maaf, kode vocher kamu salah. periksa kembali kode voucher kamu ya.." style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.wrong_index} className="form-control" 
																onChange={this.handleChange('wrong_index')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Voucher is on maximum daily / monthly hit warning </td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Maaf, promo untuk kamu hari ini sudah habis. coba lagi besok atau tunggu promo seru lainnya ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.max_reached} className="form-control" 
																onChange={this.handleChange('max_reached')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>No promo or promo have been expired warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Maaf, promo untuk kamu hari ini sudah habis. coba lagi besok atau tunggu promo seru lainnya ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.no_campaign} className="form-control" 
																onChange={this.handleChange('no_campaign')}/>	
																</div>
															</td>
														</tr>
														<tr>
															<td>Empty voucher warning</td>
															<td>
																<div className="form-group col-6" style={{ margin: '5px', display: 'table-cell'}}>
																<textarea placeholder="Maaf, promo untuk kamu hari ini sudah habis. coba lagi besok atau tunggu promo seru lainnya ya !" style={{height: '100px', width: '500px',border: 'none', fontSize: "14px"}}  type="text" value={this.props.campaign.empty_voucher} className="form-control" 
																onChange={this.handleChange('empty_voucher')}/>	
																</div>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-auto right">
							<div className="card">
								<div className="card-body">
									<button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button>
							
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		);
	}
}
const mapStateToProps = (state) =>{
	return state
}
const InsertCampaignPage = withRouter(connect(mapStateToProps, null, null, {
	pure: false
})(InsertCampaign));
export default InsertCampaignPage;