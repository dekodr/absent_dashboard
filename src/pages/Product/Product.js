import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { history } from '../../helpers';
import ReactPaginate from 'react-paginate';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import { productAction } from '../../actions';
import "../../asset/css/pagination.css";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Product extends React.Component{
	constructor(props){
        super(props);
        this.state = {
            page: 1
        }
	}
	
	componentDidMount(){
        const { dispatch } = this.props;
        dispatch(productAction.resetState());
        dispatch(productAction.getProduct(this.state.page));
        
    }
    handlePageClick = data => {
        console.log(data);
        // let selected = data.selected;
        // let offset = Math.ceil(selected * this.props.perPage);
    
        // this.setState({ offset: offset }, () => {
        //     dispatch(productAction.getProduct(this.state.page));
        // });
    };

    handleClick = (event, id) => {
        const { dispatch } = this.props;
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            dispatch(productAction.deleteProductById(id));
        }
    }
	render(){
        const { product, total_page, total } = this.props.product;
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Product</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Product</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">

                        {this.props.product.message}
                        <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                                        <div className="button-row">
                                            <div className="btn-group" >
                                                <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/product/insert")}><i className="far fa-plus-square"></i>Create</button>
                                            </div>
                                        </div>
                                        {
                                            <DataTable
                                                title="Product" 
                                                pagination={true}
                                                columns={[
                                                    {
                                                        name: 'Product Name',
                                                        selector: 'product_name',
                                                        sortable: true,
                                                    },
                                                    {
                                                        name: 'Category',
                                                        selector: 'category_name',
                                                        sortable: true,
                                                    },
                                                    {
                                                        name: 'Action',
                                                        selector: 'product_id',
                                                        cell: row => <span>
                                                            <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/product/edit/"+row.product_id)}><i className="far fa-edit"></i></button>
                                                            <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.product_id)}><i className="fas fa-trash"></i></button>
                                                            {/* <button className="btn btn-primary icon-only" onClick={() => this.props.history.push("/dashboard/campaign/detail/"+row.campaign_id)}><i className="fas fa-info-circle"></i></button>
                                                                {
                                                                    row.status==0 ? 
                                                                    <span><button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/campaign/edit/"+row.campaign_id)}><i className="fas fa-edit"></i></button>
                                                                    <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.campaign_id)}><i className="fas fa-trash"></i></button></span> : null
                                                                } */}
                                                            </span>
                                                    }
                                                    ]}
                                                data={product}
                                                striped={true}
                                                customStyles={styleTable}
                                            />
                                        }
                                        {/* <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#ID</th>
                                                    <th>Product Name</th>
                                                    <th>Category</th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                product.length > 0 ? (
                                                    product.map(data => {
                                                        return (
                                                            <tr key={data.product_id}>
                                                                <th>{data.product_id}</th>
                                                                <td>{data.product_name}</td>
                                                                <td>{data.category_name}</td><td>
                                                                <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/product/edit/"+data.product_id)}><i className="far fa-edit"></i>Edit</button>
                                                                <button className="btn btn-primary" onClick={(event) => this.handleClick(event, data.product_id)}><i className="fas fa-trash"></i>Delete</button>
                                                            </td>
                                                            </tr>
                                                        )
                                                        
                                                    })
                                                ) : (
                                                    <tr>
                                                        <td colSpan="4" align="center">Tidak Ada Data</td>
                                                    </tr>
                                                )
                                            }
                                            </tbody>
                                        </table>
                                        <nav className="pagination-box" aria-label="Page navigation">
                                            <ReactPaginate
                                                previousLabel={<i className="fas fa-backward"></i>}
                                                nextLabel={<i className="fas fa-forward"></i>}
                                                breakLabel={'...'}
                                                breakClassName={'break-me'}
                                                pageCount={total_page}
                                                marginPagesDisplayed={2}
                                                pageRangeDisplayed={2}
                                                onPageChange={this.handlePageClick}
                                                containerClassName={'pagination'}
                                                subContainerClassName={'pages pagination page-item'}
                                                activeClassName={'active'}
                                            />
                                        </nav> */}
                                        
                                    </div>      
                                 </div>
                             </div>		
                         </div>
                     </div>
                 </div>
                
             </div>
	  	);
	}
}
const mapStateToProps = (state) =>{
    return {
        product : state.product
    };
  }

  const ProductPage = connect(mapStateToProps, null, null, {
    pure: false
  })(Product);
export default ProductPage;
