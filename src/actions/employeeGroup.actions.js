import React from 'react';
import Message from "../components/Message"
import { employeeGroupServices } from '../services';
import { history } from '../helpers';

export const employeeGroupAction = {
    getEmployeeGroup,
    getEmployeeGroupById,
    onChangeProps,
    editEmployeeGroupInfo,
    createEmployeeGroup,
    deleteEmployeeGroup,
    onChangeCheckbox,
    getEmployeeGroupSelected,
    resetState,
    message,
    getEmployeeListByGroupId
};

function getEmployeeGroup(employeeGroup, payload={}){
    if(Object.entries(payload).length !== 0 && payload.constructor === Object){
        employeeGroup.submit_filter = payload;
    }
    return dispatch => {
        let apiEndpoint = 'employee_group';
        employeeGroupServices.get(apiEndpoint,employeeGroup)
        .then((response)=>{
            console.log(response)
            dispatch(changeEmployeeGroupList(response.data.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function getEmployeeGroupSelected(id, employeeGroup){
    return dispatch => {
        let apiEndpoint = 'employee_group_by_campaign_id/'+id;
        employeeGroupServices.get(apiEndpoint,employeeGroup)
        .then((response)=>{
            dispatch(changeEmployeeGroupListSelected(response.data.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}

function createEmployeeGroup(payload, props){
    return dispatch => {
        let apiEndpoint = 'employee_group';
        employeeGroupServices.post(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(createEmployeeGroupInfo());
                props.history.push('/dashboard/setting/employee_group');
            }else if(response.message.indexOf("422") > -1){
                // alert()
                dispatch(message("error", "Please check again. Required field is empty."))
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }else{
                dispatch(message("error", response.data.message))
            }  
        }) 
    }
}

function getEmployeeGroupById(id){

    return dispatch => {
        let apiEndpoint = 'employee_group/'+ id;
        employeeGroupServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(editEmployeeDetails(response.data.data));
        })
    };
}
function getEmployeeListByGroupId(id){
    return dispatch => {
        let apiEndpoint = 'employee_by_group_id/'+ id;
        employeeGroupServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(handlerGetEmployeeListByGroupId(response.data.data));
        })
    };
}
function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}

function editEmployeeGroupInfo(id, payload, props){
    return dispatch => {
        let apiEndpoint = 'employee_group/'+ id;
        employeeGroupServices.put(apiEndpoint, payload)
        .then((response)=>{
           if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(updatedEmployeeGroupInfo());
                props.history.push('/dashboard/setting/employee_group');
            }else if(response.message.indexOf(422)){
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                dispatch(message("error", response.data.message))
            }  
        }) 
    }
}

function deleteEmployeeGroup(id){
    return dispatch => {
        let apiEndpoint = 'employee_group/'+ id;
        employeeGroupServices.deleteDetail(apiEndpoint,1)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteEmployeeDetails());
                dispatch(employeeGroupAction.getEmployeeGroup());

                // redirecting
                // props.history.push('/dashboard/employee');
            }else{
                dispatch(message("error", response.response.data.message))
            }
            // dispatch(employeeGroupAction.getEmployeeGroup());
        })
    };
}

function onChangeCheckbox(props, event,field){
    return dispatch =>{
        console.log(dispatch)
        dispatch(handleOnChangeCheckbox(props, event.target.value, field));
    }
}

export function resetStateHandler(){
	return{
		type: "RESET_STATE_EMPLOYEE_GROUP"
	}
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}

export function changeEmployeeGroupList(employee_group){
    return{
        type: "FETCHED_ALL_EMPLOYEE_GROUP",
        employee_group: employee_group
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editEmployeeDetails(employeeGroup){
    return{
        type: "EMPLOYEE_GROUP_DETAIL",
        id: employeeGroup.id,
        name: employeeGroup.name,
        emp_list_id: employeeGroup.emp_list_id,
        is_active: employeeGroup.is_active,

    }
}

export function updatedEmployeeGroupInfo(){
    return{
        type: "EMPLOYEE_GROUP_UPDATED"
    }
}

export function createEmployeeGroupInfo(){
    return{
        type: "EMPLOYEE_GROUP_CREATED_SUCCESSFULLY"
    }
}

export function deleteEmployeeDetails(){
    return{
        type: "DELETED_VENDOR_DETAILS"
    }
}
export function handleOnChangeCheckbox(props, value){
    return{
        type: "HANDLE_ON_CHANGE_CHECKBOX_EMPLOYEE_GROUP",
        props: props,
        value: value
    }
}
export function handlerGetEmployeeListByGroupId(employee){
    return{
        type: "HANDLER_GET_EMPLOYEE_LIST_BY_GROUP_ID",
        employee: employee
    }
}
export function changeEmployeeGroupListSelected(employee_group){
    return{
        type: "FETCHED_ALL_EMPLOYEE_GROUP_BY_CAMPAIGN",
        employee_group: employee_group
    }
}