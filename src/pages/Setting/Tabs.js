import React from 'react';
import { connect } from 'react-redux';
import Message from "../../components/Message"
import {
	BrowserRouter as Router,
	withRouter
  } from "react-router-dom";
import $ from 'jquery';
import "../../../node_modules/dropify/src/sass/dropify.scss";
import Employee  from "../Employee/Employee"
import EmployeeGroup from "../Employee_group/Employee_group"
import Customer from "../Customer/Customer"
import CustomerGroup from "../Customer_group/Customer_group"
import Product from "../Product/Product"
import Category from "../Category/Category"
import "../../asset/css/campaign.css";

class Tabs extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
			seconds: 5
        };
	}
	componentDidMount(){
		
		$('.tabs-group').mouseover(function() {
			var tab_group = $(this).attr('id');
			$(document).on('click', '#'+tab_group+' ul.nav-tabs li', function(){
		
			// $().click(function(){
			  var tab_id = $(this).attr('data-tab');
		
			  $('#'+tab_group+' ul.nav-tabs li').removeClass('current');
			  $('#'+tab_group+ ' .tab-pane').removeClass('current');
		
			  $(this).addClass('current');
			  $('#'+tab_group+" #"+tab_id).addClass('current');
			})
		})
	}
	checkTabSelect(page){
		
		const { match : {params }} = this.props;
		console.log()
		if(Object.keys(params).length==0){
			
			if(page=='employee'){
				return 'tab-item current'
			}else{
				return 'tab-item'
			}
		}else if(params.page==page){
			return 'tab-item current'
		}else{
			return 'tab-item'
		}
	}
	checkContentSelect(page){
		const { match : {params }} = this.props;
		if(Object.keys(params).length==0){
			if(page=='employee'){
				return 'tab-pane current'
			}else{
				return 'tab-pane'
			}
		}else if(params.page==page){
			return 'tab-pane current'
		}else{
			return 'tab-pane'
		}
	}
	render(){
		$('.dropify').dropify();
		

		return (
			<div>
				<div className="p-20-c">
				<div className="breadcrumb">
						<div className="page-title">Setting</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Setting</div>
						</div>
					</div>
									</div>
				<div className="p-20-c">
					<div className="row">
						<div className="col-12">
							<div className="card">
								<div className="card-body">
									<div className="tabs-group" id="tg-1">
										<ul className="nav-tabs">
											<li className={this.checkTabSelect('employee_group')}  data-tab="tab-2" onClick={()=>this.props.history.push('employee_group')}>Employee Group</li>
											{/* <li className={this.checkTabSelect('employee')} data-tab="tab-1" onClick={()=>this.props.history.push('employee')}>Employee</li> */}
											
											<li className={this.checkTabSelect('category')}  data-tab="tab-6" onClick={()=>this.props.history.push('category')}>Product Category</li>
											{/* <li className={this.checkTabSelect('product')}  data-tab="tab-5" onClick={()=>this.props.history.push('product')}>Product</li> */}

											<li className={this.checkTabSelect('customer_group')}  data-tab="tab-4" onClick={()=>this.props.history.push('customer_group')}>Customer Group</li>
											{/* <li className={this.checkTabSelect('customer')}  data-tab="tab-3" onClick={()=>this.props.history.push('customer')}>Customer</li> */}
										</ul>
										<div className="tab-content">
											<div id="tab-1" className={this.checkContentSelect('employee')}>
												<div className="col-12">
													<Employee history={this.props.history} />
												</div>
											</div>
											<div id="tab-2" className={this.checkContentSelect('employee_group')}>
												<div className="col-12">
													<EmployeeGroup history={this.props.history}/>
												</div>
											</div>
											<div id="tab-3" className={this.checkContentSelect('customer')}>
												<div className="col-12">
													<Customer history={this.props.history}/>
												</div>
											</div>
											<div id="tab-4" className={this.checkContentSelect('customer_group')}>
												<div className="col-12">
													<CustomerGroup history={this.props.history}/>
												</div>
											</div>
											<div id="tab-5" className={this.checkContentSelect('product')}>
												<div className="col-12">
													<Product history={this.props.history}/>
												</div>
											</div>
											<div id="tab-6" className={this.checkContentSelect('category')}>
												<div className="col-12">
													<Category history={this.props.history}/>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		);
	}
}
const mapStateToProps = (state) =>{
	return state
}
const TabsPage = withRouter(connect(mapStateToProps, null, null, {
	pure: false
})(Tabs));
export default TabsPage;