import React, { Component } from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { userActions } from '../actions';
import "../asset/css/login.css";
// import io from "socket.io-client";

class Login extends Component {
  constructor(props){
      super(props);
      this.state = {
          username: '',
          password: '',
      }
  }
  componentDidMount() {

    this.props.history.push('/dashboard');
    if(localStorage.getItem('auth')){
      this.props.history.push('/dashboard');
    }
    // var socket = io('http://127.0.0.1:8082')
		// socket.on('connect', function(){
		// 	socket.send('User Connected')
		// })
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
  login = event =>{
    this.setState({ submitted: true });
    const { username, password } = this.state;
    const { dispatch } = this.props;
    if (username && password) {
        dispatch(userActions.login(username, password, this.props));
    }
  }
  render() {
    return (
      
      <div className="login-wrapper-g">
        <div className="login-g-container">
          <div className="logo-login"></div>

		
          <form className="login-g-form">
            <input type="text" placeholder="Username" onChange = {this.handleChange('username')} />
            <input type="password" placeholder="Password" onChange = {this.handleChange('password')}/>
            {/* <input type="text" name="username" placeholder="Username"  />
            <input type="password" name="password" placeholder="Password" /> */}
            <button type="button" id="login-button"  onClick={(event)=>{this.login()}}>
              Login<i class="fa fa-arrow-right"></i></button>
          </form>
        </div>

        <ul className="bg-bubbles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    );
  }
}

 
const mapStateToProps = (state) =>{
  const { loggingIn } = state.authentication;
  return {
      loggingIn
  };
}

const connectedLoginPage = withRouter(connect(mapStateToProps, null, null, {
  pure: false
})(Login));

export { connectedLoginPage as Login };