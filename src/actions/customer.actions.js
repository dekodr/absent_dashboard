import React from 'react';
import { customerServices } from '../services';
import { history } from '../helpers';
import Message from "../components/Message"

export const customerAction = {
    getCustomer,
    onChangeProps,
    createCustomer,
    editCustomerInfo,
    deleteCustomer,getCustomerById,
    onChangePropsCsv,
    uploadCsvCustomer,
    resetState,
    message
};

function getCustomer(){
    return dispatch => {
        let apiEndpoint = 'customer';
        customerServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(changeCustomerList(response.data.data));
        }).catch((err)=>{
            console.log(err);
        })
    };
}

function createCustomer(payload, props){
    return dispatch => {
        let apiEndpoint = 'customer';
        customerServices.post(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(createCustomerInfo());
                props.history.push('/dashboard/customer');
                
            }else if(response.message.indexOf("422") > -1){
                // alert()
                dispatch(message("error", "Please check again. Required field is empty."))
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }else{
                dispatch(message("error", response.data.message))
            }  
        }) 
    }
}

function getCustomerById(id){

    return dispatch => {
        let apiEndpoint = 'customer/'+ id;
        customerServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(editCustomerDetails(response.data.data));
        })
    };
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function onChangePropsCsv(props, name){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, name));
    }
}

function editCustomerInfo(id, payload, props){
    return dispatch => {
        let apiEndpoint = 'customer/'+ id;
        customerServices.put(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(updatedCustomerInfo());
                props.history.push('/dashboard/customer');
            }else if(response.message.indexOf("422") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }
        })
    }
}
function uploadCsvCustomer(form,props){
    return dispatch => {
        dispatch(showLoading())
        alert('Importing Data')
		let apiEndpoint = 'customer/bulk';

		customerServices.post_csv(apiEndpoint, {'uploadcsv':form})
		.then((response)=>{
			// dispatch(createCustomerInfo());
			dispatch(hideLoading())
			// dispatch(resetStateHandler())
            // props.history.push('/customer/action');
            window.location.reload()
		}) 
	}
}
function deleteCustomer(id){
    return dispatch => {
        let apiEndpoint = 'customer/'+ id;
        customerServices.deleteDetail(apiEndpoint)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteCustomerDetails());
                dispatch(customerAction.getCustomer());
            }else{
                dispatch(message("error", response.response.data.message))
            }
        })
    };
}
export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}
export function showLoading(){
    return{
        type: "SHOW_LOADING_CUSTOMER_ENTRY"
    }
}
export function hideLoading(){
    return{
        type: "HIDE_LOADING_CUSTOMER_ENTRY"
    }
}
export function changeCustomerList(customer){
    return{
        type: "FETCHED_ALL_CUSTOMER",
        customer: customer
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editCustomerDetails(customer){
    return{
        type: "CUSTOMER_DETAIL",
        id: customer.id,
        first_name: customer.first_name,
        last_name: customer.last_name,
        phone_no: customer.phone_no,
        customer_group_id : customer.customer_group_id,
        is_elastic: customer.is_elastic
    }
}

export function updatedCustomerInfo(){
    return{
        type: "CUSTOMER_UPDATED"
    }
}

export function createCustomerInfo(){
    return{
        type: "CUSTOMER_CREATED_SUCCESSFULLY"
    }
}

export function deleteCustomerDetails(){
    return{
        type: "DELETED_CUSTOMER_DETAILS"
    }
}

function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_CUSTOMER"
	}
}