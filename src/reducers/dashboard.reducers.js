const initialState = {
    active_promo: 0,
    broadcaster: 0,
    top_voucher:[], top_program_hit:[], top_program_redeem:[]
 };


export function dashboard(state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case 'GET_ACTIVE_PROMO':
            return {
            ...state,
            active_promo: action.active_promo
            }; 
        case 'GET_BROADCASTER':
            return {
            ...state,
            broadcaster: action.broadcaster
            }; 
        case 'GET_TOTAL_HIT':
            return {
                ...state,
                total_hit: action.total_hit
            }
        case 'GET_TOP_PRODUCT':
            return {
                ...state,
                top_product: action.data
            }
        case 'GET_TOP_SALES':
            return {
                ...state,
                data: action.data,
                sales: action.sales,
                total: action.total,
                top_sales_redeem: action.top_sales_redeem,
                top_sales_hit: action.top_sales_hit

            }
        case 'TOP_VOUCHER':
            return {
                ...state,
                top_voucher: action.program,
                top_program_redeem: action.top_program_redeem,
                top_program_hit:action.top_program_hit
            }
        case 'TOP_PROGRAM_LABEL':
            return {
                ...state,
                top_voucher: action.program,
                top_program_redeem: action.top_program_redeem,
                top_program_hit:action.top_program_hit
            }
        case 'GET_VOUCHER_REDEEM_BY_LOCATION':
            return{
                ...state,
                voucher_redeem_by_location: action.voucher_redeem_by_location
            }
        case 'GET_VOUCHER_REDEEM_BY_CAMPAIGN':
            return{
                ...state,
                voucher_redeem_by_campaign: action.voucher_redeem_by_campaign
            }
        default:
            return state
    }
  }