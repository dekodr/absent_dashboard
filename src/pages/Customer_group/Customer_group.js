import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { history } from '../../helpers';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import { customer_groupAction } from '../../actions';
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Customer_group extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch(customer_groupAction.resetState());
        dispatch(customer_groupAction.getCustomer_group());
    }

    handleClick = (event, id) => {
        const { dispatch } = this.props;
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            dispatch(customer_groupAction.deleteCustomerGroup(id));
        }
    }

    render(){
        const { customer_group } = this.props.customer_group;
       
        return (
            <div>
                {/* <div className="p-20-c">
                    <div className="breadcrumb">
                        <div className="page-title">Customer Group</div>
                        <div className="page-info">
                            <div className="breadcrumb-item">Home</div>
                            <div className="breadcrumb-item">Customer Group</div>
                        </div>
                    </div>
                </div> */}
                {/* <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">

                        {this.props.customer_group.message}
                            
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content"> */}
                                    <div className="button-row">
                                        <div className="btn-group" >
                                            <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/customer_group/insert")}><i className="far fa-plus-square"></i>Create</button>
                                        </div>
                                    </div>
                                    {
                                        <DataTable
                                            title="Customer Group" 
                                            pagination={true}
                                            columns={[
                                                {
                                                    name: 'Name',
                                                    selector: 'name',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Action',
                                                    selector: 'customer_group_id',
                                                    cell: row => <span>
                                                        <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/customer_group/edit/"+row.customer_group_id)}><i className="far fa-edit"></i></button>
                                                        <button className="btn btn-primary  icon-only" onClick={() => this.props.history.push("/dashboard/customer_group/detail/"+row.customer_group_id)}><i className="fas fa-info"></i></button>
                                                        <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.customer_group_id)}><i className="fas fa-trash"></i></button>
                                                        {/* <button className="btn btn-primary icon-only" onClick={() => this.props.history.push("/dashboard/campaign/detail/"+row.campaign_id)}><i className="fas fa-info-circle"></i></button>
                                                            {
                                                                row.status==0 ? 
                                                                <span><button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/campaign/edit/"+row.campaign_id)}><i className="fas fa-edit"></i></button>
                                                                <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.campaign_id)}><i className="fas fa-trash"></i></button></span> : null
                                                            } */}
                                                        </span>
                                                }
                                                ]}
                                            data={customer_group}
                                            striped={true}
                                            customStyles={styleTable}
                                        />
                                    }
                                    </div>      
            //                     </div>
            //                 </div>      
            //             </div>
            //         </div>
            //     </div>
                
            // </div>
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        customer_group : state.customer_group
    };
  }

  const customer_groupPage = connect(mapStateToProps, null, null, {
    pure: false
  })(Customer_group);
export default customer_groupPage;
{/* <table className="table table-striped">
<thead>
    <tr>
        <th>Name</th> */}
        {/* <th>Customer Group</th> */}
//         <th>Action</th>
//     </tr>
// </thead>
// <tbody>

// {
//     typeof customer_group !== 'undefined' ? (
//         customer_group.length > 0 ? (
//             customer_group && customer_group.map(data => {
//                 return (
//                     <tr key={data.customer_group_id}>
//                         <td>{data.name}</td>
                        {/* <td>{data.last_name}</td> */}
//                         <td>
//                         <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/customer_group/edit/"+data.customer_group_id)}><i className="far fa-edit"></i>Edit</button>
//                         <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/customer_group/detail/"+data.customer_group_id)}><i className="fas fa-info"></i>Detail</button>
//                         <button className="btn btn-primary" onClick={(event) => this.handleClick(event, data.customer_group_id)}><i className="fas fa-trash"></i>Delete</button>
//                         </td>
//                     </tr>
//                 )
                
//             })
//         ) : (
//             <tr>
//                 <td colSpan="3" align="center">Tidak Ada Data</td>
//             </tr>
//         )
//     ): null
// }    
    {/*
    customer_group && customer_group.map(data => {
        return (
            <tr key={data.customer_group_id}>
                <td>{data.first_name}</td>
                <td>{data.last_name}</td>
                <td>
                    <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/employee/insert")}><i className="far fa-edit"></i>Edit</button>
                    <button className="btn btn-primary" onClick={(event) => this.handleClick(event, data.customer_group_id)}><i className="far fa-trash"></i>Delete</button>
                </td>
            </tr>
        )
        
    })
} */}
// </tbody>
// </table>