import React , {useState, useEffect} from 'react';
import Navbar from "../components/Navbar";
import Home from "./Home";
import Employee from "./Employee/Employee";
import Presence from "./Presence/Presence";
import EmployeeGroup from "./Employee_group/Employee_group";

import insertEmployeeGroup from "./Employee_group/insert.employee_group";
import detailEmployeeGroup from "./Employee_group/detail.employee_group";
import formEmployee from "./Employee/Form";
import Tabs from "./Setting/Tabs"

import { history } from '../helpers';
import {
	BrowserRouter,
	Switch,
	Route,
    withRouter
  } from "react-router-dom";
import SideBar from '../components/Sidebar';

class Dashboard extends React.Component{
	constructor(props){
        super(props);
	}
    
	render(){
        let path = this.props.match.path
		return (
            <BrowserRouter history={history}> 
			<div>
                <div className="header-frame" />
				<Navbar />
                <SideBar url={this.props.match.url}/> 
                <div className="popover-alert">
                    
                </div>
                
                <div className="master-frame">
					<div className="right-panel">    
                       
                        <Switch>
                            <Route exact path={`${this.props.match.path}/setting`} component={Tabs}/>
                            <Route exact path={`${this.props.match.path}/setting/:page`} component={Tabs}/>
                            <Route exact path={`${this.props.match.path}/employee_group`} component={EmployeeGroup}/>
                            <Route exact path={`${this.props.match.path}/employee_group/insert`} component={insertEmployeeGroup}/>
                            <Route exact path={`${this.props.match.path}/employee_group/edit/:id`} component={insertEmployeeGroup}/>
                            <Route exact path={`${this.props.match.path}/employee_group/detail/:id`} component={detailEmployeeGroup}/>
                            
                            <Route exact path={`${this.props.match.path}/employee`} component={Employee}/>
                            <Route exact path={`${this.props.match.path}/employee/insert`} component={formEmployee}/>
                            <Route exact path={`${this.props.match.path}/employee/edit/:id`} component={formEmployee}/>
                            <Route exact path={`${this.props.match.path}/presence/:id`} component={Presence}/>
                            <Route exact path={`${this.props.match.path}`} component={Home} />
                        </Switch>
                     
                    </div>
                </div>
			</div>
            </BrowserRouter>
			
	  	);
	}
}
const DashboardPage = withRouter(Dashboard);

export default DashboardPage;