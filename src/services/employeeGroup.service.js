import axios from 'axios';
import config from '../config/config';


export const employeeGroupServices = {
    get,
    post,
    put,
    deleteDetail
};

function get(apiEndpoint, employeeGroup){
    return axios.get(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function post(apiEndpoint, payload){
    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function put(apiEndpoint, payload){
    return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, getOptions()).then((response)=>{
        return response;
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function deleteDetail(apiEndpoint){
    return axios.delete(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function getOptions(){
    let options = {}; 
    if(localStorage.getItem('token')){
        options.headers = { "Authorization" : "Bearer "+localStorage.getItem('token'),'x-access-token': localStorage.getItem('token') };
    }
    return options;
}