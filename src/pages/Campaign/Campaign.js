import React from 'react';
import { connect } from 'react-redux';
import {
	BrowserRouter as Router,
    withRouter
  } from "react-router-dom";
import { campaignAction } from '../../actions';
import DataTable, { createTheme } from 'react-data-table-component';
import Moment from "moment"
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Campaign extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
			
        };
        
	}
	
	componentDidMount(){
        const { dispatch } = this.props;
        dispatch(campaignAction.getCampaign());
	}

    handleClick = (event, id) => {
        const { dispatch } = this.props;
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            dispatch(campaignAction.deleteCampaignById(id));
        }
    }
    statusDisplay=(status)=>{
        switch (status) {
            case 0:
                return 'Not Published Yet'
                break;
            case 1:
                return 'Published'
                break;
            case 2:
                return 'Unpublished'
                break;
            default:
                break;
        }
    }
    displayDateFormat=(date)=>{
        return date.substr(5,11)
    }
	render(){
        const { campaign } = this.props.campaign;
        
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Campaign</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Campaign</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">
                            
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                                    <div className="button-row">
                                        <div className="btn-group" >
                                            <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/campaign/insert")}><i className="far fa-plus-square"></i>Create</button>
                                        </div>
                                    </div>
                                    {
                                        // <div className="form-group">
                                            <DataTable
                                                title="Campaign"
                                                pagination={true}
                                                columns={[
                                                    {
                                                        name: 'Campaign Name',
                                                        selector: 'name',
                                                        sortable: true,
                                                    },
                                                    {
                                                        name: 'Fee',
                                                        selector: 'fee',
                                                        sortable: true,
                                                    },
                                                    {
                                                        name: 'From Date',
                                                        selector: 'from_date',
                                                        sortable: true,
                                                        cell: row => this.displayDateFormat(row.from_date)
                                                    },
                                                    {
                                                        name: 'To Date',
                                                        selector: 'to_date',
                                                        sortable: true,
                                                        cell: row => this.displayDateFormat(row.to_date)
                                                    },
                                                    {
                                                        name: 'Total Voucher',
                                                        selector: 'total_voucher',
                                                        sortable: true,
                                                    },
                                                    {
                                                        name: 'Status Campaign',
                                                        selector: 'status',
                                                        sortable: true,
                                                        cell: row => this.statusDisplay(row.status)
                                                    },
                                                    {
                                                        name: 'Action',
                                                        selector: 'campaign_id',
                                                        cell: row => <span>
                                                            <button className="btn btn-primary icon-only" onClick={() => this.props.history.push("/dashboard/campaign/detail/"+row.campaign_id)}><i className="fas fa-info-circle"></i></button>
                                                                {
                                                                    (row.status==0 || row.status==2) ? 
                                                                    <span><button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/campaign/edit/"+row.campaign_id)}><i className="fas fa-edit"></i></button>
                                                                    <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.campaign_id)}><i className="fas fa-trash"></i></button></span> : null
                                                                }
                                                            </span>
                                                    }
                                                    ]}
                                                data={campaign}
                                                striped={true}
                                                customStyles={styleTable}
                                            />
                                        // </div> 
                                    }
                                    {/* <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Campaign Name</th>
                                                <th>Fee</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Total Voucher</th>
                                                <th>Status Campaign</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            campaign.length > 0 ? (
                                                campaign && campaign.map(data => {
                                                    return (
                                                        <tr key={data.name}>
                                                            <th>{data.campaign_id}</th>
                                                            <td>{data.name}</td>
                                                            <td>{data.fee}</td>
                                                            <td>{data.from_date}</td>
                                                            <td>{data.to_date}</td>
                                                            <td>{data.total_voucher}</td>
                                                            <td>{this.statusDisplay(data.status)}</td>
                                                            <td>
                                                                <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/campaign/detail/"+data.campaign_id)}><i className="fas fa-info-circle"></i>Review</button>
                                                                {
                                                                    data.status==0 ? 
                                                                    <div><button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/campaign/edit/"+data.campaign_id)}><i className="fas fa-cog"></i>Edit</button>
                                                                    <button className="btn btn-danger" onClick={(event) => this.handleClick(event, data.campaign_id)}><i className="fas fa-trash"></i>Delete</button></div> : null
                                                                }
                                                            </td>
                                                        </tr>
                                                    )
                                                    
                                                })
                                            ) : (
                                                <tr>
                                                    <td colSpan="7" align="center">Tidak Ada Data</td>
                                                </tr>
                                            )
                                        } 
                                        </tbody>
                                        </table> */}
                                    </div>      
                                </div>
                            </div>		
                        </div>
                    </div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{
    return {
        campaign : state.campaign
    };
  }

  const CampaignPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
  })(Campaign));
export default CampaignPage;
