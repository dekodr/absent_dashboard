import React from 'react';
import Message from "../components/Message"
import { campaignServices } from '../services';
import { history } from '../helpers';
import { error } from 'highcharts';

export const campaignAction = {
	getCampaign,
	getCampaignById,
	onChangeProps,
	onChangeCheckbox,
	onChangeCheckboxVoucher,
	onChangeCheckboxCustomer,
	editVendorInfo,
	createCampaign,
	deleteCampaignById,
	onChangeImage,
	onChangeNumber,
	handlePublish,
	handleUnpublish,
	onChangeOptionRequest,
	editCampaignInfo,
	resetStateCampaign,
	removeAlert,
	message,
	onChangeFromDate
};

function getCampaign(){
	return dispatch => {
		let apiEndpoint = 'campaign';
		campaignServices.get(apiEndpoint)
		.then((response)=>{
			dispatch(changeCampaignList(response.data.data));
		}).catch((err)=>{
			console.log("Error");
			console.log(err);
		})
	};
}


/*====================== Handle Option Request ========================*/
function onChangeOptionRequest(props, event, key){
	return dispatch =>{
		dispatch(handleOnChangeOptionRequest(props, event.target.value, key));
	}
	
}
export function handleOnChangeOptionRequest(props, value, key){
	return{
		type: "HANDLE_ON_CHANGE_OPTION_VOUCHER",
		props: props,
		value: value,
		key: key
	}
}
/*====================== END Handle Option Request ========================*/


function createCampaign(payload, props){

	payload.from_date   = formatDate(payload.from_date)+" 00:00:00";
	payload.to_date     = formatDate(payload.to_date)+" 23:53:59";
	console.log("payload")
	console.log(payload)
	return dispatch => {
		let apiEndpoint = 'campaign';
		if(payload._voucher.length==0){
			alert('You need to add 1 or more voucher!')
		}else if(payload.list_employee_group.length==0){
			alert('You need to add 1 or more employee group!')
		}else{
			campaignServices.post(apiEndpoint, payload)
			.then((response)=>{
				// dispatch(createCampaignInfo());
				
				if(response.status == 200 || response.status == 201){
					alert(response.data.message)
					dispatch(createCampaignInfo());
					dispatch(resetStateCampaign())
					props.history.push('/dashboard/campaign/detail/'+response.data.campaign_id+'#');

				}else if(response.message.indexOf("422") > -1){
					// alert()
					dispatch(message("error", "Please check again. Required field is empty."))
				}else if(response.message.indexOf("409") > -1 ){
					dispatch(message("error", response.response.data.message))
				}else{
					dispatch(message("error", response.data.message))
				}            
			}).catch(error=>{
				console.log(error)
				alert(error)
			})
		}
		
	}
}
/*====================== Handle Edit Campaign ========================*/
function editCampaignInfo(id, payload, props){
	payload.from_date   = formatDate(payload.from_date)+" 00:00:00";
	payload.to_date     = formatDate(payload.to_date)+" 23:53:59";
	
	let _voucher_mapping    = []
	payload.voucher.map((value, key)=>{
		_voucher_mapping[key]=value.voucher_id
	})
	payload._voucher_mapping = _voucher_mapping
	return dispatch => {
		let apiEndpoint = 'campaign/'+id;
		
		campaignServices.put(apiEndpoint, payload)
		.then((response)=>{

			if(response.status == 200 || response.status == 201){
				alert(response.data.message)
				dispatch(createCampaignInfo());
				dispatch(resetStateCampaign())
				props.history.push('/dashboard/campaign');
				
			}else if(response.message.indexOf("422") > -1){

				dispatch(message("error", "Please check again. Required field is empty."))
			}else if(response.message.indexOf("409") > -1 ){

				dispatch(message("error", response.response.data.message))
			}else{
				dispatch(message("error", response.data.message))
			}  
			
		}) 
	}
}
/*====================== END Handle Set Request Input ========================*/

function removeAlert(){
	return{
		type: "REMOVE_ALERT_CAMPAIGN"
	}
}
function getCampaignById(id){
	return dispatch => {
		let apiEndpoint = 'campaign/'+ id;
		campaignServices.get(apiEndpoint)
		.then((response)=>{
			// console.log(response)
			dispatch(editCampaignDetails(response.data.data));
		})
	};
}
/* FOR PUBLISH UNPUBLISH PURPOSES*/
function handlePublish(id, props){
	return dispatch => {
		let apiEndpoint = 'publish/'+ id;
		campaignServices.publish(apiEndpoint)
		.then((response)=>{
			dispatch(publishCampaign(response.data.data));
			if(response.status == 201){
				alert(response.data.message)
				dispatch(createCampaignInfo());
				dispatch(resetStateCampaign())
				props.history.push('/dashboard/campaign');
				
			}else if(response.message.indexOf("422") > -1){
				dispatch(message("error", "Please check again. Required field is empty."))
			}else if(response.message.indexOf("409") > -1 ){
				// dispatch(resetState())
				dispatch(message("error", response.response.data.message))
			}else{
				dispatch(message("error", response.data.message))
			}  
		})
	};
}
function handleUnpublish(id, props){
	return dispatch => {
		let apiEndpoint = 'unpublish/'+ id;
		campaignServices.unpublish(apiEndpoint)
		.then((response)=>{
			dispatch(unpublishCampaign());
			if(response.status == 201){
				alert(response.data.message)
				window.location.reload()
				props.history.push('/dashboard/campaign/detail/'+id);
			}else{
				dispatch(message("error", response.data.message))
			}  
		})
	};
}
/**END PUBLISH UNPUBLISH PURPOSES**/
function onChangeProps(props, event){
	return dispatch =>{
		dispatch(handleOnChangeProps(props, event.target.value));
	}
}

function onChangeCheckbox(props, event,field){
	return dispatch =>{
		dispatch(handleOnChangeCheckbox(props, event.target.value, field));
	}
}
function onChangeCheckboxVoucher(props, event,key){
	return dispatch =>{
		dispatch(handleOnChangeCheckboxVoucher(props, event.target.value, key));
	}
}
function onChangeCheckboxCustomer(props, event){
	return dispatch =>{
		dispatch(handleOnChangeCheckboxCustomer(props, event.target.value, event.target.id));
	}
}
function editVendorInfo(id, payload){
	return dispatch => {
		let apiEndpoint = 'campaign/'+ id;
		campaignServices.put(apiEndpoint, payload)
		.then((response)=>{
			dispatch(updatedUserInfo());
			history.push('/campaign');
		}) 
	}
}

function deleteCampaignById(id){
	return dispatch => {
		let apiEndpoint = 'campaign/'+ id;
		campaignServices.deleteDetail(apiEndpoint)
		.then((response)=>{
			if(response.status == 200 || response.status == 201){
				dispatch(message("success", response.data.message))
				dispatch(deleteCampaignDetails());
				dispatch(campaignAction.getCampaign());

				// redirecting
				// props.history.push('/dashboard/employee');
			}else{
				dispatch(message("error", response.response.data.message))
			}
		})
	};
}
function onChangeImage(props, name){
	return dispatch =>{
		dispatch(handleOnChangeImage(props, name));
	}
}

export function message(type,msg){
	// const message = 
	alert(msg);
	return{
		type    : "MESSAGE",
		status  : type,
		// message : <Message type={type} text={msg}/>,
		message : ''

	}
}

export function changeCampaignList(campaign){
	return{
		type: "FETCHED_ALL_CAMPAIGN",
		campaign: campaign
	}
}

export function handleOnChangeProps(props, value){
	return{
		type: "HANDLE_ON_CHANGE",
		props: props,
		value: value
	}
}
export function handleOnChangeCheckboxVoucher(props, value, key){
	return{
		type: "HANDLE_ON_CHANGE_CHECKBOX_VOUCHER",
		props: props,
		value: value,
		key: key
	}
}
export function handleOnChangeCheckbox(props, value){
	return{
		type: "HANDLE_ON_CHANGE_CHECKBOX",
		props: props,
		value: value
	}
}
export function handleOnChangeCheckboxCustomer(props, value, id){
	return{
		type: "HANDLE_ON_CHANGE_CHECKBOX_CUSTOMER",
		props: props,
		value: value,
		id:id
	}
}
function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) 
		month = '0' + month;
	if (day.length < 2) 
		day = '0' + day;

	return [year, month, day].join('-');
}
function onChangeNumber(prop, event, key){
	return{
		type: "HANDLE_ON_CHANGE_NUMBER",
		props: prop,
		value: event.target.value,
		key:key
	}
}
export function editCampaignDetails(campaign){ 
	// console.log(campaign,">>>")
	let from_date   = formatDate(campaign.from_date)+" 00:00:00";
	let to_date     = formatDate(campaign.to_date)+" 23:53:59";
	let _voucher = []
	let max_per_day = []
	let max_per_month = []
	let limit = []
	campaign.voucher_list_id.map((value, key)=>{
		_voucher[key] = value['voucher_id']
		max_per_day[key] = value['max_per_day'];
		max_per_month[key] = value['max_per_month'];
		limit[key] = value['limit']
	})
	return{
		type: "CAMPAIGN_DETAIL",
		// category_id: campaign.campaign_id,
		name: campaign.name,
		fee: campaign.fee,
		from_date: from_date,
		to_date: to_date,
		total_voucher: campaign.total_voucher,
		// max_per_day: campaign.max_per_day,
		// max_per_month: campaign.max_per_month,
		voucher_list_id: campaign.voucher_list_id,
		emp_list_id: campaign.emp_list_id,
		list_customer_group :campaign.cust_list_id,
		list_employee_group: campaign.emp_group_list_id,
		banner: campaign.banner,
		_voucher:_voucher,
		max_per_day:max_per_day,
		max_per_month:max_per_month,
		limit:limit,
		status:campaign.status,
		customer_group_id:campaign.cust_group_list_id,
		messages: campaign.messages,
		wrong_code: campaign.wrong_code,
		employee_no_promo: campaign.employee_no_promo,
		empty_slot: campaign.empty_slot,
		wrong_index: campaign.wrong_index,
		max_reached: campaign.max_reached,
		no_campaign: campaign.no_campaign,
		empty_voucher: campaign.empty_voucher,
		last_voucher: campaign.last_voucher,
		employee_greeting: campaign.employee_greeting,
		employee_mesages: campaign.employee_mesages,
		employee_greeting_img: campaign.employee_greeting_img
	}
}
function onChangeFromDate(field, date){
	return{
		type: "UPDATE_DATE_CAMPAIGN",
		field: field,
		date:date
	}
}
export function updatedUserInfo(){
	return{
		type: "USER_UPDATED"
	}
}

export function createCampaignInfo(){
	return{
		type: "USER_CREATED_SUCCESSFULLY"
	}
}
export function publishCampaign(){
	return{
		type: "PUBLISH_CAMPAIGN"
	}
}
export function unpublishCampaign(){
	return{
		type: "UNPUBLISH_CAMPAIGN"
	}
}
export function deleteCampaignDetails(){
	return{
		type: "DELETED_VENDOR_DETAILS"
	}
}
export function resetStateCampaign(){
	return{
		type: "RESET_STATE_CAMPAIGN"
	}
}
export function handleOnChangeImage(props, name){
	return{
		type: "HANDLE_ON_CHANGE",
		props: name,
		value: props
	}
}
export function setMessage(type, message){
	return{
		type: "SET_MESSAGE_CAMPAIGN",
		status_message: type,
		message: message
	} 
}
