
import { presenceServices } from '../services';

export const presenceAction = {
    getPresenceById,
    resetState
};

function getPresenceById(id){


    return dispatch => {
        let apiEndpoint = 'history/'+id;
        presenceServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(changePresenceList(response.data));
        }).catch((err)=>{
            console.log("Error>>");
            console.log(err);
        })
    };
}

function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}

export function changePresenceList(presence){
    return{
        type: "FETCHED_ALL_PRESENCE",
        presence: presence.data,
        total:presence.total,
        total_page:presence.total_page
    }
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_PRESENCE"
	}
}
