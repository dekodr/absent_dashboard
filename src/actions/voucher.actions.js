import { voucherServices } from '../services';
import { history } from '../helpers';
export const voucherAction = {
	getVoucher,
	getVoucherById,
	onChangeProps,
	editVoucherInfo,
	createVoucher,
	onChangeImage,
	onChangeUpload,
	deleteVoucherById,
	getVoucherCodeById,
	deleteVoucherCode,
	resetState,
	message,
	getVoucherByCampaign
};

function getVoucher(){
	return dispatch => {
		let apiEndpoint = 'voucher';
		voucherServices.get(apiEndpoint)
		.then((response)=>{
			console.log(response);
			dispatch(changeVoucherList(response.data.data));
		}).catch((err)=>{
			console.log("Error");
			console.log(err);
		})
	};
}


function getVoucherByCampaign(id_campaign){
	return dispatch => {
		let apiEndpoint = 'voucher_by_campaign/'+id_campaign;
		voucherServices.get(apiEndpoint)
		.then((response)=>{
			console.log(response);
			dispatch(changeVoucherList(response.data.data));
		}).catch((err)=>{
			console.log("Error");
			console.log(err);
		})
	};
}
function getVoucherById(id){

	return dispatch => {
		let apiEndpoint = 'voucher/'+ id;
		voucherServices.get(apiEndpoint)
		.then((response)=>{
			console.log(response)
			dispatch(editVoucherDetails(response.data.data));
		})
	};
}
function getVoucherCodeById(id){
	return dispatch => {
		let apiEndpoint = 'voucher_code/'+ id;
		voucherServices.getCode(apiEndpoint)
		.then((response)=>{
			console.log(response)
			dispatch(setVoucherID(response.data.data));
		})
	};
}
function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}
function editVoucherInfo(id, payload, props){
	return dispatch => {
		let apiEndpoint = 'voucher/'+ id;
		let _payload = {
			'voucher_name': payload.voucher_name,
			'total_voucher':payload.total_voucher,
			'type_voucher':payload.type_voucher,
			'template': payload.template,
			'product_id':payload.product_id,
			'code_csv':payload.code_csv
		}
		voucherServices.put(apiEndpoint, _payload, props)
		.then((response)=>{

			if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
				dispatch(updatedVoucherInfo());
				props.history.push('/dashboard/voucher');

                // redirecting
                // props.history.push('/dashboard/employee');
            }else if(response.message.indexOf(422)){
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                dispatch(message("error", response.data.message))
            }  
		}).catch((response)=>{
			alert('Fail to update data!')
		})
	}
}
function createVoucher(payload, props){
	return dispatch => {
		dispatch(showLoading());
		let apiEndpoint = 'voucher';
		let _payload = {
			'voucher_name': payload.voucher_name,
			'total_voucher':payload.total_voucher,
			'type_voucher':payload.type_voucher,
			'template': payload.template,
			'product_id':payload.product_id,
			'code_csv':payload.code_csv
		}
		voucherServices.post(apiEndpoint, _payload)
		.then((response)=>{
			if(response.status=='200'||response.status=='201'){
				dispatch(message("success", response.data.message))
				dispatch(createVoucherInfo());
				dispatch(hideLoading())
				dispatch(resetStateHandler())
				props.history.push('/dashboard/voucher'); 
			}else if(response.message.indexOf("422") > -1){
                // alert()
				dispatch(message("error", "Please check again. Required field is empty."))
				dispatch(hideLoading())
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
				dispatch(message("error", response.response.data.message))
				dispatch(hideLoading())
            }else{
				alert('Fail to Insert data!')
				dispatch(hideLoading())
			}
			
		}) 
	}
}

function onChangeProps(props, event){
	return dispatch =>{
		dispatch(handleOnChangeProps(props, event.target.value));
	}
}
function onChangeImage(props, name){
	return dispatch =>{
		dispatch(handleOnChangeImage(props, name));
	}
}
function onChangeUpload(props, name){
	return dispatch =>{
		dispatch(handleOnChangeUpload(props, name));
	}
}

function editVendorInfo(id, payload){
	return dispatch => {
		let apiEndpoint = 'voucher/'+ id;
		voucherServices.put(apiEndpoint, payload)
		.then((response)=>{

			dispatch(updatedVoucherInfo());
			history.push('/voucher');
		}) 
	}
}

function deleteVoucherById(id){
	return dispatch => {
		let apiEndpoint = 'voucher/'+ id;
		voucherServices.deleteDetail(apiEndpoint)
		.then((response)=>{
			if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
				dispatch(deleteVoucherDetails());
				dispatch(voucherAction.getVoucher());

                // redirecting
                // props.history.push('/dashboard/employee');
            }else{
                dispatch(message("error", response.response.data.message))
            }
		})
	};
}

function deleteVoucherCode(voucher_id, voucher_code_id){
	return dispatch => {
		let apiEndpoint = 'voucher_code/'+ voucher_code_id;
		voucherServices.deleteDetail(apiEndpoint)
		.then((response)=>{
			dispatch(deleteVoucherCodeDetails(voucher_code_id));
			dispatch(voucherAction.getVoucherCodeById(voucher_id));
		})
	};
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''
    }
}

export function changeVoucherList(voucher){
	return{
		type: "FETCHED_ALL_VOUCHER",
		voucher: voucher
	}
}

export function handleOnChangeProps(props, value){
	return{
		type: "HANDLE_ON_CHANGE",
		props: props,
		value: value
	}
}

export function handleOnChangeImage(props, name){
	return{
		type: "HANDLE_ON_CHANGE",
		props: name,
		value: props
	}
}
export function handleOnChangeUpload(props, name){
	return{
		type: "HANDLE_ON_CHANGE",
		props: name,
		value: props
	}
}

export function editVoucherDetails(voucher){ 
	console.log("voucher>>>",voucher)
	return{
		type: "VOUCHER_DETAIL",
		voucher_id: voucher.id,
		total_voucher: voucher.total_voucher,
		image: voucher.image,
		type_voucher: voucher.type_voucher,
		voucher_name: voucher.voucher_name,
		image: voucher.image,
		product_id: voucher.product_id,
		is_linked: voucher.is_linked,
	}
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_VOUCHER"
	}
}
export function setVoucherID(voucher_code_list){
	return{
		type: "VOUCHER_CODE_LIST_BY_ID",
		voucher_code_list: voucher_code_list
	}
}
export function updatedVoucherInfo(){
	return{
		type: "VOUCHER_UPDATED"
	}
}
export function showLoading(){
	return{
		type: "SHOW_LOADING_VOUCHER_ENTRY"
	}
}
export function hideLoading(){
	return{
		type: "HIDE_LOADING_VOUCHER_ENTRY"
	}
}

export function createVoucherInfo(){
	return{
		type: "VOUCHER_CREATED_SUCCESSFULLY"
	}
}

export function deleteVoucherDetails(){
	return{
		type: "DELETE_VOUCHER_DETAILS"
	}
}
export function deleteVoucherCodeDetails(){
	return{
		type: "DELETED_VENDOR_CODE_DETAILS"
	}
}