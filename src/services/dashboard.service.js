import axios from 'axios';
import config from '../config/config';
export const dashboardService = {
    getActivePromo,
    getBroadcaster,
    getTotalHit,
    getTopVoucher,
    getTopVoucherLabel,
    getTopProduct,
    getTopSales,
    getVoucherRedeemByLocation,
    getVoucherRedeemByCampaign
};

function getActivePromo(){
    // return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {

    return axios.get(config.baseUrl+'voucher/active?merchant_id='+localStorage.getItem('merchant_id'),{
        
     },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        return response.data.total;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function getBroadcaster(){
    return axios.get(config.baseUrl+'broadcaster?merchant_id='+localStorage.getItem('merchant_id'),{
        // "query":{
        //     "bool":{
        //         "must":[
        //             {
        //                 "exists":{
        //                     "field":"code_name"
        //                 }
        //             }	
        //         ]
        //     }
        // }
    },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        
        return response.data.broadcaster;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function getTotalHit(){
    return axios.get(config.baseUrl+'hit/total?merchant_id='+localStorage.getItem('merchant_id'),{
        // "query":{
        //     "match_all":{}
        // }
    },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        
        return response.data.total;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function getTopVoucher(){
    return axios.get(config.baseUrl+'voucher/top?merchant_id='+localStorage.getItem('merchant_id'),{
        
    },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function getTopVoucherLabel(list){
    return axios.post(config.baseUrl+'get_top_product_label',{"data":list},{
        headers:{
            'Content-Type':'application/json',
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
function getTopProduct(){
    return axios.get(config.baseUrl+'hit/product?merchant_id='+localStorage.getItem('merchant_id'),{
        headers:{
            'Content-Type':'application/json',
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}
async function getTopSales(){
    return await axios.get(config.baseUrl+'hit/total_sales?merchant_id='+localStorage.getItem('merchant_id'),{
        headers:{
            'Content-Type':'application/json',
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}

function getVoucherRedeemByLocation(){
    // return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {

    return axios.get(config.baseUrl+'voucher_redeem_by_location?merchant_id='+localStorage.getItem('merchant_id'),{
        
     },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        return response.data;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}

function getVoucherRedeemByCampaign(){
    // return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {

    return axios.get(config.baseUrl+'voucher_redeem_by_campaign_name?merchant_id='+localStorage.getItem('merchant_id'),{
        
     },{
        headers:{
            'Content-Type':'application/json',
        }
    }).then((response)=>{
        return response.data;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}