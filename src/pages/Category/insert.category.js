import React from 'react';
import { connect } from 'react-redux';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
// import {transactionService} from '../api/transaction.js';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import {categoryAction} from '../../actions';
import $ from 'jquery'

class InsertCategory extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			category: [],
			
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(categoryAction.onChangeProps(prop, event));
    };
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
        // dispatch(categoryAction.getCategory());

        dispatch(categoryAction.resetState());
        if(params.id){
            this.setState({button: 'Update'})
            dispatch(categoryAction.getCategoryById(params.id));
        } else {
            this.setState({button: 'Insert'})
        }
        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
    }
    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
            
        let payload={
            category_name  : this.props.category.category_name,
        }

        if(params.id){
            dispatch(categoryAction.editCategoryInfo(params.id, payload, this.props));
            dispatch(categoryAction.resetState());
        }else{
            dispatch(categoryAction.createCategory(payload, this.props));
            // this.props.history.push('/dashboard/product');
            // history.push('/dashboard');
        }
    }
	render(){
        
        const { match : {params }} = this.props;
        const { category } = this.props.category;

        console.log(this.props.category)

		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Category</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Category</div>
                            <div className="breadcrumb-item">
                                {
                                    this.state.button+" Category"
                                }
                            </div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.category.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">
                                {
                                    this.state.button+" Category"
                                }
                            </h2>
                            <div className="card-content">
                                
                                <form className="form" onkeydown="return event.key != 'Enter';">
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Category</label>
                                        <input type="text" value={this.props.category.category_name} className="form-control" 
                                         onChange={this.handleChange('category_name')}/>
                                    </div>
                                    {/* <div className="button-row">
                                        <div className="btn-group" >
                                            {
                                                this.state.button == "Update" ? (
                                                    <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                                ) : (
                                                    <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-save"></i>Insert</button>
                                                )                                                
                                            }
                                            <button className="btn" onClick={() => this.props.history.push('/dashboard/category')}>Cancel</button>
                                       
                                        </div>
                                    </div> */}
                                </form>
                            </div>
                            </div>
                  </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
                                {
                                    this.state.button == "Update" ? (
                                        <button type="button" className="btn btn-primary" 
                                    onClick={(event) => this.handleSubmit(event)} disabled={this.props.category.is_linked}><i className="fas fa-pen-alt"></i>Update</button>
                                    ) : (
                                        <button type="button" className="btn btn-primary" 
                                    onClick={(event) => this.handleSubmit(event)}><i className="fas fa-save"></i>Insert</button>
                                    )                                                
                                }
                                <button className="btn" onClick={() => this.props.history.push('/dashboard/setting/category')}>Cancel</button>
								</div>
                                {this.props.category.is_linked ? <p>*Cannot update product: data already linked to a running campaign</p>:''}
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const InsertCategoryPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(InsertCategory));
export default InsertCategoryPage;
