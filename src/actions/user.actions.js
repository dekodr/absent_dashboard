import { userService } from '../services';
import { history } from '../helpers';

export const userActions = {
    login,
    logout
};

function login(username, password, props){
    // console.log(username)
    return dispatch => {
        let apiEndpoint = 'login';
        let payload = {
            username: username,
            password: password,
            apps_key: 'telegram_bot'
        }
        userService.post(apiEndpoint, payload)
        .then((response)=>{
            console.log(response)
            if(typeof response!='undefined'){
                if (response.data.token) {
                    localStorage.setItem('token', response.data.token);
                    localStorage.setItem('auth', 1);
                    localStorage.setItem('merchant_id', response.data.merchant_id);
                    localStorage.setItem('username', response.data.username);
                    dispatch(setUserDetails(response.data));
                    props.history.push('/dashboard');
                }
            }
        })
    };
}

function logout(){
    return dispatch => {
        localStorage.removeItem('auth');
        localStorage.removeItem('token');
        dispatch(logoutUser());
        history.push('/');
        window.location.reload();
    }
}

export function setUserDetails(user){
    return{
        type: "LOGIN_SUCCESS",
        auth: 1,
        token: user.token
    }
}

export function logoutUser(){
    return{
        type: "LOGOUT_SUCCESS",
        auth: false,
        token: ''
    }
}