import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';


import $ from "jquery";
import { presenceAction } from '../../actions';
import "../../asset/css/pagination.css";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Presence extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            page: 1
        }
    }
    
    componentDidMount(){
        const { dispatch, presence, match : {params } } = this.props;
        this.props.resetState()
        this.props.getPresenceById(params.id)

    }

    render(){
        const { presence } = this.props.presence;
        return (
            <div>
                <div className="p-20-c">
                    <div className="breadcrumb">
                        <div className="page-title">Presence</div>
                        <div className="page-info">
                            <div className="breadcrumb-item">Home</div>
                            <div className="breadcrumb-item">Presence</div>
                        </div>
                    </div>
                </div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                {
                
                    <DataTable
                        title="Presence"
                        pagination={true}
                        columns={[
                            {
                                name: 'Name',
                                selector: 'name',
                                sortable: true,
                            },
                            {
                                name: 'Check In',
                                selector: 'check_in',
                                sortable: true,
                            },
                            {
                                name: 'Check Out',
                                selector: 'check_out',
                                sortable: true,
                            }
                            ]}
                        data={presence}
                        striped={true}
                        customStyles={styleTable}
                    />
                }   
                                    </div>      
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        presence : state.presence
    };
  }
const mapDispatchToProps = (dispatch)=>{
    
    return {
        
        getPresenceById: (id) => {
            dispatch(presenceAction.getPresenceById(id))
        },
        resetState:()=>{
            dispatch(presenceAction.resetState())
        },
        
    }
}
  const presencePage = connect(mapStateToProps,mapDispatchToProps, null, {
    pure: false
  })(Presence);
export default presencePage;
