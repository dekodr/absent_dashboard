import React from 'react';
import Message from "../components/Message"
import { customer_groupServices } from '../services';
import { history } from '../helpers';

export const customer_groupAction = {
    getCustomerGroupById,
    getCustomer_group,
    onChangeProps,
    createCustomer_group,
    deleteCustomerGroup,
    onChangeCheckbox,
    editCustomerGroupInfo,
    getCustomer_group_selected,
    resetState,
    message,
    getCustomerListByGroupId
};

function getCustomer_group(){
    return dispatch => {
        let apiEndpoint = 'customer_group';
        customer_groupServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(changeCustomer_groupList(response.data.data));
        }).catch((err)=>{
            console.log(err);
        })
    };
}
function getCustomer_group_selected(id_campaign){
    return dispatch => {
        let apiEndpoint = 'customer_group_by_campaign/'+id_campaign;
        customer_groupServices.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            dispatch(changeCustomer_groupList(response.data.data));
        }).catch((err)=>{
            console.log(err);
        })
    };
}
function createCustomer_group(payload, props){
    return dispatch => {
        let apiEndpoint = 'customer_group';
        customer_groupServices.post(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(createCustomerGroupInfo());
                props.history.push('/dashboard/setting/customer_group');

                // redirecting
                // props.history.push('/dashboard/employee');
            }else if(response.message.indexOf("422") > -1){
                // alert()
                dispatch(message("error", "Please check again. Required field is empty."))
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }else{
                dispatch(message("error", response.data.message))
            }
        }) 
    }
}

function getCustomerGroupById(id){

    return dispatch => {
        let apiEndpoint = 'customer_group/'+ id;
        customer_groupServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(editCustomerGroupDetails(response.data.data));
        })
    };
}

function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}

function editCustomerGroupInfo(id, payload, props){
    return dispatch => {
        let apiEndpoint = 'customer_group/'+ id;
        customer_groupServices.put(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                alert(response.data.message)
                // dispatch(message("success", response.data.message))
                dispatch(updatedCustomerGroupInfo());
                props.history.push('/dashboard/setting/customer_group');
            }else if(response.message.indexOf(422)){
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                dispatch(message("error", response.response.data.message))
            }
        }) 
    }
}

function onChangeCheckbox(props, event,field){
    return dispatch =>{
        // console.log(dispatch)
        dispatch(handleOnChangeCheckbox(props, event.target.value, field));
    }
}
function deleteCustomerGroup(id){
    return dispatch => {
        let apiEndpoint = 'customer_group/'+ id;
        customer_groupServices.deleteDetail(apiEndpoint)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteCustomer_Details());
                dispatch(customer_groupAction.getCustomer_group());
            }else{
                dispatch(message("error", response.response.data.message))
            }
        })
    };
}
function getCustomerListByGroupId(id){
    return dispatch => {
        let apiEndpoint = 'customer_by_group_id/'+ id;
        customer_groupServices.get(apiEndpoint)
        .then((response)=>{
            console.log(response)
            dispatch(handlerGetCustomerListByGroupId(response.data.data));
        })
    };
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_CUSTOMER_GROUP"
	}
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}

export function changeCustomer_groupList(customer_group){
    return{
        type: "FETCHED_ALL_CUSTOMER_GROUP",
        customer_group: customer_group
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editCustomerGroupDetails(CustomerGroup){
    // console.log(">>>",CustomerGroup)
    return{
        type: "CustomerGroup_DETAIL",
        customer_group_id: CustomerGroup.id,
        name: CustomerGroup.name,
        cust_list_id: CustomerGroup.cust_list_id,
    }
}


export function updatedCustomerGroupInfo(){
    return{
        type: "CUSTOMER_GROUP_UPDATED"
    }
}

export function createCustomerGroupInfo(){
    return{
        type: "CUSTOMER_GROUP_CREATED_SUCCESSFULLY"
    }
}

export function deleteCustomer_Details(){
    return{
        type: "DELETED_CUSTOMER_GROUP_DETAILS"
    }
}
export function handleOnChangeCheckbox(props, value){
    return{
        type: "HANDLE_ON_CHANGE_CHECKBOX_CUSTOMER_GROUP",
        props: props,
        value: value
    }
}
export function resetStateCampaign(){
    return{
        type: "RESET_STATE_CUSTOMER_GROUP"
    }
}
export function handlerGetCustomerListByGroupId(customer){
    return{
        type: "HANDLER_GET_CUSTOMER_LIST_BY_GROUP_ID",
        customer: customer
    }
}