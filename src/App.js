import React, { Component } from "react";
import { Route,  Switch, BrowserRouter } from "react-router-dom";

import "./asset/scss/index.scss";

import { history } from './helpers';
import {Login} from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import { PrivateRoute } from './components';
class App extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <BrowserRouter history={history}>    
        <div className="master-body">
                  
                <Switch>
                  <Route exact path='/' component={Login} />
                  <Route path='/dashboard' component={Dashboard} />
                </Switch>
           
        </div>
        </BrowserRouter>
        
    );
  }
}

export default App;
