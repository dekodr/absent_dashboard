const initialState = { anchor: 'left',
	presence: [],
	open: false,
	total:0,
	total_page: 0, 
 };


export function presence(state = initialState, action) {
	switch (action.type) {
		case 'FETCHED_ALL_PRESENCE':
			return {
			...state,
			presence: action.presence,
			total: action.total,
			total_page: action.total_page
			};
		case "RESET_STATE_PRESENCE":
			return initialState
		default:
			return state
	}
  }