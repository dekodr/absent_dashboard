import React from 'react';
import { connect } from 'react-redux';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
    withRouter
  } from "react-router-dom";
import {customerGroupAction,categoryAction,customerAction,voucherAction, customer_groupAction} from '../../actions';

class DetailCustomerGroup extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			campaign: [],
			
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(customerAction.onChangeProps(prop, event));
    };
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
        dispatch(categoryAction.getCategory());
        // dispatch(customerAction.getCustomer());
        dispatch(voucherAction.getVoucher());
        
        if(params.id){
            dispatch(customer_groupAction.getCustomerGroupById(params.id));
            dispatch(customer_groupAction.getCustomerListByGroupId(params.id));
        }
    }
    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
        
        let cust_list_id = "["+this.props.customer_group.cust_list_id.join(", ")+"]"
        let payload={
            name: this.props.customer_group.name,
            // cust_list_id:cust_list_id,
        }

        if(params.id){
            dispatch(customer_groupAction.editCustomerGroupInfo(params.id, payload));
        }else{
            dispatch(customer_groupAction.createCustomer_group(payload, this.props));
        }
    }
    handleChangeCheckbox = prop => event => {
        const { dispatch } = this.props;
        // console.log(prop)
        dispatch(customer_groupAction.onChangeCheckbox(prop, event));
        // console.log(this.props)
    };
	render(){
        
        const { match : {params }, customer_group} = this.props;
        const { category } = this.props.category;
        const { list_customer } = this.props.customer_group;
        const { voucher } = this.props.voucher;
        const custom = this.props.customer_group.cust_list_id;

        console.log(this.props.customer_group);
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Customer Group</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Customer</div>
                            <div className="breadcrumb-item">{this.state.status} Customer Group</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.customer_group.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">{this.state.status} Customer Group</h2>
                            <div className="card-content">
                                
                                <form className="form">
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Customer Group Name</label>
                                        <input type="text" value={this.props.customer_group.name} className="form-control" 
                                         onChange={this.handleChange('name')} disabled  />
                                    </div>
                                    <h3>Customer List</h3>
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Telephone</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            list_customer && list_customer.map(data => {
                                                
                                                // if(typeof custom != 'undefined'){
                                                //     if((custom.indexOf(data.customer_id) > -1) == true){
                                                        return (
                                                            <tr key={data.customer_id}>
                                                                <td>{data.customer_id}</td>
                                                                <td>{data.first_name}</td>
                                                                <td>{data.last_name}</td>
                                                                <td>{data.phone_no}</td>
                                                            </tr>
                                                        )
                                                //     }
                                                // }
                                            })
                                        }
                                        </tbody>
                                    </table>
                                    
                                </form>
                            </div>
                            </div>
                          </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
                                <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/setting/customer_group')}>Cancel</button>

								</div>
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const DetailCustomerGroupPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(DetailCustomerGroup));
export default DetailCustomerGroupPage;
