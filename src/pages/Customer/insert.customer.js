import React from 'react';
import { connect } from 'react-redux';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
// import {transactionService} from '../api/transaction.js';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import {customerAction,customer_groupAction} from '../../actions';
import $ from 'jquery';

class Customer extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			customer: [],
			
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(customerAction.onChangeProps(prop, event));
    };
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
        dispatch(customerAction.resetState());
        dispatch(customerAction.getCustomer());
        dispatch(customer_groupAction.getCustomer_group());

        if(params.id){
            this.setState({status : 'Update'});
            dispatch(customerAction.getCustomerById(params.id));
        } else {
            this.setState({status : 'Insert'});
        }

        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
    }
    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
            
        let payload={
            first_name          : this.props.customer.first_name,
            last_name           : this.props.customer.last_name,
            phone_no            : this.props.customer.phone_no,
            customer_group_id   : this.props.customer.customer_group_id
        }

        if(params.id){
            dispatch(customerAction.editCustomerInfo(params.id, payload, this.props));
            dispatch(customerAction.resetState());
        }else{
            dispatch(customerAction.createCustomer(payload, this.props));
            dispatch(customerAction.resetState());
        }
    }
	render(){
        
        const { match : {params }} = this.props;
        const { customer } = this.props.customer;
        const customergroup  = this.props.customer_group.customer_group;
        console.log(this.props.customer,"<<<<<")
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Customer</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Customer</div>
                            <div className="breadcrumb-item">{this.state.status} Customer</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.customer.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">{this.state.status} Customer</h2>
                            <div className="card-content">
                                
                                <form className="form">
                                    <div className="form-group" style={{'background':(this.props.customer.is_elastic) ? '#cdcdcd' : ''}}>
                                        <label htmlFor="#" className="form-label">Telephone</label>
                                        <span class="number">+62</span>
                                        <input  type="number" inputmode="numeric" pattern="[0-9]*" type="tel" value={this.props.customer.phone_no} className="form-control number" 
                                         onChange={this.handleChange('phone_no')}
										 disabled={this.props.customer.is_elastic} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">First Name</label>
                                        <input type="text" value={this.props.customer.first_name} className="form-control" 
                                         onChange={this.handleChange('first_name')}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Last Name</label>
                                        <input type="text" value={this.props.customer.last_name} className="form-control" 
                                         onChange={this.handleChange('last_name')}/>
                                    </div>
                                    
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Customer Group</label>
                                        <select value={this.props.customer.customer_group_id} className="form-control" onChange={this.handleChange('customer_group_id')}>
                                            <option value="">--Choose one Customer Group--</option>
                                            {
                                                customergroup && customergroup.map(data => {
                                                    return (
                                                        <option key={data.customer_group_id} value={data.customer_group_id}>
                                                            {data.name}
                                                        </option>
                                                    )
                                                })
                                            }
                                        </select>
                                        {/*<select value={this.props.employee.employee_group_id} className="form-control" onChange={this.handleChange('employee_group_id')}>
                                            <option value="">--Choose one Employee Group--</option>
                                            {
                                                employeegroup.map(data => {
                                                    return (
                                                        <option key={data.employee_group_id} value={data.employee_group_id}>
                                                            {data.name}
                                                        </option>
                                                    )
                                                })
                                            }
                                        </select> */}
                                    </div>
                                    {/* <div className="form-group">
                                        <label htmlFor="#" className="form-label">Username</label>
                                        <input type="text" value={this.props.customer.username} className="form-control" 
                                         onChange={this.handleChange('username')}/>
                                    </div> */}
                                    {/* <div className="button-row">
                                        <div className="btn-group" >
                                        {
                                            this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                            onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> :
                                            <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                        }
                                        <button className="btn" onClick={() => this.props.history.push('/dashboard/customer')}>Cancel</button>
                                       
                                        </div>
                                    </div> */}
                                </form>
                            </div>
                            </div>
                  </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
                                {
                                    this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                    onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> :
                                    <button type="button" className="btn btn-primary" 
                                        onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                }
                                <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/customer')}>Cancel</button>

									{/* <button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> */}
							
								</div>
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const CustomerPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(Customer));
export default CustomerPage;
