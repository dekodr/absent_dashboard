import React from 'react';
import { connect } from 'react-redux';
import dropify from "dropify";

import "../../../node_modules/dropify/src/sass/dropify.scss";
import {
	withRouter
} from "react-router-dom";
import {employeeAction,employeeGroupAction} from '../../actions';
import $ from 'jquery';
class InsertEmployee extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			employee: [],
			isUpdate:1
		};
	}
	handleChange = prop => event => {
		const { dispatch } = this.props;

		dispatch(employeeAction.onChangeProps(prop, event));
	};
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
		dispatch(employeeAction.resetState())

		if(params.id){
			this.setState({status: 'Update'})
			dispatch(employeeAction.getEmployeeById(params.id));
		} else {
			this.setState({status: 'Insert'})
		}

        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
	}

	handleUploadImage = (prop, name) => event => {
		const { dispatch } = this.props;
		dispatch(employeeAction.onChangeImage(prop.files[0], name));
	};

	handleSubmit(event){
		const { match : {params } } = this.props;
		const { dispatch } = this.props;
		window.scrollTo(0, 0)


		let payload={
			name		: this.props.employee.name,
			email		: this.props.employee.email,
			nik			: this.props.employee.nik,
			card_id		: this.props.employee.card_id,
			phone_no    : this.props.employee.phone_no,
			photo    	: this.props.employee.photo,
			company_id  : 1
		}
		if(this.props.employee.image!==''){
			payload['image'] = this.props.employee.image
		}
		if(params.id){

			dispatch(employeeAction.editEmployeeInfo(payload, this.props));
			dispatch(employeeAction.resetState())
		}else{
			dispatch(employeeAction.createEmployee(payload, this.props));

		}
	}
	render(){
		$('.dropify').dropify();
		const { match : {params }} = this.props;
		const { employee } = this.props.employee;
		const employeegroup  = this.props.employeegroup.employee_group;
		console.log(this.props)
		return (
			<div>
				<div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Employee</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Employee</div>
							<div className="breadcrumb-item">{this.state.status} Employee</div>
						</div>
					</div>
				</div>
				<div className="p-20-c">
					
					<div className="row">
						<div className="col-7">
							
						{this.props.employee.message}
						
						<div className="card">
							<div className="card-body">
							<h2 className="m-20-vc">{this.state.status} Employee</h2>
							<div className="card-content">
								
								<form className="form">
									<div className="form-group">
										<label htmlFor="#" className="form-label">Name</label>
										<input type="text" value={this.props.employee.name} className="form-control" 
										 onChange={this.handleChange('name')}/>
									</div>
									<div className="form-group">
										<label htmlFor="#" className="form-label">Email</label>
										<input type="text" value={this.props.employee.email} className="form-control" 
										 onChange={this.handleChange('email')}/>
									</div>
									<div className="form-group" style={{'background':(this.props.employee.is_elastic) ? '#cdcdcd' : ''}}>
										<label htmlFor="#" className="form-label">NIK</label>
										<input type="text" value={this.props.employee.nik} className="form-control" onChange={this.handleChange('nik')}/>
									</div>
									<div className="form-group">
										<label htmlFor="#" className="form-label">Card ID</label>
										<input type="text" value={this.props.employee.card_id} className="form-control" 
										 onChange={this.handleChange('card_id')}/>
									</div>
									<div className="form-group" style={{'background':(this.props.employee.is_elastic) ? '#cdcdcd' : ''}}>
										<label htmlFor="#" className="form-label">Telephone</label>
										<span className="number">+62</span>
										<input  type="number"  inputmode="numeric" pattern="[0-9]*" type="tel" value={this.props.employee.phone_no} className="form-control number"  
										 onChange={this.handleChange('phone_no')} 
										 />
									</div>
									<div className="card-content">
										{
											(params.id) ? <div className="form-group" style={{textAlign: 'center'}}>
												<img src={this.props.employee.photo} height="300" ></img>
											</div> : null
										}
									</div>
									<div className="form-group">
										<label htmlFor="#" className="form-label">Employee Image</label>
										<input type="file" className="dropify"  id="id_service_image" name="image" ref={(ref)=>this.fileupload = ref} onChange={this.handleUploadImage(this.fileupload,'image')}  />
									</div>
									
									

								</form>
							</div>
							</div>
				  			</div>		
						</div>
					</div>
					<div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
								{
									this.state.status == 'Update' ? 
									<button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button> :
									<button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="fas fa-save"></i>Insert</button>
								}
								
								<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/employee')}>Cancel</button>

									{/* <button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> */}
							
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		  );
	}
}
const mapStateToProps = (state) =>{

	return state
}
const InsertEmployeePage = withRouter(connect(mapStateToProps, null, null, {
	pure: false
})(InsertEmployee));
export default InsertEmployeePage;
