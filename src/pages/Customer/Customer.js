import React from 'react';
import { connect } from 'react-redux';
import DataTable, { createTheme } from 'react-data-table-component';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { history } from '../../helpers';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import { customerAction } from '../../actions';

import $ from "jquery";
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Customer extends React.Component{
    constructor(props){
        super(props);
    }
    
    componentDidMount(){

        // window.location.reload()
        const { dispatch } = this.props;
        
        dispatch(customerAction.resetState());
        dispatch(customerAction.getCustomer());
        
        $('.modal-toggle').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal-wrapper').addClass('modal-active');
            $('.modal').addClass('modal-active');
        })

        $('.modal-wrapper span.modal-outsider').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal').removeClass('modal-active');
        })
        $('.modal-wrapper .btn-link').click(function() {
            $('.modal-wrapper').removeClass('modal-active');
            $('.modal').removeClass('modal-active');
        })
        $('.dropify').dropify();
        $('#customerWrapper').detach().appendTo('#root')
    }

    handleClick = (event, id) => {
        
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
            
        
        if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
            dispatch(customerAction.deleteCustomer(id));
        }
    }
    handleUploadCsv = (prop, name) => event => {
        const { dispatch } = this.props;
        dispatch(customerAction.onChangePropsCsv(name, prop.files[0]))
		// dispatch(employeeAction.onChangeProps(prop.files[0], name));
	}
    handleSubmitUploadCsv(event){
        const { dispatch, customer } = this.props;
		dispatch(customerAction.uploadCsvCustomer(customer.uploadcsv, this.props))
	}
    render(){
        $('.modal-toggle').click(function() {
            $('#customerWrapper').addClass('modal-active');
            $('#customerWrapper .modal').addClass('modal-active');
        })

        $('.modal-wrapper span.modal-outsider').click(function() {
            $('#customerWrapper').removeClass('modal-active');
            $('#customerWrapper .modal').removeClass('modal-active');
        })
        $('.dropify').dropify();
        $('#customerWrapper').detach().appendTo('#root')
        // console.log(this.props)
        const { customer, isLoading } = this.props.customer;
       
        return (
            <div>
                <div className="p-20-c">
                    <div className="breadcrumb">
                        <div className="page-title">Customer</div>
                        <div className="page-info">
                            <div className="breadcrumb-item">Home</div>
                            <div className="breadcrumb-item">Customer</div>
                        </div>
                    </div>
                </div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-12">

                        {this.props.customer.message}
                            
                            <div className="card">
                                <div className="card-body">
                                    <div className="card-content">
                                    <div className="button-row">
                                        <div className="btn-group" >
                                            <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/customer/insert")}><i className="far fa-plus-square"></i>Create</button>
                                            <button className="btn btn-primary modal-toggle" ><i className="fas fa-upload"></i>Upload CSV</button>
                                        </div>
                                    </div>
                                    {
                
                                        <DataTable
                                            title="Customer"
                                            pagination={true}
                                            columns={[
                                                {
                                                    name: 'First Name',
                                                    selector: 'first_name',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Last Name',
                                                    selector: 'last_name',
                                                    sortable: true,
                                                },
                                                {
                                                    name: 'Action',
                                                    selector: 'customer_id',
                                                    cell: row => <span>
                                                        <button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/customer/edit/"+row.customer_id)}><i className="far fa-edit"></i></button>
                                                        <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.customer_id)}><i className="fas fa-trash"></i></button>
                                                        {/* <button className="btn btn-primary icon-only" onClick={() => this.props.history.push("/dashboard/campaign/detail/"+row.campaign_id)}><i className="fas fa-info-circle"></i></button>
                                                            {
                                                                row.status==0 ? 
                                                                <span><button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/campaign/edit/"+row.campaign_id)}><i className="fas fa-edit"></i></button>
                                                                <button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.campaign_id)}><i className="fas fa-trash"></i></button></span> : null
                                                            } */}
                                                        </span>
                                                }
                                                ]}
                                            data={customer}
                                            striped={true}
                                            customStyles={styleTable}
                                        />
                                    }
                                    {/* <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            typeof customer != 'undefined' ? (
                                                customer.length > 0 ? (
                                                    customer && customer.map(data => {
                                                        return (
                                                                    <tr key={data.customer_id}>
                                                                        <td>{data.first_name}</td>
                                                                        <td>{data.last_name}</td>
                                                                        <td>
                                                                            <button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/customer/edit/"+data.customer_id)}><i className="far fa-edit"></i>Edit</button>
                                                                            <button className="btn btn-primary" onClick={(event) => this.handleClick(event, data.customer_id)}><i className="fas fa-trash"></i>Delete</button>
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            })
                                                    ) : (
                                                        <tr>
                                                            <td colSpan="3" align="center">Tidak Ada Data</td>
                                                        </tr>
                                                    )
                                                )
                                             : null
                                        }
                                        </tbody>
                                        </table> */}
                                    </div>      
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
                <div className="modal-wrapper" id="customerWrapper">
                    <span className="modal-outsider"></span>
                    <div className="modal-wrapper-inner">
                        <div className="modal">
                        <div className="modal-header">
                            Upload CSV Customer
                        </div>
                        <div className="modal-content">
                            <div className="form-group">
                                <label htmlFor="#" className="form-label">Upload CSV</label>
                                <input type="file" className="dropify"  id="id_service_image" name="template" ref={(ref)=>this.fileupload = ref} onChange={this.handleUploadCsv(this.fileupload,'uploadcsv')}  />
                            </div>
                        </div>
                        <div className="modal-footer">
                            {
                                isLoading ?
                                <div className="button-group d-flex">
                                    <button className="btn btn-info" >Uploading ...</button>
                                </div> :
                                <div className="button-group d-flex">
                                <button className="btn btn-link ml-auto">cancel</button>
                                <button className="btn btn-info" onClick={() => this.handleSubmitUploadCsv()}>Process</button>
                                </div>
                            }
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) =>{
    return {
        customer : state.customer
    };
  }

  const customerPage = connect(mapStateToProps, null, null, {
    pure: false
  })(Customer);
export default customerPage;
