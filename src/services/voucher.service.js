import axios from 'axios';
import config from '../config/config';
import S3 from 'react-aws-s3';
import uuid from 'react-uuid'
export const voucherServices = {
	get,
	post,
	put,
	deleteDetail,
	getCode
};
const config_aws = {
    bucketName: 'telegram-bot-s3',
    dirName: 'promo', /* optional */
    region: 'ap-southeast-1',
    accessKeyId: "AKIAXZBVME4GEGDNABFF",
    secretAccessKey: "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",
    s3Url: 'https://telegram-bot-s3.s3-ap-southeast-1.amazonaws.com', /* optional */
}
const ReactS3Client = new S3(config_aws);
function get(apiEndpoint, id=null){
	let url = '';
	if(id==null){
		url = config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id')
	}else{
		url = config.baseUrl+apiEndpoint+'/'+id+'?merchant_id='+localStorage.getItem('merchant_id')
	}
	return axios.get(url, {
		headers: {
			"Authorization" : "Bearer "+localStorage.getItem('token')
		}
	}).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}
function getCode(apiEndpoint, id=null){
	let url = config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id')
	return axios.get(url, {
		headers: {
			"Authorization" : "Bearer "+localStorage.getItem('token')
		}
	}).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}
function post(apiEndpoint, payload){
	

	
	/*  Notice that if you don't provide a dirName, the file will be automatically uploaded to the root of your bucket */
	
	/* This is optional */
	const newFileName = uuid();
	// let img = payload.image.replace(/\\/g,/\// )

	return ReactS3Client
	.uploadFile(payload.template, newFileName)
	.then(data => {
		const formData = new FormData();
		
		formData.append('image', data.location);
		formData.append('voucher_name', payload.voucher_name);
		formData.append('code_csv', payload.code_csv);
		formData.append('total_voucher', payload.total_voucher);
		formData.append('type_voucher', payload.type_voucher);
		formData.append('product_id', payload.product_id);
		formData.append('id_merchant', 1);

		return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
				"Authorization" : "Bearer "+localStorage.getItem('token')
			}
		}).then((response)=>{
			return response;
		}).catch((err)=>{
			return err;
		})
	})
	.catch(err => {
		return err
	})
	
}

function put(apiEndpoint, payload, props){
	const newFileName = uuid();
	if(typeof payload.template !== 'undefined'){
		return ReactS3Client
		.uploadFile(payload.template, newFileName)
		.then(data => {
			const formData = new FormData();
			formData.append('image', data.location);
			formData.append('voucher_name', payload.voucher_name);
			formData.append('code_csv', payload.code_csv);
			formData.append('total_voucher', payload.total_voucher);
			formData.append('type_voucher', payload.type_voucher);
			formData.append('product_id', payload.product_id);
			formData.append('id_merchant', 1);
			return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), formData, {
				headers: {
					"Content-Type": "multipart/form-data",
					"Authorization" : "Bearer "+localStorage.getItem('token')
				}
			}).then((response)=>{
		        return response;
		    }).catch((err)=>{
		        // console.log(err.response.data);
		        return err
		    })
		})
		.catch(err => console.error(err))
	}else{
		const formData = new FormData();
		formData.append('image', props.voucher.image);
		formData.append('voucher_name', payload.voucher_name);
		formData.append('code_csv', payload.code_csv);
		formData.append('total_voucher', payload.total_voucher);
		formData.append('type_voucher', payload.type_voucher);
		formData.append('product_id', payload.product_id);
		formData.append('id_merchant', 1);
		return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), formData, {
			headers: {
				"Content-Type": "multipart/form-data",
				"Authorization" : "Bearer "+localStorage.getItem('token')
			}
		}).then((response)=>{
			return response;
		}).catch((err)=>{
			console.log(err);
		})
		
	}
	
}

function deleteDetail(apiEndpoint){
	return axios.delete(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
		headers: {
			"Authorization" : "Bearer "+localStorage.getItem('token')
		}
	}).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function getOptions(){
	let options = {}; 
	if(localStorage.getItem('token')){
		options.headers = { 'x-access-token': localStorage.getItem('token') };
	}
	return options;
}