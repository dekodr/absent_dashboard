import axios from 'axios';
import config from '../config/config';
import uuid from 'react-uuid'
import S3 from 'react-aws-s3';

import Message from '../components/Message'

export const employeeServices = {
    get,
    post,
    put,
    deleteDetail
};

const config_aws = {
    bucketName: 'rkb-fr',
    dirName: 'register', /* optional */
    region: 'ap-southeast-1',
    accessKeyId: "AKIAIFQK53A6O7EOSTVQ",
    secretAccessKey: "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",
    s3Url: 'https://rkb-fr.s3-ap-southeast-1.amazonaws.com', /* optional */

}
const ReactS3Client = new S3(config_aws);


function get(apiEndpoint, employee){
    return axios.get(config.baseUrl+apiEndpoint+'?company_id=1', {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function post(apiEndpoint, payload){
    
	/* This is optional */
    const newFileName = uuid();
    
    console.log(payload)

    return ReactS3Client
	.uploadFile(payload.image, newFileName)
	.then(data => {
        console.log("upload s3 sukses", data)
		
        payload['photo'] = data.location

        console.log("formdata", payload)
		return axios.post(config.baseUrl+apiEndpoint, payload, {
            headers: {
                // "Authorization" : "Bearer "+localStorage.getItem('token')
            }
        }).then((response)=>{
            console.log(response)
            return response;
        }).catch((err)=>{
            // console.log(err.response.data);
            return err
        })
	})
	.catch(err => {
        console.log("error s3", err)
		return err
	})
}
function put(apiEndpoint, payload){
    
    if(payload['image']!=''){
        const newFileName = uuid();
        return ReactS3Client
        .uploadFile(payload.image, newFileName)
        .then(data => {
            payload['photo'] = data.location
            return axios.put(config.baseUrl+apiEndpoint, payload, {
                headers: {
                    "Authorization" : "Bearer "+localStorage.getItem('token')
                }
            }).then((response)=>{
                return response;
            }).catch((err)=>{
                return err
            })
        })
    }else{
        return axios.put(config.baseUrl+apiEndpoint, payload, {
            headers: {
                "Authorization" : "Bearer "+localStorage.getItem('token')
            }
        }).then((response)=>{
            return response;
        }).catch((err)=>{
            return err
        })
    }
}

function deleteDetail(apiEndpoint){
    return axios.delete(config.baseUrl+apiEndpoint, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        return err
    })
}