import React from 'react';
import Message from "../components/Message"
import { categoryServices } from '../services';
import { history } from '../helpers';

export const categoryAction = {
    getCategory,
    onChangeProps,
    createCategory,
    editCategoryInfo,
    deleteCategoryById,
    getCategoryById,
    resetState,
    message
};

function getCategory(){
    return dispatch => {
        let apiEndpoint = 'category';
        categoryServices.get(apiEndpoint)
        .then((response)=>{
            // console.log(response.data.data);
            dispatch(changeCategoryList(response.data.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}

function createCategory(payload, props){
    return dispatch => {
        let apiEndpoint = 'category';
        categoryServices.post(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(createCategoryInfo());
                props.history.push('/dashboard/setting/category');
            }else if(response.message.indexOf("422") > -1){
                // alert()
                dispatch(message("error", "Please check again. Required field is empty."))
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }else{
                dispatch(message("error", response.data.message))
            }
        }) 
    }
}

function getCategoryById(id){

    return dispatch => {
        let apiEndpoint = 'category/'+ id;
        categoryServices.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            dispatch(editCategoryDetails(response.data.data));
        })
    };
}

function editCategoryInfo(id, payload,props){
    return dispatch => {
        let apiEndpoint = 'category/'+ id;
        categoryServices.put(apiEndpoint, payload)
        .then((response)=>{
            // check response and send feedback
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(updatedCategoryInfo());
                props.history.push('/dashboard/setting/category');
            }else if(response.message.indexOf(422) == true){
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                dispatch(message("error", response.response.data.message))
            }
        }) 
    }
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}

function editVendorInfo(id, payload){
    return dispatch => {
        let apiEndpoint = 'product/'+ id;
        categoryServices.put(apiEndpoint, payload)
        .then((response)=>{
            dispatch(updatedCategoryInfo());
            history.push('/product');
        }) 
    }
}

function deleteCategoryById(id){
    return dispatch => {
        let apiEndpoint = 'category/'+ id;
        categoryServices.deleteDetail(apiEndpoint)
        .then((response)=>{
            console.log(response);
            // check response and send feedback
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteCategoryDetails());
                dispatch(categoryAction.getCategory());
            }else{
                dispatch(message("error", response.response.data.message))
            }
        })
    };
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}

export function changeCategoryList(category){
    return{
        type: "FETCHED_ALL_CATEGORY",
        category: category
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editProductDetails(product){
    return{
        type: "PRODUCT_DETAIL",
        category_id: product._id,
        product_name: product.name
    }
}

export function updatedCategoryInfo(){
    return{
        type: "CATEGORY_UPDATED"
    }
}

export function createCategoryInfo(){
    return{
        type: "USER_CATEGORY_SUCCESSFULLY"
    }
}

export function deleteCategoryDetails(){
    return{
        type: "DELETED_CATEGORY_DETAILS"
    }
}

export function editCategoryDetails(category){
    return{
        type: "CATEGORY_DETAIL",
        category_id: category.id,
        category_name: category.category_name,
        is_linked: category.is_linked,
    }
}

function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_EMPLOYEE"
	}
}