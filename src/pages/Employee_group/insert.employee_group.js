import React from 'react';
import { connect } from 'react-redux';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
// import {transactionService} from '../api/transaction.js';
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import {employeeAction,categoryAction, employeeGroupAction} from '../../actions';

import ToggleSwitch from '../../components/ToggleSwitch';
import $ from 'jquery';

class InsertEmployee extends React.Component{
	constructor(props){
		super(props);
		this.state = {
            employee: []
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(employeeAction.onChangeProps(prop, event));
        
    };
    handleChangeCheckbox = prop => event => {
        const { dispatch } = this.props;
        dispatch(employeeGroupAction.onChangeCheckbox(prop, event));
        
    };
	componentDidMount(){
        const { match : {params }, dispatch, employee, employeegroup } = this.props;
        
        dispatch(employeeGroupAction.resetState());

        dispatch(employeeAction.getEmployee(employee))
        
        if(params.id){
            this.setState({status : 'Update'});
            dispatch(employeeGroupAction.getEmployeeGroupById(params.id));
        } else {
            this.setState({status : 'Insert'});
        }

        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
    }
    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
        
        let list_employee = "["+this.props.employeegroup.list_employee.join(", ")+"]"
        let payload={
            name            : this.props.employeegroup.name,
            emp_list_id     : list_employee
        }
        
        if(params.id){
            dispatch(employeeGroupAction.editEmployeeGroupInfo(params.id, payload,this.props));
            dispatch(employeeGroupAction.resetState());
        }else{
            dispatch(employeeGroupAction.createEmployeeGroup(payload, this.props));

        }
    }
	render(){
        
        const { match : {params }} = this.props;
        const { employeegroup } = this.props.employeegroup;
        
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Employee Group</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Employee Group</div>
                            <div className="breadcrumb-item">{this.state.status} Employee Group</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.employeegroup.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">{this.state.status} Employee Group</h2>
                            <div className="card-content">
                                
                                <form className="form">
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Group Name</label>
                                        <input type="text" value={this.props.employeegroup.name} className="form-control" 
                                         onChange={this.handleChange('name')}/>
                                    </div>
                                    {/* <div className="button-row">
                                        <div className="btn-group" >
                                            {
                                                this.state.status == 'Update' ? 
                                                <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button> :
                                                <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-save"></i>Insert</button>
                                            }
                                            <button className="btn" onClick={() => this.props.history.push('/dashboard/employee_group')}>Cancel</button>
                                       
                                        </div>
                                    </div> */}
                                </form>
                                {/* <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Telephone</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                employee.length > 0 ? (
                                                    employee && employee.map(data => {
                                                        return (
                                                            <tr key={data.employee_id}>
                                                                <td>
                                                                    <input type="checkbox" value={data.employee_id} className="form-control" onChange={this.handleChangeCheckbox('list_employee')}/>
                                                                </td>
                                                                <td>{data.first_name}</td>
                                                                <td>{data.last_name}</td>
                                                                <td>{data.phone_no}</td>
                                                               
                                                            </tr>
                                                        )
                                                        
                                                    })
                                                ) : (
                                                    <tr>
                                                        <td colSpan="4" align="center">Tidak Ada Data</td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </table> */}
                            </div>
                            </div>
                  </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
								    {
                                        this.state.status == 'Update' ? 
                                        <button type="button" className="btn btn-primary" 
                                        onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button> :
                                        <button type="button" className="btn btn-primary" 
                                        onClick={(event) => this.handleSubmit(event)}><i className="fas fa-save"></i>Insert</button>
                                    }
                                    <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/setting/employee_group')}>Cancel</button>

									{/* <button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> */}
							
								</div>
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const InsertEmployeePage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(InsertEmployee));
export default InsertEmployeePage;
