import React from 'react';

import { productServices } from '../services';

import Message from "../components/Message"

export const productAction = {
    getProduct,
    getProductById,
    onChangeProps,
    editProductInfo,
    createProduct,
    deleteProductById,
    resetState,
    message
};

function getProduct(page=1){
    return dispatch => {
        let apiEndpoint = 'product';
        productServices.get(apiEndpoint, page)
        .then((response)=>{
            dispatch(changeProductList(response.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}

function createProduct(payload, props){
    return dispatch => {
        let apiEndpoint = 'product';
        productServices.post(apiEndpoint, payload)
        .then((response)=>{
            console.log(response);
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(createUserInfo());
                props.history.push('/dashboard/product');

                // redirecting
                // props.history.push('/dashboard/employee');
            }else if(response.message.indexOf("422") > -1){
                // alert()
                dispatch(message("error", "Please check again. Required field is empty."))
            }else if(response.message.indexOf("409") > -1 ){
                // dispatch(resetState())
                dispatch(message("error", response.response.data.message))
            }else{
                dispatch(message("error", response.data.message))
            } 
        }) 
    }
}

function getProductById(id){

    return dispatch => {
        let apiEndpoint = 'product/'+ id;
        productServices.get(apiEndpoint)
        .then((response)=>{
            dispatch(editProductDetails(response.data.data));
        })
    };
}

function onChangeProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}

function editProductInfo(id, payload, props){
    return dispatch => {
        let apiEndpoint = 'product/'+ id;
        productServices.put(apiEndpoint, payload)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(updatedProductInfo());
                props.history.push('/dashboard/product');

                // redirecting
                // props.history.push('/dashboard/employee');
            }else if(response.message.indexOf(422) == true){
                console.log(response, "wekwek")
                dispatch(message("error", "Please check again. Required field is empty."))
            }else{
                dispatch(message("error", response.response.data.message))
            }
        }) 
    }
}

function deleteProductById(id){
    let page = 1;
    // console.log(id)
    return dispatch => {
        let apiEndpoint = 'product/'+ id;
        productServices.deleteDetail(apiEndpoint)
        .then((response)=>{
            if(response.status == 200 || response.status == 201){
                dispatch(message("success", response.data.message))
                dispatch(deleteProductDetails());
                dispatch(productAction.getProduct(page));

                // redirecting
                // props.history.push('/dashboard/employee');
            }else{
                dispatch(message("error", response.response.data.message))
            }
            
        })
    };
}

export function message(type,msg){
    // const message = 
    alert(msg);
    return{
        type    : "MESSAGE",
        status  : type,
        // message : <Message type={type} text={msg}/>,
        message : ''

    }
}

export function changeProductList(data){
    console.log(data)
    return{
        type: "FETCHED_ALL_PRODUCT",
        product: data.data,
        total: data.total,
        total_page: data.total_page
    }
}

export function handleOnChangeProps(props, value){
    return{
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}

export function editProductDetails(product){
    return{
        type: "PRODUCT_DETAIL",
        product_id: product.product_id,
        category_id: product.category_id,
        product_name: product.product_name,
        is_linked: product.is_linked,
    }
}

export function updatedProductInfo(){
    return{
        type: "PRODUCT_UPDATED"
    }
}

export function createUserInfo(){
    return{
        type: "USER_CREATED_SUCCESSFULLY"
    }
}

export function deleteProductDetails(){
    return{
        type: "DELETED_PRODUCT_DETAILS"
    }
}
function resetState(){
	return dispatch => {
		dispatch(resetStateHandler())
	}
}
export function resetStateHandler(){
	return{
		type: "RESET_STATE_PRODUCT"
	}
}