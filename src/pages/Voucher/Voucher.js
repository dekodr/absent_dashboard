import React from 'react';
import { connect } from 'react-redux';
import { voucherAction } from '../../actions';

import DataTable, { createTheme } from 'react-data-table-component';
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class Voucher extends React.Component{
	constructor(props){
		super(props);

	}
	
	componentDidMount(){
		const { dispatch } = this.props;
		dispatch(voucherAction.getVoucher());
	}

	handleClick = (event, id) => {
		const { dispatch } = this.props;
		
		if (window.confirm('Apakah anda yakin ingin menghapus data ini ?')) {
			dispatch(voucherAction.deleteVoucherById(id));
		}
	}
	displayDateFormat=(date)=>{
        return date.substr(5,11)
    }
	render(){
		
		const { voucher } = this.props.voucher;
		return (
			<div>
				<div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Voucher</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Voucher</div>
						</div>
					</div>
				</div>
				<div className="p-20-c">
					
					<div className="row">
						<div className="col-12">

						{this.props.voucher.message}
							
							<div className="card">
								<div className="card-body">
									<div className="card-content">
									<div className="button-row">
										<div className="btn-group" >
											<button className="btn btn-primary" onClick={() => this.props.history.push("/dashboard/voucher/insert")}><i className="far fa-plus-square"></i>Create</button>
										</div>
									</div>
									<DataTable
										title="Voucher"
										pagination={true}
										columns={[
											{
												name: 'Voucher Name',
												selector: 'voucher_name',
												sortable: true,
											},
											{
												name: 'Type',
												selector: 'type_voucher',
												sortable: true,
												cell:row=>row.type_voucher==1?
												<i className="fas fa-percentage"></i>
												: <i className="fas fa-dollar-sign"></i>
											},
											{
												name: 'Active',
												selector: 'is_active',
												sortable: true,
												cell:row=>row.is_active?<i className="fas fa-check"></i>:'-'
											},
											{
												name: 'Created Date',
												selector: 'created_at',
												sortable: true,
												cell: row => this.displayDateFormat(row.created_at)
											},
											{
												name: 'Redemption Limit',
												selector: 'total_voucher',
												sortable: true,
											},
											{
												name: 'Action',
												selector: 'campaign_id',
												cell: row => <span>
														<button className="btn btn-warning icon-only" onClick={() => this.props.history.push("/dashboard/voucher/edit/"+row.voucher_id)}><i className="fas fa-edit"></i></button>
														<button className="btn btn-danger icon-only" onClick={(event) => this.handleClick(event, row.voucher_id)}><i className="fas fa-trash"></i></button>
													</span>
											}
											]}
										data={voucher}
										striped={true}
										customStyles={styleTable}
									/>
									{/* <table className="table table-striped">
										<thead>
											<tr>
												<th>#ID</th>
												<th>Promo Name</th>
												<th>Type</th>
												<th>Active</th>
												<th>Created Date</th>
												<th>Redemptions Limit</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										{
											typeof voucher!='undefined' ? 
												voucher.length > 0 ? (
													voucher.map(data => {
													return (
															<tr key={data.voucher_id}>
																<th>{data.voucher_id}</th>
																<th>{data.voucher_name}</th>
																<td>{data.type_voucher==1?
																	<i className="fas fa-percentage"></i>
																	: <i className="fas fa-dollar-sign"></i>
																}</td>
																<td>{data.is_active?<i className="fas fa-check"></i>:'-'}</td>
																<td>{data.created_at}</td>
																<td>{data.total_voucher}</td>
																<td>
																	
																</td>
															</tr>
														)
														
													})
												) : (
													<tr>
														<td colSpan="8" align="center">Tidak Ada Data</td>
													</tr>
												)
											:null
										}
										</tbody>
										</table> */}
									</div>      
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>
	  	);
	}
}
const mapStateToProps = (state) =>{
	return {
		voucher : state.voucher
	};
  }

  const VoucherPage = connect(mapStateToProps, null, null, {
	pure: false
  })(Voucher);
export default VoucherPage;
