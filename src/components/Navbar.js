import React from "react";
import $ from "jquery";

function Navbar() {
	return (
		<div className="navbar-menu">
			<div className="p-20-c">
				{/* <div className="search-bar-frame">
					<div className="search-bar">
						<input id="search-fill" placeholder="Search ..." />
						<button id="search-button">
							<i className="fas fa-search" />
						</button>
					</div>
				</div> */}

				<div className="navbar-right">
					{/* <i className="fas fa-comments" />
					<i className="fas fa-bell" /> */}
					<div className="user-info-frame">
						<div className="image">
							<img src={require("../asset/images/user-profile.png")} />
						</div>
						<div className="content">
							<div className="name">Alexandro Putra</div>
							<div className="status">Super Admin</div>
						</div>
						<br className="clear" />
					</div>
				</div>
				<br className="clear" />
			</div>
		</div>
	);
}

$(window).scroll(function() {
	var scroll = $(window).scrollTop();

	if (scroll >= 30) {
		$(".navbar-menu").addClass("scroll-active");
	} else {
		$(".navbar-menu").removeClass("scroll-active");
	}
});

export default Navbar;
