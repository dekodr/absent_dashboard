import { combineReducers } from 'redux';
import { employeegroup } from './employeeGroup.reducers'; 
import { authentication } from './auth.reducers';
import { employee } from './employee.reducers';
import { product } from './product.reducers';
import { presence } from './presence.reducers';
import { category } from './category.reducers';
import { customer } from './customer.reducers';
import { voucher } from './voucher.reducers';
import { campaign } from './campaign.reducers';
import { customer_group } from "./customer_group.reducers";
import { dashboard } from "./dashboard.reducers";

// import {customer_group} from './customer_group.reducers'

const rootReducer = combineReducers({
  authentication,
  employee,
  product,
  presence,
  category,
  customer,
  voucher,
  campaign,
  employeegroup,
  customer_group,
  dashboard
});;

export default rootReducer;