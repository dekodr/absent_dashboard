const initialState = { anchor: 'left',
    employee_group: [],
    open: false,
    total:0,
    page:0,
    total_page: 1,
    list_employee:[],
    group_name:''
 };
export function employeegroup(state = initialState, action) {
    // console.log(action)
    switch (action.type) {
        case 'FETCHED_ALL_EMPLOYEE_GROUP':
            return {
                ...state,
                employee_group: action.employee_group,
                total: action.total,
                total_page: action.total_page
            };
        case 'FETCHED_ALL_EMPLOYEE_GROUP_BY_CAMPAIGN':
            return {
                ...state,
                employee_group: action.employee_group
            };
        case "HANDLE_ON_CHANGE_CHECKBOX_EMPLOYEE_GROUP":
            // console.log(state[action.props])
            
            let _state = state[action.props]
            // console.log(_state)
            if(typeof _state !== 'undefined'){
                if( _state.includes(action.value)===false ){
                    _state.push(action.value)
                    
                }else{
                    let index = _state.indexOf(action.value);
                    if (index !== -1) _state.splice(index, 1);

                }
            }else{
                _state[action.props] = action.value
            }

            console.log(_state)
            
            return {
                ...state,
                [action.props]: _state
            };    
        case 'EMPLOYEE_GROUP_DETAIL':
            return {
                ...state,
                id: action.id,
                name: action.name,
                emp_list_id: action.emp_list_id,
                is_active: action.is_active,
            };
        case "EMPLOYEE_GROUP_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            }; 
        case "MESSAGE":
            // console.log(action)
            return {
                ...state,
                type: action.type,
                message: action.message
            }
        case "RESET_PAGE":
            return {
                ...state,
                page: 1
            }
        case "RESET_STATE_EMPLOYEE_GROUP":
            return initialState
        default:
            return state
        case "HANDLER_GET_EMPLOYEE_LIST_BY_GROUP_ID":
            return {
                ...state,
                list_employee: action.employee
            }
    }
  }