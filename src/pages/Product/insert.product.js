import React from 'react';
import { connect } from 'react-redux';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { history } from '../../helpers';
import {
	BrowserRouter as Router,
	Switch,
	Route,
    Link,
    useParams,
    useRouteMatch,
    withRouter
  } from "react-router-dom";
import {productAction,categoryAction} from '../../actions';
import $ from 'jquery';

class InsertProduct extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			product: []
        };
	}
	handleChange = prop => event => {
        const { dispatch } = this.props;

        dispatch(productAction.onChangeProps(prop, event));
    };
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
        dispatch(productAction.resetState());
        
        dispatch(categoryAction.getCategory());

        if(params.id){
            this.setState({status : 'Update'});
            dispatch(productAction.getProductById(params.id));
        } else {
            this.setState({status : 'Insert'});
        }
    }

    handleSubmit(event){
        const { match : {params } } = this.props;
        const { dispatch } = this.props;
        window.scrollTo(0, 0)
            
        let payload={
            category_id: this.props.product.category_id,
            product_name: this.props.product.product_name,
        }
        
        if(params.id){
            dispatch(productAction.editProductInfo(params.id, payload, this.props));
            dispatch(productAction.resetState());
        }else{
            dispatch(productAction.createProduct(payload, this.props));
        }

        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
    }
	render(){
        
        const { match : {params }} = this.props;
        const { category } = this.props.category;
        
		return (
			<div>
                <div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Product</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Product</div>
                            <div className="breadcrumb-item">{this.state.status} Product</div>
						</div>
					</div>
				</div>
                <div className="p-20-c">
                    
                    <div className="row">
                        <div className="col-7">

                        {this.props.employee.message}
                            
                        <div className="card">
                            <div className="card-body">
                            <h2 className="m-20-vc">{this.state.status} Product</h2>
                            <div className="card-content">
                                
                                <form className="form">
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Category</label>
                                        <select value={this.props.product.category_id} className="form-control" onChange={this.handleChange('category_id')}>
                                            <option value="">--Choose one Category--</option>
                                        {
                                            category.map(data => {
                                                return (
                                                    <option key={data.category_id} value={data.category_id}>
                                                        {data.category_name}
                                                    </option>
                                                )
                                                
                                            })
                                        }
                                        </select> 
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="#" className="form-label">Product Name</label>
                                        <input type="text" value={this.props.product.product_name} className="form-control" 
                                         onChange={this.handleChange('product_name')}/>
                                    </div>
                                    {/* <div className="button-row">
                                        <div className="btn-group" >
                                            {
                                                this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                            onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> : 
                                            <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)}><i className="fas fa-pen-alt"></i>Update</button>
                                            }
                                            
                                            <button className="btn" onClick={() => this.props.history.push('/dashboard/product')}>Cancel</button>
                                        </div>
                                    </div> */}
                                </form>
                            </div>
                            </div>
                  </div>		
                        </div>
                    </div>
                    <div className="row">
						<div className="col-auto">
							<div className="card">
								<div className="card-body">
                                {
                                            this.state.status == 'Insert' ? <button type="button" className="btn btn-primary" 
                                            onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button> : 
                                            <button type="button" className="btn btn-primary" 
                                                onClick={(event) => this.handleSubmit(event)} disabled={this.props.product.is_linked}><i className="fas fa-pen-alt"></i>Update</button>
                                            }
                                            
                                            <button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/product')}>Cancel</button>

									{/* <button type="button" className="btn btn-primary" 
									onClick={(event) => this.handleSubmit(event)}><i className="far fa-plus-square"></i>Create</button>
									<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/campaign')}>Cancel</button> */}
							
								</div>
                                {this.props.product.is_linked ? <p>*Cannot update product: data already linked to a running campaign</p>:''}
							</div>
						</div>
					</div>
                </div>
                
            </div>
	  	);
	}
}
const mapStateToProps = (state) =>{

    return state
}
const InsertProductPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(InsertProduct));
export default InsertProductPage;
