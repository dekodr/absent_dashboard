import React from 'react';
import { connect } from 'react-redux';
// import logo from './logo.svg';
// import './Member.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
// import {transactionService} from '../api/transaction.js';
import { history } from '../../helpers';
import DataTable, { createTheme } from 'react-data-table-component';
import "../../asset/js/jquery-master";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	useParams,
	useRouteMatch,
	withRouter
  } from "react-router-dom";
import $ from "jquery";
import dropify from "dropify";
import "../../../node_modules/dropify/src/sass/dropify.scss";
import {voucherAction, productAction} from '../../actions';
const styleTable ={
	table: {
		style: {
		  height: 'auto',

		},
	  },
}
class InsertVoucher extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			form_type: 'Insert'
		};
	}
	handleUploadImage = (prop, name) => event => {
		const { dispatch } = this.props;
		dispatch(voucherAction.onChangeImage(prop.files[0], name));
	};
	handleUpload = (prop, name) => event => {
		const { dispatch } = this.props;
		dispatch(voucherAction.onChangeUpload(prop.files[0], name));
	};
	handleChange = prop => event => {
		const { dispatch } = this.props;
	   
		dispatch(voucherAction.onChangeProps(prop, event));
	};
	componentDidMount(){
		const { match : {params }, dispatch } = this.props;
		// dispatch(voucherAction.resetStateCampaign())
		
		dispatch(productAction.getProduct())
		if(params.id){

			this.setState({form_type: 'Update'})
			dispatch(voucherAction.getVoucherById(params.id));
			dispatch(voucherAction.getVoucherCodeById(params.id))
		}
        $(document).on("keydown", "form", function(event) { 
            return event.key != "Enter";
        });
	}
	handleSubmit(event){
		const { match : {params } } = this.props;
		const { dispatch } = this.props;
		window.scrollTo(0, 0)
		
		let payload={
			template: this.props.voucher.template,
			voucher_name: this.props.voucher.voucher_name,
			code_csv: this.props.voucher.code_csv,
			total_voucher: this.props.voucher.total_voucher,
			type_voucher: this.props.voucher.type_voucher,
			product_id: this.props.voucher.product_id
		}

		if(params.id){
		   
			dispatch(voucherAction.editVoucherInfo(params.id, payload, this.props));
		}else{
			dispatch(voucherAction.createVoucher(payload, this.props));
			
		}
		
	}
	deleteVoucherCode(voucher_code_id){
		const { dispatch, match : {params }, voucher } = this.props;
		if (window.confirm("Are you sure to delete this data?")) {
			dispatch(voucherAction.deleteVoucherCode(params.id, voucher_code_id));
		}
	}
	render(){
	   
		const { match : {params }, voucher } = this.props;
		const {product} = this.props.product

		const {isLoading, is_submit, voucher_code_list} = this.props.voucher
		// $("#id_service_image").addClass('dropify');
		$("#id_service_image").attr("data-height", 300);
		// $("#id_service_image").attr("data-default-file", require('/images/image-name.png'));

		$('#id_service_image').dropify({
			messages: {
				'default': ( params.id) ? 'Drag and drop or click to change the image above' :'Drag and drop a file here or click upload the image' ,
				'replace': 'Drag and drop or click to replace',
				'remove':  'Remove',
				'error':   'Ooops, something wrong happended.'
			}
		});
		$('#csv_upload').dropify({
			messages: {
				'default': 'Drag and drop or click to upload the data' ,
				'replace': 'Drag and drop or click to replace',
				'remove':  'Remove',
				'error':   'Ooops, something wrong happended.'
			}
		});
		
		// const { category } = this.props.category;
		return (
			<div>
				<div className="p-20-c">
					<div className="breadcrumb">
						<div className="page-title">Insert Voucher</div>
						<div className="page-info">
							<div className="breadcrumb-item">Home</div>
							<div className="breadcrumb-item">Voucher</div>
							<div className="breadcrumb-item">{this.state.form_type} Voucher</div>
						</div>
					</div>
				</div>
				<div className="p-20-c">
					
					<div className="row">
						<div className="col-12">

							{this.props.voucher.message}
								
							<div className="card">
								<div className="card-body">

									<form className="form">
										<div className="row">
											<div className="col-8">
													
												<div className="form-group">
													<label htmlFor="#" className="form-label">Voucher Name</label>
													<input type="text" value={this.props.voucher.voucher_name} className="form-control" 
													 onChange={this.handleChange('voucher_name')}/>
												</div>
												<div className="form-group">
													<label htmlFor="#" className="form-label">Total Voucher</label>
													<input type="number" inputmode="numeric" pattern="[0-9]*" type="tel" value={this.props.voucher.total_voucher} className="form-control" 
													 onChange={this.handleChange('total_voucher')}/>
												</div>
												<div className="form-group">
													<label htmlFor="#" className="form-label">Type Promo</label>
													<select value={this.props.voucher.type_voucher} className="form-control" onChange={this.handleChange('type_voucher')}>
														<option value="1">Diskon %</option>
														<option value="2">Diskon Rp</option>
													</select> 
												</div>
												<div className="form-group">
													<label htmlFor="#" className="form-label">Product</label>
													<select value={this.props.voucher.product_id}  className="form-control" onChange={this.handleChange('product_id')}>
														<option value="">--Choose one Product--</option>
													{
														product.length > 0 ? (
															product.map(data => {
																return (
																	<option key={data.product_id} value={data.product_id} >
																		{data.product_name}
																	</option>
																)
																
															})
														): null
													}
													</select> 
												</div>
												<br />
												<h6>Upload CSV Code</h6><br/>
												<div className="form-group upload-csv">
													
													<input type="file" className="dropify" name="code_csv" id="csv_upload" ref={(ref)=>this.fileuploadcsv = ref} onChange={this.handleUpload(this.fileuploadcsv,'code_csv')}  />
												</div>
												
														
											</div>
								
											<div className="col-4">		
												<div className="form-group">
													<label htmlFor="#" className="form-label">Add Voucher Image</label>
													<input type="file" className="dropify"  id="id_service_image" name="template" ref={(ref)=>this.fileupload = ref} onChange={this.handleUploadImage(this.fileupload,'template')}  />
												</div>
												
												<div className="card-content">
													{
														(params.id) ? <div className="form-group" style={{textAlign: 'center'}}>
															<img src={voucher.image} height="300" ></img>
														</div> : null
													}
												</div>
											</div>
									
										</div>	
										
									</form>
										{
											 params.id ? <div className="form-group">
												<DataTable
													title="Voucher Code"
													pagination={true}
													columns={[
														{
														  name: 'Voucher Code',
														  selector: 'voucher_code',
														  sortable: true,
														},
														{
															name: 'Released',
															selector: 'is_release',
															cell: row => <div>{row.is_release? <i className="fas fa-check"></i> : <i className="fas fa-times"></i>}</div>,
														},
														{
															name: 'Redeemed',
															selector: 'is_redeem',
															cell: row => <div>{row.is_redeem? <i className="fas fa-check"></i> : <i className="fas fa-times"></i>}</div>,
														
														},
														{
															name: 'Action',
															selector: 'voucher_code_id',
															cell: row => <button className="btn btn-danger-hover-outline btn-icon-sm" onClick={() => this.deleteVoucherCode(row.voucher_code_id)}><i className="fas fa-trash"></i>Delete</button>,
														
														}
													  ]}
													data={voucher_code_list}
													customStyles={styleTable}
												/>
											</div> :
											null
										}
								</div>					
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-auto right">
							<div className="card">
								<div className="card-body">
									<div className="btn-group" >
										{
											(isLoading) ?
											<p>Uploading Data ...</p>
											:
											<div>
												<button type="button" className="btn btn-primary" 
												onClick={(event) => this.handleSubmit(event)} disabled={this.props.voucher.is_linked}>{
													params.id ? 
													<p><i className="fas fa-pen"></i>Update </p>: 
													<p><i className="far fa-plus-square"></i>Create</p>
												}</button>
												<button className="btn btn-secondary" onClick={() => this.props.history.push('/dashboard/voucher')}>Cancel</button>
											</div>
										}
										
									
									</div>
									{this.props.voucher.is_linked ? <p>*Cannot update product: data already linked to a running campaign</p>:''}
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
	  	);
	}
}
const mapStateToProps = (state) =>{

	return state
}
const InsertVoucherPage = withRouter(connect(mapStateToProps, null, null, {
	pure: false
})(InsertVoucher));
export default InsertVoucherPage;
