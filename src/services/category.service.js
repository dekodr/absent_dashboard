import axios from 'axios';
import config from '../config/config';


export const categoryServices = {
    get,
    post,
    put,
    deleteDetail
};

function get(apiEndpoint){
    return axios.get(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        return response;
    }).catch((err)=>{
        console.log("Error in response");
        console.log(err);
    })
}

function post(apiEndpoint, payload){
    return axios.post(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), payload, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function put(apiEndpoint, payload){
    return axios.put(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'),payload, {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function deleteDetail(apiEndpoint){
    return axios.delete(config.baseUrl+apiEndpoint+'?merchant_id='+localStorage.getItem('merchant_id'), {
        headers: {
            "Authorization" : "Bearer "+localStorage.getItem('token')
        }
    }).then((response)=>{
        // console.log(response.data.message)
        return response;
    }).catch((err)=>{
        // console.log(err.response.data);
        return err
    })
}

function getOptions(){
    let options = {}; 
    if(localStorage.getItem('token')){
        options.headers = { 'x-access-token': localStorage.getItem('token') };
    }
    return options;
}