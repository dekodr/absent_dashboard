import React, { Component } from "react";
import "../asset/css/message.css";
import "../asset/css/animate_keren_mancui.css";
import "../actions"
import PropTypes from "prop-types";

class Message extends Component {
	
	state = {
		// checked: this.props.defaultChecked
	};

	// info
	// success
	// warning
	// error
	componentDidMount(){
		alert(this.props.text)
	}
	
	onClick(){
	}

	message_(){
		let type = [];
		if(this.props.type == 'warning') {
			type.css = 'warning-msg'
			type.icon= <i className="fa fa-warning"></i>
		}else if(this.props.type == 'error') {
			type.css = 'error-msg'
			type.icon= <i className="fa fa-info-circle"></i>
		}else if(this.props.type == 'success') {
			type.css = 'success-msg'
			type.icon= <i className="fa fa-check"></i>
		}else{
			type.css = 'info-msg'
			type.icon= <i className="fa fa-info"></i>
		}
		
		return (
			// delay-1s 
			<div onClick={this.onClick()} className={"message animated shake "+type.css}>
				{type.icon}
				{this.props.text}
			</div>
		)
	}

	// startTimer() {
    //     if (this.timer == 0 && this.state.seconds > 0) {
    //       this.timer = setInterval(this.countDown, 1000);
    //     }
    // }
    // countDown() {
    //     const { dispatch} = this.props;
    //     // Remove one second, set state so a re-render happens.
    //     let seconds = this.state.seconds - 1;
    //     this.setState({
    //       time: this.secondsToTime(seconds),
    //       seconds: seconds,
    //     });
        
    //     // Check if we're at zero.
    //     if (seconds == 0) { 
    //         dispatch(campaignAction.removeAlert())
    //       clearInterval(this.timer);
    //     }
    // }

	render() {
		return true;
		
		// return (
		// 	this.message_()
		// )

	}
}
Message.propTypes = {
	text: PropTypes.string.isRequired,
	type: PropTypes.string,
	// onChange: PropTypes.func,
  };
  
export default Message;