const initialState = { anchor: 'left',
	product: [],
	open: false,
	product_id: '', 
	total:0,
	total_page: 0, 
	category_id: '',
	product_name: ''
 };


export function product(state = initialState, action) {
	switch (action.type) {
		case 'FETCHED_ALL_PRODUCT':
			return {
			...state,
			product: action.product,
			total: action.total,
			total_page: action.total_page
			};
		case 'PRODUCT_DETAIL':
			return {
				...state,
				product_id: action.product_id,
				category_id: action.category_id,
				product_name: action.product_name,
				is_linked: action.is_linked,
			};
		// case "EMPLOYEE_UPDATED":
		//     return state;
		case "HANDLE_ON_CHANGE":
			return {
				...state,
				[action.props]: action.value
			};
		
		case "RESET_STATE_PRODUCT":
			return initialState

		case "MESSAGE":
			return {
				...state,
				type: action.type,
				message: action.message
			}
		
		case "RESET_STATE_PRODUCT":
			return initialState
			
		default:
			return state
	}
  }